/*
 * @lc app=leetcode.cn id=268 lang=cpp
 *
 * [268] 丢失的数字
 */

#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include <set>

using namespace std;

// @lc code=start
class Solution {
public:
    int missingNumber(vector<int>& nums) {
        // My
        // std::sort(nums.begin(), nums.end());
        // for (int i = 0; i < nums.size(); i++)
        //     if (nums[i] != i) return i;
        // return nums.size();

        // 异或法
        int res = nums.size();
        for (int i = 0; i < nums.size(); i++) {
            res ^= nums[i];
            res ^= i;
        }
        return res;

        // 等差数列计算法
        // int sum = 0;
        // for (auto& n : nums) sum += n;
        // return nums.size() * (nums.size() + 1) / 2 - sum;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=8 lang=cpp
 *
 * [8] 字符串转换整数 (atoi)
 */

#include <algorithm>
#include <cstdint>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    int myAtoi(string s) {
        int nega_flg = 1;
        long int res = 0;

        int i = 0;
        while (s[i] == ' ') i++;
        if (s[i] == '-') nega_flg = -1;
        if (s[i] == '-' || s[i] == '+') i++;
        while (s[i] >= '0' && s[i] <= '9') {
            res = res * 10 + s[i] - '0';
            if (res > INT32_MAX) break;
            i++;
        }
        res *= nega_flg;
        if (res > INT32_MAX) return INT32_MAX;
        if (res < INT32_MIN) return INT32_MIN;
        return (int)res;
    }
};
// @lc code=end

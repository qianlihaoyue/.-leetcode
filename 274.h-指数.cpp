/*
 * @lc app=leetcode.cn id=274 lang=cpp
 *
 * [274] H 指数
 */

#include <algorithm>
#include <functional>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int hIndex(vector<int>& citations) {
        // 统计
        // 每个数代表，该引用数的文章有几篇
        // std::vector<int> cite(1001, 0);
        // for (auto& p : citations) cite[p]++;
        // // 大于该引用的共有几篇
        // for (int i = cite.size() - 1; i > 0; i--) cite[i - 1] += cite[i];
        // // 需要满足，sum(h) > h
        // for (int i = cite.size() - 1; i >= 0; i--) {
        //     if (cite[i] >= i) return i;
        // }
        // return 0;

        // 排序法
        std::sort(citations.begin(), citations.end(), std::greater());
        for (int i = citations.size() - 1; i >= 0; i--)
            if (citations[i] >= i + 1) return i + 1;
        return 0;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=449 lang=cpp
 *
 * [449] 序列化和反序列化二叉搜索树
 */

#include <bits/stdc++.h>
#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Codec {
public:
    // 单独使用中序遍历序列化BST时，无法唯一确定原始的BST，除非所有元素都是唯一的

    // 使用先序遍历，编码为 BST
    void dfs(TreeNode* root, string& str) {
        if (!root) return;
        if (!str.empty()) str.push_back(',');
        str.append(to_string(root->val));
        dfs(root->left, str);
        dfs(root->right, str);
    }

    // Encodes a tree to a single string.
    string serialize(TreeNode* root) {
        string str;
        dfs(root, str);
        return str;
    }

    // Helper function to construct the BST from preorder traversal
    TreeNode* constructBST(vector<int>& nums, int& idx, int lower, int upper) {
        if (idx == nums.size() || nums[idx] < lower || nums[idx] > upper) return nullptr;

        int val = nums[idx++];
        TreeNode* root = new TreeNode(val);
        root->left = constructBST(nums, idx, lower, val);
        root->right = constructBST(nums, idx, val, upper);
        return root;
    }

    // Decodes your encoded data to tree.
    TreeNode* deserialize(string data) {
        vector<int> nums;
        string token;
        stringstream ss(data);
        while (getline(ss, token, ',')) nums.push_back(stoi(token));
        int idx = 0;
        return constructBST(nums, idx, INT_MIN, INT_MAX);
    }
};

// Your Codec object will be instantiated and called as such:
// Codec* ser = new Codec();
// Codec* deser = new Codec();
// string tree = ser->serialize(root);
// TreeNode* ans = deser->deserialize(tree);
// return ans;
// @lc code=end

/*
 * @lc app=leetcode.cn id=1049 lang=cpp
 *
 * [1049] 最后一块石头的重量 II
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int lastStoneWeightII(vector<int>& stones) {
        // 找寻最接近 sum/2 的组 01背包

        int sum = 0;
        for (auto& n : stones) sum += n;
        int hsum = sum / 2;

        std::vector<int> dp(hsum + 1, 0);
        for (int i = 0; i < stones.size(); i++) {
            for (int j = hsum; j >= stones[i]; j--) {
                dp[j] = max(dp[j], dp[j - stones[i]] + stones[i]);
            }
            // for (int k = 0; k < hsum + 1; k++) cout << dp[k] << " ";
            // cout << endl;
        }

        return sum - dp[hsum] * 2;
    }
};
// @lc code=end

int main() {
    vector<int> stones = {2, 7, 4, 1, 8, 1};
    Solution solu;
    int res = solu.lastStoneWeightII(stones);
    cout << "res: " << res << endl;
}

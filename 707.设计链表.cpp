/*
 * @lc app=leetcode.cn id=707 lang=cpp
 *
 * [707] 设计链表
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start

// 虚拟头结点
class MyLinkedList {
private:
    struct LinkNode {
        int val;
        LinkNode *next;
        LinkNode() : val(0), next(nullptr) {}
        LinkNode(int val_) : val(val_), next(nullptr) {}
        LinkNode(int val_, LinkNode *next_) : val(val_), next(next_) {}
    };

    LinkNode *dummy;
    int size;

public:
    MyLinkedList() {
        dummy = new LinkNode();
        size = 0;
    }

    int get(int index) {
        if (index >= size || index < 0) return -1;
        LinkNode *tmp = dummy->next;
        while (index--) tmp = tmp->next;
        return tmp->val;
    }

    void addAtHead(int val) {
        LinkNode *ori = dummy->next;
        dummy->next = new LinkNode(val, ori);
        ++size;
    }

    void addAtTail(int val) {
        LinkNode *cur = dummy;
        while (cur->next) cur = cur->next;
        cur->next = new LinkNode(val);
        ++size;
    }

    void addAtIndex(int index, int val) {
        if (index > size || index < 0) return;
        // if (index == 0) return addAtHead(val);
        // if (index == size) return addAtTail(val);
        LinkNode *cur = dummy;
        while (index--) cur = cur->next;
        LinkNode *ori = cur->next;
        cur->next = new LinkNode(val, ori);
        ++size;
    }

    void deleteAtIndex(int index) {
        if (index >= size || index < 0) return;
        LinkNode *cur = dummy;
        while (index--) cur = cur->next;
        LinkNode *next = cur->next->next;
        delete cur->next;
        cur->next = next;
        --size;
    }

    void show() {
        LinkNode *tmp = dummy->next;
        while (tmp) {
            std::cout << tmp->val << " ";
            tmp = tmp->next;
        }
        std::cout << endl;
    }
};

// @lc code=end

int main(int argc, char *argv[]) {
    MyLinkedList *obj = new MyLinkedList();
    obj->addAtHead(7);
    obj->addAtHead(2);
    obj->addAtHead(1);

    obj->addAtIndex(3, 0);

    obj->show();  // 1 2 7 0

    obj->deleteAtIndex(2);

    obj->show();  // 1 2 0

    obj->addAtHead(6);
    obj->addAtTail(4);
    obj->addAtHead(4);

    obj->show();  // 4 6 1 2 0 4

    obj->addAtIndex(5, 0);
    obj->addAtHead(6);

    obj->show();  // 6 4 6 1 2 0 0 4

    return 0;
}

// class MyLinkedList {
//     // private:
// public:
//     struct LinkNode {
//         int val;
//         LinkNode *next;
//         LinkNode() : val(0), next(nullptr) {}
//         LinkNode(int val_) : val(val_), next(nullptr) {}
//         LinkNode(int val_, LinkNode *next_) : val(val_), next(next_) {}
//     };
//     LinkNode *head;
//     LinkNode *tail;
//     int size;
// public:
//     MyLinkedList() {
//         head = tail = nullptr;
//         size = 0;
//     }
//     int get(int index) {
//         if (index >= size) return -1;
//         LinkNode *tmp = head;
//         while (index--) tmp = tmp->next;
//         return tmp->val;
//     }
//     void addAtHead(int val) {
//         if (size == 0)
//             head = tail = new LinkNode(val);
//         else {
//             LinkNode *new_head = new LinkNode(val, head);
//             head = new_head;
//         }
//         size++;
//     }
//     void addAtTail(int val) {
//         if (size == 0)
//             head = tail = new LinkNode(val);
//         else {
//             tail->next = new LinkNode(val);
//             tail = tail->next;
//         }
//         size++;
//     }
//     void addAtIndex(int index, int val) {
//         if (index > size || index < 0) return;
//         if (index == 0) return addAtHead(val);
//         if (index == size) return addAtTail(val);
//         LinkNode *tmp = head;
//         while (--index) tmp = tmp->next;
//         LinkNode *node = new LinkNode(val, tmp->next);
//         tmp->next = node;
//         size++;
//     }
//     void deleteAtIndex(int index) {
//         if (index >= size || index < 0) return;
//         if (index == 0) {
//             LinkNode *tmp_del = head;
//             head = head->next;
//             delete tmp_del;
//         } else {
//             LinkNode *tmp = head;
//             int tmp_idx = index;
//             while (--tmp_idx) tmp = tmp->next;
//             LinkNode *tmp_del = tmp->next;
//             tmp->next = tmp->next->next;
//             delete tmp_del;
//             if (index == size - 1) tail = tmp;
//         }
//         size--;
//     }
//     void show() {
//         LinkNode *tmp = head;
//         while (tmp) {
//             std::cout << tmp->val << " ";
//             tmp = tmp->next;
//         }
//         std::cout << endl;
//     }
// };

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * MyLinkedList* obj = new MyLinkedList();
 * int param_1 = obj->get(index);
 * obj->addAtHead(val);
 * obj->addAtTail(val);
 * obj->addAtIndex(index,val);
 * obj->deleteAtIndex(index);
 */
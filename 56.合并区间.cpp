/*
 * @lc app=leetcode.cn id=56 lang=cpp
 *
 * [56] 合并区间
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> merge(vector<vector<int>>& intervals) {
        // 需要进行排序
        sort(intervals.begin(), intervals.end());
        vector<vector<int>> res;

        for (int i = 0; i < intervals.size();) {
            int t = intervals[i][1];
            int j = i + 1;
            while (j < intervals.size() && t >= intervals[j][0]) t = max(t, intervals[j++][1]);

            res.push_back({intervals[i][0], t});
            i = j;
        }

        /////////////////////// 自己写的
        // for (int i = 0; i < intervals.size() - 1; i++) {
        //     auto& bfr = intervals[i];
        //     auto& aft = intervals[i + 1];
        //     // 全方面覆盖
        //     if (aft[1] <= bfr[1]) {
        //         aft[0] = bfr[0], aft[1] = bfr[1];  // 更新后面的
        //     } else {
        //         // 前后
        //         if (aft[0] <= bfr[1])
        //             aft[0] = bfr[0];
        //         else {
        //             res.push_back(bfr);
        //         }
        //     }
        // }
        // res.push_back(intervals[intervals.size() - 1]);

        return res;
    }
};
// @lc code=end

int main() {
    Solution so;
    vector<vector<int>> intervals = {{1, 3}, {2, 6}, {8, 10}, {15, 18}};
    auto res = so.merge(intervals);

    for (auto& vec : res) {
        cout << vec[0] << " " << vec[1] << endl;
    }
}
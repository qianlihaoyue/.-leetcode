/*
 * @lc app=leetcode.cn id=107 lang=cpp
 *
 * [107] 二叉树的层序遍历 II
 */

#include <algorithm>
#include <any>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrderBottom(TreeNode *root) {
        vector<vector<int>> result;
        if (root == nullptr) return result;
        vector<TreeNode *> qu;
        qu.push_back(root);
        while (!qu.empty()) {
            vector<TreeNode *> qu_next;
            vector<int> num_group;
            for (auto &node : qu) {
                num_group.push_back(node->val);
                if (node->left != nullptr) qu_next.push_back(node->left);
                if (node->right != nullptr) qu_next.push_back(node->right);
            }
            qu = qu_next;
            result.push_back(num_group);
        }

        // 对result倒序
        for (int i = 0; i < result.size() / 2; i++) swap(result[i], result[result.size() - 1 - i]);

        return result;
    }
};
// @lc code=end

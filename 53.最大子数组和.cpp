/*
 * @lc app=leetcode.cn id=53 lang=cpp
 *
 * [53] 最大子数组和
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <type_traits>
#include <utility>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>
#include <queue>
using namespace std;

// @lc code=start
class Solution {
public:
    int maxSubArray(vector<int>& nums) {
        int sum = 0;
        int max_sum = -10e5;
        for (int i = 0; i < nums.size(); i++) {
            sum += nums[i];
            max_sum = max(max_sum, sum);
            if (sum < 0) sum = 0;
        }
        return max_sum;
    }
};
// @lc code=end

int main() {
    Solution solute;

    // vector<int> g = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
    // vector<int> g = {5, 4, -1, 7, 8};
    vector<int> g = {-1,-2};
    auto result = solute.maxSubArray(g);

    // for (auto& n : result) cout << n << " ";

    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    cout << endl << result << endl;

    return 0;
}

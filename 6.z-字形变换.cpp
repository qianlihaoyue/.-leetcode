/*
 * @lc app=leetcode.cn id=6 lang=cpp
 *
 * [6] Z 字形变换
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

// @lc code=start
class Solution {
public:
    string convert(string s, int numRows) {
        // 特例
        if (numRows == 1) return s;
        // 新建n个字符串，代表每一行
        std::vector<string> strs(numRows);

        // 遍历字符串
        int r = 0, dir = 1;  // 记录第几行，以及向上还是向下
        for (int i = 0; i < s.length(); i++) {
            strs[r] += s[i];
            if (r + dir >= numRows || r + dir < 0) dir = -dir;  // 换方向
            r += dir;
        }
        // 组合
        string res;
        for (auto &str : strs) res += str;
        return res;
    }
};
// @lc code=end

int main() {
    Solution so;
    std::cout << so.convert("AB", 1);

    return 0;
}
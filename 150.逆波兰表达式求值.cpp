/*
 * @lc app=leetcode.cn id=150 lang=cpp
 *
 * [150] 逆波兰表达式求值
 */

#include <bits/stdc++.h>
#include <cstddef>
#include <functional>
#include <stack>
#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    void getNum(stack<int> &st, int &a, int &b) {
        // 注意a，b顺序
        b = st.top();
        st.pop();
        a = st.top();
        st.pop();
    }

    int evalRPN(vector<string> &tokens) {
        stack<int> st;

        int a, b;
        for (auto &s : tokens) {
            if (s == "+") {
                getNum(st, a, b);
                st.push(a + b);
            } else if (s == "-") {
                getNum(st, a, b);
                st.push(a - b);
            } else if (s == "*") {
                getNum(st, a, b);
                st.push(a * b);
            } else if (s == "/") {
                getNum(st, a, b);
                st.push(a / b);
            } else {
                st.push(stoi(s));
            }

            // continue;
            // std::cout << st.top();
        }
        return st.top();
    }
};
// @lc code=end

int main() {
    vector<string> tokens = {"4", "13", "5", "/", "+"};
    Solution solu;
    cout << solu.evalRPN(tokens);
}
/*
 * @lc app=leetcode.cn id=258 lang=cpp
 *
 * [258] 各位相加
 */

#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int addDigits(int num) {
        // int sum = 0;
        // while (num >= 10) {
        //     sum += num % 10;
        //     num /= 10;
        // }
        // sum += num;
        // if (sum >= 10) return addDigits(sum);
        // return sum;

        // 数学法，n=10a+b = 9a+a+b
        // if (!num) return 0;
        // return num % 9 == 0 ? 9 : num % 9;

        return (num - 1) % 9 + 1;  // 特殊情况 9-9，0-0
    }
};
// @lc code=end

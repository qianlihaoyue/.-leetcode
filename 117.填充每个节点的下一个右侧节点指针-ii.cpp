/*
 * @lc app=leetcode.cn id=117 lang=cpp
 *
 * [117] 填充每个节点的下一个右侧节点指针 II
 */

#include <algorithm>
using namespace std;

class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;
    Node() : val(0), left(NULL), right(NULL), next(NULL) {}
    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}
    Node(int _val, Node* _left, Node* _right, Node* _next) : val(_val), left(_left), right(_right), next(_next) {}
};

// @lc code=start
/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* left;
    Node* right;
    Node* next;
    Node() : val(0), left(NULL), right(NULL), next(NULL) {}
    Node(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}
    Node(int _val, Node* _left, Node* _right, Node* _next)
        : val(_val), left(_left), right(_right), next(_next) {}
};
*/

class Solution {
public:
    // 层序
    Node* connect(Node* root) {
        if (!root) return root;

        vector<Node*> group;
        group.push_back(root);

        while (!group.empty()) {
            vector<Node*> group_next;
            for (int i = 0; i < group.size(); i++) {
                if (i < group.size() - 1) group[i]->next = group[i + 1];
                // else  group[i]->next = NULL;

                if (group[i]->left) group_next.push_back(group[i]->left);
                if (group[i]->right) group_next.push_back(group[i]->right);
            }
            group = group_next;
        }
        return root;
    }
};
// @lc code=end

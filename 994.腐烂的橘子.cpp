/*
 * @lc app=leetcode.cn id=994 lang=cpp
 *
 * [994] 腐烂的橘子
 */

#include <iostream>
#include <vector>
#include <queue>
#include <unordered_set>

using namespace std;

// @lc code=start
class Solution {
public:
    // 计算新鲜总数
    int calCnt(vector<vector<int>>& grid) {
        int cnt = 0;
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if (grid[i][j] == 1) cnt++;
            }
        }
        return cnt;
    }

    int orangesRotting(vector<vector<int>>& grid) {
        // 直接迭代就行，然后当两次结果一致时，如果仍然存在新鲜水果-1，其他cnt
        int step = 0;
        int cnt_bfr, cnt_aft;

        // 重复计算了，待优化
        cnt_bfr = calCnt(grid);
        while (1) {
            // 需要同时触发
            vector<pair<int, int>> buff;
            for (int i = 0; i < grid.size(); i++) {
                for (int j = 0; j < grid[0].size(); j++) {
                    if (grid[i][j] == 2) buff.push_back(make_pair(i, j));
                    // 可以把统计calCnt放在这里，能进一步优化
                }
            }
            for (auto& p : buff) {
                int i = p.first, j = p.second;
                if (i - 1 >= 0)
                    if (grid[i - 1][j] == 1) grid[i - 1][j] = 2;
                if (i + 1 < grid.size())
                    if (grid[i + 1][j] == 1) grid[i + 1][j] = 2;
                if (j - 1 >= 0)
                    if (grid[i][j - 1] == 1) grid[i][j - 1] = 2;
                if (j + 1 < grid[0].size())
                    if (grid[i][j + 1] == 1) grid[i][j + 1] = 2;
            }

            cnt_aft = calCnt(grid);
            if (cnt_bfr == cnt_aft) break;

            cnt_bfr = cnt_aft;
            step++;
        }
        if (cnt_aft)
            return -1;
        else
            return step;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=234 lang=cpp
 *
 * [234] 回文链表
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    bool isPalindrome(ListNode *head) {
        std::vector<int> vec;
        while (head) {
            vec.push_back(head->val);
            head = head->next;
        }

        for (int i = 0; i < vec.size() / 2; i++) {
            if (vec[i] != vec[vec.size() - 1 - i]) return false;
        }

        return true;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=215 lang=cpp
 *
 * [215] 数组中的第K个最大元素
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

// @lc code=start

class Solution {
public:
    int findKthLargest(vector<int>& nums, int k) {
        // quick select
        // std::nth_element(nums.begin(), nums.begin() + k - 1, nums.end(), std::greater<int>());
        // return nums[k - 1];

        // Heap
        std::priority_queue<int, std::vector<int>, std::greater<int>> heap;
        for (auto& n : nums) {
            // heap.push(n);
            // if (heap.size() > k) heap.pop();

            if (heap.size() < k)
                heap.push(n);
            else if (n > heap.top()) {
                heap.pop();
                heap.push(n);
            }
        }
        return heap.top();
    }
};
// @lc code=end

int main() {
    Solution so;
    vector<int> nums = {-1, 2, 0};
    std::cout << so.findKthLargest(nums, 3);
}
/*
 * @lc app=leetcode.cn id=438 lang=cpp
 *
 * [438] 找到字符串中所有字母异位词
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> findAnagrams(string s, string p) {
        if (s.size() < p.size()) return {};

        // 维护滑窗,统计频次
        std::vector<int> scnt(26, 0), pcnt(26, 0), res;

        int si = 0, pi = 0;
        for (auto& c : p) {
            pcnt[c - 'a']++;
            scnt[s[si++] - 'a']++;  // 初始填充
        }
        if (scnt == pcnt) res.push_back(0);

        while (si < s.size()) {
            scnt[s[si++] - 'a']++;
            scnt[s[pi++] - 'a']--;
            if (scnt == pcnt) res.push_back(pi);
        }
        return res;
    }
};
// @lc code=end

int main() {
    Solution solute;
    string s = "cbaebabacd", p = "abc";
    vector<int> res = solute.findAnagrams(s, p);
    for (auto& n : res) {
        cout << n << " ";
    }

    // cout << result << endl;

    return 0;
}

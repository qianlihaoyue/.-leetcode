/*
 * @lc app=leetcode.cn id=217 lang=cpp
 *
 * [217] 存在重复元素
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    // 经典map计数
    bool containsDuplicate(vector<int>& nums) {
        // 计数法
        // set<int> mp;
        // for (auto& n : nums) {
        //     if (mp.find(n) != mp.end()) return true;
        //     mp.insert(n);
        // }
        // return false;

        // 通过set去重
        return set<int>(nums.begin(), nums.end()).size() != nums.size();
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=224 lang=cpp
 *
 * [224] 基本计算器
 */

#include <stack>
#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    // int substrSum(string &s, int start, int end) {
    //     int sum = 0;
    //     int val = 0;
    //     bool nega_flg = false;
    //     for (int i = start; i < end; i++) {
    //         if (s[i] == ' ') continue;
    //         // 累计val
    //         if (s[i] >= '0' && s[i] <= '9') {
    //             val = val * 10 + s[i] - '0';
    //         } else {
    //             sum += val * (nega_flg ? -1 : 1);
    //             val = 0;
    //             if (s[i] == '-') nega_flg = true;
    //         }
    //         s[i] = ' ';  // 已计算完毕
    //     }
    //     return sum;
    // }

    int calculate(string s) {
        std::stack<int> st_idx;

        bool nega_flg = false;  // 指示
        bool num_flg = false;

        int val = 0;
        int sum = 0;

        for (int i = 0; i < s.size(); i++) {
            if (s[i] == '(')
                st_idx.push(i);
            else if (s[i] == ')') {
                int start = st_idx.top();
                st_idx.pop();
                string numstr = s.substr(start, i - start);
                val = std::stoi(numstr);
            }
        }
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=740 lang=cpp
 *
 * [740] 删除并获得点数
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int deleteAndEarn(vector<int>& nums) {
        // 转为打家劫舍问题
        // sort(nums.begin(), nums.end());
        // vector<int> vals(nums.back() + 1, 0);

        int max_v = *std::max_element(nums.begin(), nums.end());
        vector<int> vals(max_v + 1, 0);
        for (auto& n : nums) vals[n] += n;

        if (max_v < 2) return vals[max_v];

        // 打家劫舍问题
        vector<int> dp(max_v + 1, 0);
        dp[1] = vals[1];
        for (int i = 2; i <= max_v; i++) {
            dp[i] = max(dp[i - 2] + vals[i], dp[i - 1]);
        }
        return dp[max_v];
    }
};
// @lc code=end

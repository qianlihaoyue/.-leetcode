/*
 * @lc app=leetcode.cn id=60 lang=cpp
 *
 * [60] 排列序列
 */

#include <algorithm>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    string getPermutation(int n, int k) {
        string str;
        for (int i = 1; i <= n; i++) str.push_back(i + '0');
        int cnt = 0;
        do {
            if (++cnt == k) break;
        } while (my_next_permutation(str));
        // while (std::next_permutation(str.begin(), str.end()));
        return str;
    }

    // bool my_next_permutation(vector<int>& nums) {
    bool my_next_permutation(string& nums) {
        // 太短不用排
        if (nums.size() <= 1) return false;

        // 从后往前找第一个非递增的元素
        int i = nums.size() - 2;
        while (i >= 0 && nums[i] >= nums[i + 1]) i--;

        // 如果找不到非递增的元素，说明当前排列已经是最后一个排列，返回 false
        if (i < 0) return false;

        // 从后往前找第一个比 nums[i] 大的元素
        int j = nums.size() - 1;
        while (j > i && nums[j] <= nums[i]) j--;

        swap(nums[i], nums[j]);
        // 将下标 i+1 之后的元素逆序排列
        reverse(nums.begin() + i + 1, nums.end());

        return true;
    }
};
// @lc code=end

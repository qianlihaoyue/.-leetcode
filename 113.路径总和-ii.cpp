/*
 * @lc app=leetcode.cn id=113 lang=cpp
 *
 * [113] 路径总和 II
 */

#include <bits/stdc++.h>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 从上往下传递
    vector<vector<int>> ans;
    vector<int> path;

    // 标准回溯
    // void dfs(TreeNode* root, int targetSum, int val) {
    //     if (!root->left && !root->right) {
    //         if (val + root->val == targetSum) ans.push_back(path);
    //     }
    //     int v = val + root->val;
    //     // if (v > targetSum) return;

    //     if (root->left) {
    //         path.push_back(root->left->val);
    //         dfs(root->left, targetSum, v);
    //         path.pop_back();
    //     }
    //     if (root->right) {
    //         path.push_back(root->right->val);
    //         dfs(root->right, targetSum, v);
    //         path.pop_back();
    //     }
    // }

    void dfs(TreeNode* root, int targetSum) {
        if (!root->left && !root->right)
            if (root->val == targetSum) ans.push_back(path);

        if (root->left) {
            path.push_back(root->left->val);
            dfs(root->left, targetSum - root->val);
            path.pop_back();
        }
        if (root->right) {
            path.push_back(root->right->val);
            dfs(root->right, targetSum - root->val);
            path.pop_back();
        }
    }
    vector<vector<int>> pathSum(TreeNode* root, int targetSum) {
        if (!root) return ans;
        path.push_back(root->val);
        dfs(root, targetSum);
        return ans;
    }
};
// @lc code=end

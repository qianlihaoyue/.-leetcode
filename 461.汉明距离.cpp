/*
 * @lc app=leetcode.cn id=461 lang=cpp
 *
 * [461] 汉明距离
 */

#include <algorithm>
#include <iostream>

#include <iterator>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    int hammingDistance(int x, int y) {
        // | 为1 但&为0的不同区域 ，即异或操作，相同为0，不同为1，然后通过移位&1 进行统计
        int xy = x ^ y;
        int cnt = 0;
        while (xy) {
            cnt += xy & 1;
            xy >>= 1;
        }

        return cnt;

        // uint32_t z = x ^ y;
        // z = (z & 0x55555555) + ((z >> 1) & 0x55555555);
        // z = (z & 0x33333333) + ((z >> 2) & 0x33333333);
        // z = (z & 0x0f0f0f0f) + ((z >> 4) & 0x0f0f0f0f);
        // z = (z & 0x00ff00ff) + ((z >> 8) & 0x00ff00ff);
        // z = (z & 0x0000ffff) + ((z >> 16) & 0x0000ffff);
        // return (int)z;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=404 lang=cpp
 *
 * [404] 左叶子之和
 */

#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // my 100% 81%
    void recurpre(TreeNode* root, int& sum, bool flg_left) {
        if (!root->left && !root->right) {
            if (flg_left) sum += root->val;
            return;
        }
        if (root->left) recurpre(root->left, sum, true);
        if (root->right) recurpre(root->right, sum, false);
    }
    int sumOfLeftLeaves(TreeNode* root) {
        int sum = 0;
        if (root->left) recurpre(root->left, sum, true);
        if (root->right) recurpre(root->right, sum, false);
        return sum;
    }

    // 抄的，更精简，但效率不如我写的，不知道为什么  67% 57%
    // int sumOfLeftLeaves(TreeNode* root) {
    //     if (!root) return 0;
    //     // 存在左节点，并且是个叶子，加上该叶子的值
    //     if (root->left && !root->left->left && !root->left->right) {
    //         return root->left->val + sumOfLeftLeaves(root->right);
    //     }
    //     // 初次把左右两个节点都加上
    //     return sumOfLeftLeaves(root->left) + sumOfLeftLeaves(root->right);
    // }
};
// @lc code=end

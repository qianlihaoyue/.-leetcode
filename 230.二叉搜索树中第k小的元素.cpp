/*
 * @lc app=leetcode.cn id=230 lang=cpp
 *
 * [230] 二叉搜索树中第K小的元素
 */

#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int cnt = 0;
    // int kthSmallest(TreeNode *root, int k) {
    //     if (root) {
    //         // 中序遍历
    //         int res = kthSmallest(root->left, k);
    //         if (res) return res;
    //         if (++cnt == k) return root->val;
    //         res = kthSmallest(root->right, k);
    //         if (res) return res;
    //     }
    //     return 0;
    // }

    // 也可以中序，然后保存第k位

    int ans = 0;
    void travel_mid(TreeNode *root, int k) {
        if (!root || cnt > k) return;
        travel_mid(root->left, k);
        if (++cnt == k) ans = root->val;
        travel_mid(root->right, k);
    }

    int kthSmallest(TreeNode *root, int k) {
        travel_mid(root, k);
        return ans;
    }
};
// @lc code=end

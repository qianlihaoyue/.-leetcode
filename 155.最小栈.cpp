/*
 * @lc app=leetcode.cn id=155 lang=cpp
 *
 * [155] 最小栈
 */

#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <functional>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>
using namespace std;

// @lc code=start
class MinStack {
public:
    MinStack() {}

    ////////////////////// 维护双stack  66%-21%
    void push(int val) {
        st.push(val);
        if (min_st.empty() || min_st.top() > val)
            min_st.push(val);
        else
            min_st.push(min_st.top());  // 只压入当前最小值
    }

    void pop() {
        st.pop();
        min_st.pop();
    }

    int top() { return st.top(); }
    int getMin() { return min_st.top(); }

private:
    stack<int> st;
    stack<int> min_st;

    ////////////////////// 使用 multiset 34%-5%

    //     void push(int val) {
    //         data.push(val);
    //         mymap.insert(val);
    //     }

    //     void pop() {
    //         mymap.erase(mymap.lower_bound(data.top()));
    //         data.pop();
    //     }

    //     int top() { return data.top(); }

    //     int getMin() { return *mymap.begin(); }

    // private:
    //     std::stack<int> data;
    //     std::multiset<int> mymap;
};

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(val);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */
// @lc code=end

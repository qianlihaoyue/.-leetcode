/*
 * @lc app=leetcode.cn id=208 lang=cpp
 *
 * [208] 实现 Trie (前缀树)
 */

#include <algorithm>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

// @lc code=start

class Trie {
public:
    struct Node {
        bool isWord = false;
        vector<Node*> children;
        Node() { children.resize(26); }
    };

    Node* root;
    Trie() { root = new Node(); }

    void insert(string word) {
        Node* cur = root;
        for (auto& c : word) {
            int i = c - 'a';
            if (cur->children[i] == nullptr) cur->children[i] = new Node();
            cur = cur->children[i];
        }
        cur->isWord = true;
    }

    bool search(string word) {
        Node* cur = root;
        for (auto& c : word) {
            if (cur->children[c - 'a'] == nullptr) return false;
            cur = cur->children[c - 'a'];
        }
        return cur->isWord;
    }

    bool startsWith(string prefix) {
        Node* cur = root;
        for (auto& c : prefix) {
            if (cur->children[c - 'a'] == nullptr) return false;
            cur = cur->children[c - 'a'];
        }
        return true;
    }
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */
// @lc code=end

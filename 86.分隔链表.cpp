/*
 * @lc app=leetcode.cn id=86 lang=cpp
 *
 * [86] 分隔链表
 */

#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* partition(ListNode* head, int x) {
        // 非原地操作
        ListNode* dummys = new ListNode();
        ListNode* dummyl = new ListNode();
        ListNode *curs = dummys, *curl = dummyl;

        while (head) {
            if (head->val < x) {
                curs->next = head;
                curs = curs->next;
            } else {
                curl->next = head;
                curl = curl->next;
            }
            head = head->next;
        }
        curl->next = nullptr;
        curs->next = dummyl->next;
        return dummys->next;
    }
};
// @lc code=end

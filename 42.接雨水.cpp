/*
 * @lc app=leetcode.cn id=42 lang=cpp
 *
 * [42] 接雨水
 */

#include <algorithm>
#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int trap(vector<int>& height) {
        ///////////////// 前后缀 最大值  O(n)  O(n)
        // vector<int> pre_max(height.size());
        // pre_max[0] = height[0];
        // for (int i = 1; i < height.size(); i++) pre_max[i] = max(pre_max[i - 1], height[i]);

        // vector<int> suf_max(height.size());
        // suf_max.back() = height.back();
        // for (int i = height.size() - 2; i >= 0; i--) suf_max[i] = max(suf_max[i + 1], height[i]);

        // int sum = 0;
        // for (int i = 0; i < height.size(); i++) sum += min(pre_max[i], suf_max[i]) - height[i];

        // return sum;

        /////////////// O(n)  O(1)
        int sum = 0, pre_max = height[0], suf_max = height.back();
        int l = 1, r = height.size() - 1;

        while (l <= r) {
            pre_max = max(pre_max, height[l]);
            suf_max = max(suf_max, height[r]);
            if (pre_max > suf_max)
                sum += suf_max - height[r--];
            else
                sum += pre_max - height[l++];
        }
        return sum;
        
    }
};
// @lc code=end

// int floor = 0;
// int sum = 0;
// int max_floor = 0;
// int sec_max_floor = 0;

// int start = 0, end = height.size();

// while (1) {
//     int val = 0;
//     int start_idx = -1;

//     // 缩小开始点
//     for (int i = start; i < end; i++) {
//         if (height[i] > floor)
//             break;
//         else
//             start = i;
//     }

//     for (int i = start; i < end; i++) {
//         if (floor == 0) max_floor = (max_floor > height[i]) ? max_floor : height[i];
//         if (floor == 1) {
//             if (height[i] > sec_max_floor && height[i] < max_floor) sec_max_floor = height[i];
//         }
//         if (height[i] > floor) {
//             if (start_idx != -1) val += (i - start_idx - 1);
//             start_idx = i;
//         }
//     }
//     // 缩小终止点
//     for (int i = end - 1; i > start; i--) {
//         if (height[i] > floor)
//             break;
//         else
//             end = i;
//     }
//     sum += val;
//     floor++;

//     // 跳出条件
//     if (val == 0 && sec_max_floor && floor > sec_max_floor) break;
//     if (end - start < 3) break;
// }
// return sum;
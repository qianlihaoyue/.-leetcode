/*
 * @lc app=leetcode.cn id=128 lang=cpp
 *
 * [128] 最长连续序列
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

// @lc code=start
class Solution {
public:
    int longestConsecutive(vector<int>& nums) {
        if (nums.size() < 2) return nums.size();

        ////////////////// sort 95-90
        sort(nums.begin(), nums.end());
        int max_cnt = 1, cnt = 1;
        for (int i = 1; i < nums.size(); i++) {
            if (nums[i] == nums[i - 1] + 1)
                cnt++;
            else if (nums[i] != nums[i - 1]) {
                max_cnt = max(max_cnt, cnt);
                cnt = 1;
            }
        }

        return max(max_cnt, cnt);

        ////////////// 使用set排序
        // set<int> myset(nums.begin(), nums.end());
        // int max_cnt = 1, cnt = 1;
        // int last = *myset.begin();
        // for (auto it = ++myset.begin(); it != myset.end(); ++it) {
        //     if (*it == last + 1) {
        //         cnt++;
        //     } else {
        //         max_cnt = max(max_cnt, cnt);
        //         cnt = 1;
        //     }
        //     last = *it;
        // }
        // return max(max_cnt, cnt);

        ////////////// 使用 hash table
        // unordered_set<int> myhash(nums.begin(), nums.end());
        // int max_cnt = 1;
        // for (const int& n : nums) {
        //     // 确保这是起点，加速
        //     if (myhash.count(n - 1) == 0) {
        //         int tmp = n;
        //         int cnt = 1;
        //         while (myhash.count(++tmp)) cnt++;
        //         max_cnt = max(max_cnt, cnt);
        //     }
        // }
        // return max_cnt;
    }
};
// @lc code=end

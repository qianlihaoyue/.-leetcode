/*
 * @lc app=leetcode.cn id=3 lang=cpp
 *
 * [3] 无重复字符的最长子串
 */

#include <algorithm>
#include <unordered_map>
#include <vector>
#include <iostream>
#include <map>
using namespace std;

// @lc code=start
class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        ////////////// 滑动优化
        int max_len = 0;
        int len = 0;
        std::vector<int> hash_map(128, 0);

        // for (int i = 0; i < s.size(); i++) {
        //     // 之前没有索引
        //     if (!hash_map[s[i]]) {
        //         len++;
        //     } else {
        //         max_len = max(max_len, len);
        //         // 逐个进行删除，直到相同
        //         for (int j = i - len; j < i; j++) {
        //             if (s[j] == s[i]) {
        //                 break;
        //             } else {
        //                 hash_map[s[j]] = 0;
        //                 len--;
        //             }
        //         }
        //     }
        //     hash_map[s[i]] = i + 1;
        // }

        // return max(max_len, len);

        // 滑窗--子串  O(n) O(1)
        int start = 0;
        for (int i = 0; i < s.size(); i++) {
            hash_map[s[i]]++;
            while (hash_map[s[i]] > 1) hash_map[s[start++]] -= 1;
            max_len = max(max_len, i - start + 1);
        }
        return max_len;
    }
};
// @lc code=end

int main() {
    Solution solu;
    std::cout << "l: " << solu.lengthOfLongestSubstring("abba") << std::endl;

    return 0;
}

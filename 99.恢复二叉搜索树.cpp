/*
 * @lc app=leetcode.cn id=99 lang=cpp
 *
 * [99] 恢复二叉搜索树
 */

#include <algorithm>
#include <fstream>
#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void dfs(TreeNode* root, std::vector<TreeNode*>& vec) {
        if (!root) return;
        dfs(root->left, vec);
        vec.push_back(root);
        dfs(root->right, vec);
    }
    void recoverTree(TreeNode* root) {
        std::vector<TreeNode*> vec;
        dfs(root, vec);

        vector<int> idx;
        for (int i = 0; i < vec.size() - 1; i++)
            if (vec[i]->val > vec[i + 1]->val) idx.push_back(i);

        // 如果有一个逆序对
        if (idx.size() == 1)
            swap(vec[idx[0]]->val, vec[idx[0] + 1]->val);
        else
            swap(vec[idx[0]]->val, vec[idx[1] + 1]->val);
    }
};
// @lc code=end

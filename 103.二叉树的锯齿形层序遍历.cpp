/*
 * @lc app=leetcode.cn id=103 lang=cpp
 *
 * [103] 二叉树的锯齿形层序遍历
 */

#include <algorithm>
#include <queue>
#include <vector>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> zigzagLevelOrder(TreeNode *root) {
        vector<vector<int>> ans;
        if (!root) return ans;
        vector<TreeNode *> vec;
        vec.push_back(root);
        int depth = 0;
        while (vec.size()) {
            vector<TreeNode *> vec_nxt;
            vector<int> vals;
            for (auto &node : vec) {
                vals.push_back(node->val);
                if (node->left) vec_nxt.push_back(node->left);
                if (node->right) vec_nxt.push_back(node->right);
            }

            if (depth % 2) std::reverse(vals.begin(), vals.end());
            depth++;
            vec = vec_nxt;
            ans.push_back(vals);
        }
        return ans;
    }
};
// @lc code=end

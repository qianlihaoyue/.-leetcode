/*
 * @lc app=leetcode.cn id=122 lang=cpp
 *
 * [122] 买卖股票的最佳时机 II
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <ratio>
#include <type_traits>
#include <utility>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>
#include <queue>
using namespace std;

// @lc code=start
class Solution {
public:
    // 也就是最大上升沿。这个模型直接获取所有上升沿就行
    int maxProfit(vector<int>& prices) {
        int sum = 0;
        // for (int i = 1; i < prices.size(); i++) sum += max(prices[i] - prices[i - 1], 0);
        // 炒短线，取上升沿
        for (int i = 1; i < prices.size(); i++) {
            int diff = prices[i] - prices[i - 1];
            if (diff > 0) sum += diff;
        }
        return sum;
    }
};
// @lc code=end

int main() {
    Solution solute;

    // vector<int> g = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
    // vector<int> g = {5, 4, -1, 7, 8};
    vector<int> g = {1, 2, 3, 4, 5};
    auto result = solute.maxProfit(g);


    cout << endl << result << endl;

    return 0;
}

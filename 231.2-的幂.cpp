/*
 * @lc app=leetcode.cn id=231 lang=cpp
 *
 * [231] 2 的幂
 */

#include <algorithm>
#include <iostream>

#include <iterator>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    bool isPowerOfTwo(int n) {
        // if (n <= 0) return false;
        // if (n == 1 || n == 2) return true;

        // while (n > 2) {
        //     if (n % 2) return false;
        //     n /= 2;
        // }

        // return true;

        // 递归
        // if (n == 1)
        //     return true;
        // else if (n < 1)
        //     return false;
        // else
        //     return isPowerOfTwo(n / 2);  // 需要改形参类型 double

        // 位运算符
        // 2的幂次方在二进制下，只有1位是1，其余全是0
        // return (n > 0) && (n & -n) == n;
        // 减1之后的二进制就是原来的1变成0，后面的全为1
        return (n > 0) && ((n & (n - 1)) == 0);
    }
};
// @lc code=end

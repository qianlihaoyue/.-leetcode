/*
 * @lc app=leetcode.cn id=11 lang=cpp
 *
 * [11] 盛最多水的容器
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <map>
using namespace std;

// @lc code=start
class Solution {
public:
    // time:O(n)  space:O(1)
    int maxArea(vector<int>& height) {
        // 也就是找到两个数，max(索引差 * min(h))
        // 若向内 移动短板 ，水槽的短板可能变大，  因此下个水槽的面积 可能增大 。
        // 若向内 移动长板 ，水槽的短板不变或变小，因此下个水槽的面积 一定变小
        // 从外往内
        int l = 0, r = height.size() - 1;
        int max_a = 0;

        while (l < r) {
            // int cur = (r - l) * min(height[l], height[r]);
            // max_a = max(max_a, cur);
            // if (height[l] < height[r])
            //     ++l;
            // else
            //     --r;

            if (height[l] < height[r]) {
                max_a = max(max_a, (r - l) * height[l]);
                ++l;
            } else {
                max_a = max(max_a, (r - l) * height[r]);
                --r;
            }
        }
        return max_a;

        // int max_res = 0;
        // int left = 0, right = height.size() - 1;
        // while (left < right) {
        //     max_res = std::max(max_res, (right - left) * min(height[left], height[right]));
        //     if (height[left] < height[right])
        //         left++;
        //     else
        //         right--;
        // }
        // return max_res;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=219 lang=cpp
 *
 * [219] 存在重复元素 II
 */

#include <algorithm>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    bool containsNearbyDuplicate(vector<int>& nums, int k) {
        if (k == 0) return false;

        unordered_map<int, int> mymap;
        for (int i = 0; i < nums.size(); i++) {
            auto it = mymap.find(nums[i]);
            if (it != mymap.end()) {
                if (i - it->second <= k) return true;
            }
            mymap[nums[i]] = i;  // 更新新位置
        }
        return false;
    }
};
// @lc code=end

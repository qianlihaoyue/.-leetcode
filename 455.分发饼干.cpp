/*
 * @lc app=leetcode.cn id=455 lang=cpp
 *
 * [455] 分发饼干
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <type_traits>
#include <utility>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>
#include <queue>
using namespace std;

// @lc code=start
class Solution {
public:
    int findContentChildren(vector<int>& g, vector<int>& s) {
        sort(g.begin(), g.end(), std::greater<int>());
        sort(s.begin(), s.end(), std::greater<int>());

        int cnt = 0;
        for (int i = 0, j = 0; i < g.size() && j < s.size(); i++) {
            if (s[j] >= g[i]) {
                cnt++;
                j++;
            }
        }
        return cnt;
    }
};
// @lc code=end

int main() {
    Solution solute;

    vector<int> g = {1, 2}, s = {1, 2, 3};
    auto result = solute.findContentChildren(g, s);

    // for (auto& n : result) cout << n << " ";

    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    cout << endl << result << endl;

    return 0;
}

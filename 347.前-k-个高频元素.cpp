/*
 * @lc app=leetcode.cn id=347 lang=cpp
 *
 * [347] 前 K 个高频元素
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    // 自定义比较函数,必须传递函数对象

    // class 默认是私有的
    struct cmp {
        bool operator()(const pair<int, int>& a, const pair<int, int>& b) { return a.second > b.second; }
    };

    vector<int> topKFrequent(vector<int>& nums, int k) {
        map<int, int> record;
        for (auto& n : nums) record[n]++;

        priority_queue<pair<int, int>, vector<pair<int, int>>, cmp> heap;
        for (auto it = record.begin(); it != record.end(); it++) {
            heap.push(*it);
            if (heap.size() > k) heap.pop();
        }

        vector<int> result(k);
        for (int i = k - 1; i >= 0; i--) {
            result[i] = heap.top().first;
            heap.pop();
        }
        return result;

        // map<int, int> v;
        // for (auto& n : nums) v[n]++;
        // vector<int> tmp;
        // for (auto it = v.begin(); it != v.end(); it++) tmp.push_back(it->second);

        // nth_element(tmp.begin(), tmp.begin() + k - 1, tmp.end(), greater<int>());
        // int min_cnt = tmp[k - 1];

        // vector<int> result;
        // for (auto it = v.begin(); it != v.end(); it++)
        //     if (it->second >= min_cnt) result.push_back(it->first);

        // return result;
    }
};
// @lc code=end

int main() {
    Solution solute;

    vector<int> nums = {1, 1, 1, 2, 2, 3};
    auto result = solute.topKFrequent(nums, 2);

    for (auto& n : result) cout << n << " ";

    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

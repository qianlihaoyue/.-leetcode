/*
 * @lc app=leetcode.cn id=141 lang=cpp
 *
 * [141] 环形链表
 */

#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        // 通过缩减来判断是否能 自指

        // 标记法，显然新建一个bool更恰当
        // const int kFlg = 10e6;
        // while (head) {
        //     if (head->val > 10e5) return true;
        //     head->val += kFlg;
        //     head = head->next;
        // }
        // return false;

        // 快慢指针法
        ListNode *slow = head, *fast = head;
        while (1) {
            if (!fast || !fast->next) return false;
            slow = slow->next, fast = fast->next->next;
            if (slow == fast) return true;
        }
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=739 lang=cpp
 *
 * [739] 每日温度
 */

#include <algorithm>
#include <stack>
#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        // 单调栈 O(n) O(n)，从右到左
        auto& tm = temperatures;
        vector<int> ans(tm.size(), 0);
        stack<int> st;  // 栈中存储索引
        // 倒序遍历
        for (int i = tm.size() - 1; i >= 0; --i) {
            // 当栈中有元素，进行排除，以符合单调性
            while (st.size() && tm[i] >= tm[st.top()]) st.pop();
            // 如果仍然有元素，代表栈中较大值
            if (st.size()) ans[i] = st.top() - i;
            // 压入当前
            st.push(i);
        }
        return ans;

        // // 从左到右
        // for (int i = 0; i < temp.size(); i++) {
        //     while (st.size() && temp[i] > temp[st.top()]) {
        //         ans[st.top()] = i - st.top();
        //         st.pop();
        //     }
        //     st.push(i);
        // }

        // return ans;
    }
};
// @lc code=end

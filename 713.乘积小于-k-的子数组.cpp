/*
 * @lc app=leetcode.cn id=713 lang=cpp
 *
 * [713] 乘积小于 K 的子数组
 */

#include <algorithm>
#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int numSubarrayProductLessThanK(vector<int>& nums, int k) {
        int prod = 1, start = 0, cnt = 0;
        for (int i = 0; i < nums.size(); i++) {
            prod *= nums[i];
            while (prod >= k && start < i) prod /= nums[start++];
            // 到i为止，且必须包括i，前面的都是子数组
            if (prod < k) cnt += (i - start + 1);
        }
        return cnt;
    }
};
// @lc code=end

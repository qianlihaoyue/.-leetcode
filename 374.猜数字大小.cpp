/*
 * @lc app=leetcode.cn id=374 lang=cpp
 *
 * [374] 猜数字大小
 */

#include <string>
#include <vector>
using namespace std;

// @lc code=start
/**
 * Forward declaration of guess API.
 * @param  num   your guess
 * @return 	     -1 if num is higher than the picked number
 *			      1 if num is lower than the picked number
 *               otherwise return 0
 * int guess(int num);
 */

class Solution {
public:
    int guessNumber(int n) {
        int l = 1, r = n;
        // int mid = (l + r) >> 1;
        int mid = l + (r - l) / 2;
        while (l < r) {
            switch (guess(mid)) {
                case 0:
                    return mid;
                case 1:
                    l = mid + 1;
                    break;
                case -1:
                    r = mid - 1;
                    break;
            }
            mid = l + (r - l) / 2;
        }
        return mid;
    }
};
// @lc code=end

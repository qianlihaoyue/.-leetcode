/*
 * @lc app=leetcode.cn id=80 lang=cpp
 *
 * [80] 删除有序数组中的重复项 II
 */

#include <algorithm>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        // 双指针
        // int slow = 0;
        // int last_val = 10e4 + 1, cnt = 0;
        // for (int fast = 0; fast < nums.size(); fast++) {
        //     if (nums[fast] == last_val) {
        //         if (++cnt >= 2) continue;
        //     } else {
        //         cnt = 0;
        //         last_val = nums[fast];
        //     }
        //     nums[slow++] = nums[fast];
        // }
        // nums.resize(slow);
        // return slow;

        // 利用有序性
        if (nums.size() < 3) return nums.size();
        int slow = 2;
        for (int fast = 2; fast < nums.size(); fast++)
            if (nums[fast] != nums[slow - 2]) nums[slow++] = nums[fast];
        return slow;
    }
};
// @lc code=end

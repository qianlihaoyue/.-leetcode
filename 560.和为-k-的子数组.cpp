/*
 * @lc app=leetcode.cn id=560 lang=cpp
 *
 * [560] 和为 K 的子数组
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

// @lc code=start
class Solution {
public:
    // 不可以用 双指针/滑动窗口：
    // 因为 nums[i] 可以小于 0
    // - 也就是说右指针 i 向后移 1 位不能保证区间会增大
    // - 左指针 j 向后移 1 位也不能保证区间和会减小
    // 给定 j，i 的位置没有二段性
    int subarraySum(vector<int>& nums, int k) {
        // // 滑窗不行
        // int sum = 0, cnt = 0, left = 0, right = 0;

        // while (right < nums.size()) {
        //     sum += nums[right++];
        //     if (sum == k) cnt++;
        //     while (((sum > k && nums[left] > 0) || (sum < k && nums[left] < 0)) && left < right - 1) {
        //         sum -= nums[left++];
        //         if (sum == k) cnt++;
        //     }
        // }
        // return cnt;

        int cnt = 0;

        // // 前缀和 + 暴力解法
        // vector<int> sum(nums.begin(), nums.end());
        // for (int i = 1; i < nums.size(); i++) sum[i] += sum[i - 1];
        // for (int i = 0; i < nums.size(); i++) {
        //     if (sum[i] == k) cnt++;
        //     for (int j = 0; j < i; j++)
        //         if (sum[i] - sum[j] == k) cnt++;
        // }

        // 暴力
        // for (int i = 0; i < nums.size(); ++i) {
        //     int sum = 0;
        //     for (int j = i; j < nums.size(); ++j) {
        //         sum += nums[j];
        //         cnt += sum == k ? 1 : 0;
        //     }
        // }

        // 哈希
        // 遍历nums，计算从0到当前元素的和，哈希保存累积和sum的次数
        // 如果sum - k在哈希表中出现过，则代表从当前下标 往前有连续的子数组的和为sum。
        int sum = 0;
        unordered_map<int, int> cul;
        cul[0] = 1;
        for (auto& m : nums) {
            sum += m;
            cnt += cul[sum - k];
            cul[sum]++;
        }

        return cnt;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=98 lang=cpp
 *
 * [98] 验证二叉搜索树
 */

#include <algorithm>
#include <climits>
#include <cstdint>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 1.dfs
    // bool check(TreeNode* root, long left, long right) {
    //     if (!root) return true;
    //     if (root->val <= left || (root->val >= right)) return false;
    //     return check(root->left, left, root->val) && check(root->right, root->val, right);
    // }

    // 3.先展开，再直接判单调
    // void dfs(TreeNode* root, std::vector<int>& vec) {
    //     if (!root) return;
    //     dfs(root->left, vec);
    //     vec.push_back(root->val);  // 中序遍历
    //     dfs(root->right, vec);
    // }

    // 2.中序遍历，即为按照升序排列，需要定义全局变量
    long pre = LONG_MIN;

    bool isValidBST(TreeNode* root) {
        // 2.中序遍历
        if (!root) return true;
        if (!isValidBST(root->left)) return false;
        if (root->val <= pre) return false;
        pre = root->val;
        return isValidBST(root->right);

        // 1.dfs
        // return check(root, LONG_MIN, LONG_MAX);

        // 3.先展开，再直接判单调
        // std::vector<int> vec;
        // dfs(root, vec);
        // for (int i = 0; i < vec.size() - 1; i++)
        //     if (vec[i] >= vec[i + 1]) return false;
        // return true;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=142 lang=cpp
 *
 * [142] 环形链表 II
 */

#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(nullptr) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *detectCycle(ListNode *head) {
        // 标记法，不允许修改链表
        // const int kFlg = 10e6;

        // 287.快慢指针
        ListNode *slow = head, *fast = head;

        while (1) {
            // 如果fast走到尽头，说明不存在环
            if (!fast || !fast->next) return nullptr;
            slow = slow->next, fast = fast->next->next;
            if (fast == slow) break;
        }
        // 当相遇时，说明存在环
        // 令fast或slow回到起点，然后同步每次走一格，直到再次相遇
        slow = head;
        while (slow != fast) slow = slow->next, fast = fast->next;
        return slow;
    }
};
// @lc code=end

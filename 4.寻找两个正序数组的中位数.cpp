/*
 * @lc app=leetcode.cn id=4 lang=cpp
 *
 * [4] 寻找两个正序数组的中位数
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
        int n = nums1.size(), m = nums2.size();
        int idx = (n + m) / 2;

        // int left = -1, right = -1;
        // int idx1 = 0, idx2 = 0;
        // for (int i = 0; i <= idx; i++) {
        //     left = right;
        //     if (idx1 < m && (idx2 >= n || nums1[idx1] < nums2[idx2])) {
        //         right = nums1[idx1++];
        //     } else {
        //         right = nums2[idx2++];
        //     }
        // }
        // if ((n + m) % 2 == 0)
        //     return (left + right) / 2.0;
        // else
        //     return right;

        // O(m+n)
        nums1.resize(n + m);
        int idx1 = n - 1, idx2 = m - 1;
        for (int i = n + m - 1; i >= 0; i--) {
            if (i < idx - 1) break;
            if (idx2 < 0) break;
            if (idx1 >= 0 && nums1[idx1] > nums2[idx2])
                nums1[i] = nums1[idx1--];
            else
                nums1[i] = nums2[idx2--];
        }

        if ((n + m) % 2)
            return nums1[idx];
        else
            return (double)(nums1[idx] + nums1[idx - 1]) / 2.0;
    }
};
// @lc code=end

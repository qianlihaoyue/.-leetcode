/*
 * @lc app=leetcode.cn id=69 lang=cpp
 *
 * [69] x 的平方根
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    int mySqrt(int x) {
        // rename f = x^2 + S ,然后泰勒展开=0, 数值分析: 牛顿迭代法
        // f(x) = f(x_0) + f'(x_0)(x-x_0) = 0
        // x = x_0 - f(x_0) / f'(x_0)

        // double last_val = 0;
        // double val = (double)x / 2;  // 取x/2作为迭代初值
        // while (std::abs(val - last_val) > 0.4) {
        //     last_val = val;
        //     val = 0.5 * (val + x / val);
        // }
        // return (int)val;

        ////////////////////////////////////////////////////////////////

        // 二分法，根据频率优化
        if (x == 1) return 1;
        int l = 0, r = x;
        while (l < r - 1) {
            int mid = l + (r - l) / 2;
            if (x / mid < mid)  // 使用这种方法可以避免越界
                r = mid;
            else
                l = mid;
        }
        return l;

        ////////////////////////////////////////////////////////////////

        // 因为本题只要求整数精度，步长加速法
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=380 lang=cpp
 *
 * [380] O(1) 时间插入、删除和获取随机元素
 */

#include <algorithm>
#include <any>
#include <cstdlib>
#include <functional>
#include <unordered_set>
#include <vector>
using namespace std;

// 可以使用 unorder_set实现

// @lc code=start
class RandomizedSet {
public:
    RandomizedSet() {}

    bool insert(int val) {
        if (umap.find(val) != umap.end())
            return false;
        else {
            umap[val] = uvec.size();
            uvec.push_back(val);
            return true;
        }
    }

    bool remove(int val) {
        auto iter = umap.find(val);
        if (iter != umap.end()) {
            int idx = iter->second;
            // 需要删除的是 second 对应的
            swap(uvec[idx], uvec[uvec.size() - 1]);
            // 此时 uvec[idx] 以及是之前的末尾，修改其位置
            umap[uvec[idx]] = idx;

            umap.erase(iter);
            uvec.pop_back();

            return true;
        } else {
            return false;
        }
    }

    int getRandom() { return uvec[rand() % umap.size()]; }

private:
    unordered_map<int, int> umap;
    vector<int> uvec;
};

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet* obj = new RandomizedSet();
 * bool param_1 = obj->insert(val);
 * bool param_2 = obj->remove(val);
 * int param_3 = obj->getRandom();
 */
// @lc code=end

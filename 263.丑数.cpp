/*
 * @lc app=leetcode.cn id=263 lang=cpp
 *
 * [263] 丑数
 */

// @lc code=start
class Solution {
public:
    bool isUgly(int n) {
        if (n <= 0) return false;
        while (true) {
            if (n % 2 == 0)
                n /= 2;
            else if (n % 3 == 0)
                n /= 3;
            else if (n % 5 == 0)
                n /= 5;
            else
                break;
        }
        return n == 1;

        // while (n % 10 == 0) n /= 10;
        // while (n % 6 == 0) n /= 6;
        // while (n % 5 == 0) n /= 5;
        // while (n % 3 == 0) n /= 3;
        // while (n % 2 == 0) n /= 2;
    }
};
// @lc code=end

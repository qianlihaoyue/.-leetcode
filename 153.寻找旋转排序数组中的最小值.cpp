/*
 * @lc app=leetcode.cn id=153 lang=cpp
 *
 * [153] 寻找旋转排序数组中的最小值
 */

#include <algorithm>
#include <iterator>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int findMin(vector<int>& nums) {
        // if (nums.front() < nums.back()) return nums[0];

        // int l = 0, r = nums.size() - 1;
        // while (l < r) {
        //     // 下降
        //     int mid = l + (r - l) / 2;
        //     if (nums[mid + 1] < nums[mid]) return nums[mid + 1];

        //     if (nums[mid] > nums.front())
        //         l = mid;
        //     else
        //         r = mid;
        // }
        // return nums[0];

        // 二分？最后一个数，要么是最小值，要么最小值在其左侧
        int l = 0, r = nums.size() - 1;
        while (l < r) {
            int mid = l + (r - l) / 2;
            if (nums[mid] < nums.back())
                r = mid;
            else
                l = mid + 1;
        }
        return nums[r];
    }
};
// @lc code=end

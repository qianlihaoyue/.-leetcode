/*
 * @lc app=leetcode.cn id=148 lang=cpp
 *
 * [148] 排序链表
 */

#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

bool linkComp(ListNode* a, ListNode* b) { return a->val > b->val; }

class Solution {
public:
    ListNode* sortList(ListNode* head) {
        if (!head) return nullptr;

        // 链表不支持 随机访问

        //////////// 数组法，较快，但占用O(n)空间
        std::vector<ListNode*> vec;
        ListNode* cur = head;
        while (cur) {
            vec.push_back(cur);
            cur = cur->next;
        }
        std::sort(vec.begin(), vec.end(), [](ListNode* a, ListNode* b) { return a->val < b->val; });

        for (int i = 0; i < vec.size() - 1; ++i) vec[i]->next = vec[i + 1];

        vec[vec.size() - 1]->next = nullptr;  // 数组法最后一个要置0

        return vec[0];

        //////////// 归并排序

        
    }
};
// @lc code=end

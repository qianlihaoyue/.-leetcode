/*
 * @lc app=leetcode.cn id=59 lang=cpp
 *
 * [59] 螺旋矩阵 II
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> generateMatrix(int n) {
        vector<vector<int>> result(n, vector<int>(n));
        // 使用状态机
        int r = 0, c = 0, dir = 0;
        for (int i = 1; i <= n * n; i++) {
            result[r][c] = i;

            // 判断状态是否改变
            if (dir == 0) {
                if (c + 1 >= n)  // 出界
                    dir = 1;
                else if (result[r][c + 1])  // 之前已存在
                    dir = 1;
            } else if (dir == 1) {
                if (r + 1 >= n)
                    dir = 2;
                else if (result[r + 1][c])
                    dir = 2;
            } else if (dir == 2) {
                if (c - 1 < 0)
                    dir = 3;
                else if (result[r][c - 1])
                    dir = 3;
            } else if (dir == 3) {
                if (r - 1 < 0)
                    dir = 0;
                else if (result[r - 1][c])
                    dir = 0;
            }

            // 状态更新
            if (dir == 0)
                c++;
            else if (dir == 1)
                r++;
            else if (dir == 2)
                c--;
            else if (dir == 3)
                r--;
        }

        return result;
    }
};
// @lc code=end

int main() {
    Solution solute;

    auto result = solute.generateMatrix(5);
    for (auto& row : result) {
        for (auto& n : row) cout << n << " ";
        cout << endl;
    }

    return 0;
}

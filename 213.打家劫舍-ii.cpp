/*
 * @lc app=leetcode.cn id=213 lang=cpp
 *
 * [213] 打家劫舍 II
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int rob(vector<int>& nums) {
        vector<int> dp(nums.size() + 1, 0);
        // dp[1]=

        for (int i = 2; i < nums.size(); i++) {
            dp[i] = max(dp[i - 2] + nums[i], dp[i - 1]);
            if (i == nums.size() - 1) {
                dp[i] = max(dp[i - 2] + nums[i], dp[i - 1] + nums[0]);
            }
        }

        return dp[nums.size() - 1];
    }
};
// @lc code=end

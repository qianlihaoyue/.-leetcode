/*
 * @lc app=leetcode.cn id=376 lang=cpp
 *
 * [376] 摆动序列
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <type_traits>
#include <utility>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <unordered_map>
#include <queue>
using namespace std;

// @lc code=start
class Solution {
public:
    int wiggleMaxLength(vector<int>& nums) {
        if (nums.size() == 1) return 1;

        for (auto it = nums.begin() + 1; it != nums.end() - 1;) {
            int tmp = (*it - *(it - 1)) * (*it - *(it + 1));
            if (tmp > 0)
                it++;
            else
                nums.erase(it);
        }

        if (nums.size() == 2) {
            if (nums[0] == nums[1])
                return 1;
            else
                return 2;
        }

        return nums.size();
    }
};
// @lc code=end

int main() {
    Solution solute;

    vector<int> g = {1, 17, 5, 10, 13, 15, 10, 5, 16, 8};
    auto result = solute.wiggleMaxLength(g);

    // for (auto& n : result) cout << n << " ";

    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    cout << endl << result << endl;

    return 0;
}

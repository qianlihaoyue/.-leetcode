/*
 * @lc app=leetcode.cn id=617 lang=cpp
 *
 * [617] 合并二叉树
 */

#include <algorithm>
#include <iterator>
#include <memory>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 合并到root1
    // void recurpre(TreeNode* root1, TreeNode* root2) {
    //     if (!root2) return;  // root2提前结束， 无事发生
    //     // 接下来root2一定存在
    //     if (!root1) return;
    //     root1->val += root2->val;
    //     // root1 = root2;  //!!!! nullptr能作为指针吗,不能

    //     bool flg = false;
    //     if (!root1->left) {
    //         // 如果2有，那就继承，没有就算了
    //         if (root2->left) root1->left = root2->left;
    //         flg = true;
    //         // return;
    //     }
    //     if (!root1->right) {
    //         if (root2->right) root1->right = root2->right;
    //         flg = true;
    //         // return;
    //     }
    //     if (flg) return;

    //     // 接下来root1 left right一定存在
    //     recurpre(root1->left, root2->left);
    //     recurpre(root1->right, root2->right);
    // }

    TreeNode* mergeTrees(TreeNode* root1, TreeNode* root2) {
        if (!root2) return root1;
        if (!root1) return root2;

        root1->val += root2->val;  // 在树1节点上合并节点
        root1->left = mergeTrees(root1->left, root2->left);
        root1->right = mergeTrees(root1->right, root2->right);
        return root1;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=73 lang=cpp
 *
 * [73] 矩阵置零
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

// @lc code=start
class Solution {
public:
    void setZeroes(vector<vector<int>> &matrix) {
        set<int> vecr, vecc;
        int m = matrix.size(), n = matrix[0].size();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    vecr.insert(i);
                    vecc.insert(j);
                }
            }
        }

        for (auto &r : vecr)
            for (int j = 0; j < n; j++) matrix[r][j] = 0;
        for (auto &c : vecc)
            for (int i = 0; i < m; i++) matrix[i][c] = 0;
    }
};
// @lc code=end

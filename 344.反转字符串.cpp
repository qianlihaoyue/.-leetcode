/*
 * @lc app=leetcode.cn id=344 lang=cpp
 *
 * [344] 反转字符串
 */

#include <algorithm>
using namespace std;

// @lc code=start
// easy
class Solution {
public:
    void reverseString(vector<char>& s) {
        for (int i = 0; i < s.size() / 2; i++) {
            // std::swap(s[i], s[s.size() - 1 - i]);
            char tmp = s[i];
            s[i] = s[s.size() - 1 - i];
            s[s.size() - 1 - i] = tmp;
        }
    }
};
// @lc code=end

int main() {
    Solution solute;
    // vector<char> s = {"h", "e", "l", "l", "o"};
    // solute.reverseString(s);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

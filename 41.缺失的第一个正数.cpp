/*
 * @lc app=leetcode.cn id=41 lang=cpp
 *
 * [41] 缺失的第一个正数
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int firstMissingPositive(vector<int>& nums) {
        // 统计法,不用排序
        // vector<bool> visit(nums.size(), false);
        // for (auto& n : nums)
        //     if (n > 0 && n <= nums.size()) visit[n - 1] = true;
        // for (int i = 0; i < nums.size(); i++)
        //     if (!visit[i]) return i + 1;
        // return nums.size() + 1;

        sort(nums.begin(), nums.end());
        int res = 1;
        for (auto& n : nums) {
            if (n == res)
                res++;
            else if (n > res)
                return res;
        }

        return res;
    }
};
// @lc code=end

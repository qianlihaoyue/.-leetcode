/*
 * @lc app=leetcode.cn id=383 lang=cpp
 *
 * [383] 赎金信
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
// easy
class Solution {
public:
    bool canConstruct(string ransomNote, string magazine) {
        int map[26] = {0};
        for (auto &c : magazine) map[c - 'a']++;
        for (auto &c : ransomNote)
            if (--map[c - 'a'] < 0) return false;
        return true;
    }
};
// @lc code=end

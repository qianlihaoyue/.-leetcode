/*
 * @lc app=leetcode.cn id=342 lang=cpp
 *
 * [342] 4的幂
 */

#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include <set>

using namespace std;

// @lc code=start
class Solution {
public:
    bool isPowerOfFour(int n) {
        // // 快速判断
        // if (n == 1) return true;
        // int mod10 = n % 10;
        // if (mod10 != 4 && mod10 != 6) return false;

        // while (n > 4) {
        //     if (n % 4) return false;
        //     n /= 4;
        // }
        // return n == 4;

        if (n < 0 || n & (n - 1))  // check(is or not) a power of 2.
            return false;

        // 检测奇数位法，二进制
        return n & 0x55555555;  // check 1 on odd bits
        // 通过求mod
        // return n % 3 == 1;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=24 lang=cpp
 *
 * [24] 两两交换链表中的节点
 */

#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* swapPairs(ListNode* head) {
        // if (!head) return nullptr;

        // 使用数组大法
        // vector<ListNode*> vec;
        // while (head) {
        //     vec.push_back(head);
        //     head = head->next;
        // }
        // for (int i = 1; i < vec.size(); i++)
        //     if (i % 2) std::swap(vec[i - 1], vec[i]);
        // for (int i = 1; i < vec.size(); i++) vec[i - 1]->next = vec[i];
        // vec[vec.size() - 1]->next = nullptr;
        // return vec[0];

        // 递归法
        // if (head->next == nullptr) return head;
        // ListNode* tmp = head->next;
        // ListNode* tmpnext = head->next->next;
        // tmp->next = head;
        // head->next = swapPairs(tmpnext);
        // return tmp;

        // 寻找递归终止条件
        // 1、head 指向的结点为 null
        // 2、head 指向的结点的下一个结点为 null
        // 在这两种情况下，一个节点或者空节点无论怎么交换操作，都是原来的 head
        if (head == nullptr || head->next == nullptr) return head;
        // 不断的通过递归调用
        ListNode* subHead = swapPairs(head->next->next);
        // 互换head 与 head.next
        ListNode* headNext = head->next;
        headNext->next = head;
        head->next = subHead;
        // 交换之后，原来 第二位节点 headNext 变成了第一位,返回
        return headNext;
    }
};
// @lc code=end

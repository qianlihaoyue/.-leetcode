/*
 * @lc app=leetcode.cn id=417 lang=cpp
 *
 * [417] 太平洋大西洋水流问题
 */

#include <algorithm>
#include <cstdint>
#include <iostream>

#include <array>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <string>
#include <utility>

using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> result;
    void dfs(vector<vector<int>>& heights, vector<vector<uint8_t>>& visit, uint8_t mark, int i, int j) {
        if (visit[i][j] == mark) return;
        // 如果计算另一个洋到访过
        if (visit[i][j]) result.push_back({i, j});
        visit[i][j] = mark;
        if (i - 1 >= 0)
            if (heights[i - 1][j] >= heights[i][j]) dfs(heights, visit, mark, i - 1, j);
        if (j - 1 >= 0)
            if (heights[i][j - 1] >= heights[i][j]) dfs(heights, visit, mark, i, j - 1);
        if (i + 1 < heights.size())
            if (heights[i + 1][j] >= heights[i][j]) dfs(heights, visit, mark, i + 1, j);
        if (j + 1 < heights[0].size())
            if (heights[i][j + 1] >= heights[i][j]) dfs(heights, visit, mark, i, j + 1);
    }

    vector<vector<int>> pacificAtlantic(vector<vector<int>>& heights) {
        // 首先，只有两个角的才可能同时流入，然后判断其他的是否能流到这两个角
        // 右上角
        // 不对不对
        // 从注定可以流向太平洋的，开始向上

        int m = heights.size(), n = heights[0].size();
        vector<vector<uint8_t>> visit(m, vector<uint8_t>(n, 0));
        int i = 0;
        for (int j = 0; j < n; j++) dfs(heights, visit, 0x01, i, j);
        int j = 0;
        for (int i = 0; i < m; i++) dfs(heights, visit, 0x01, i, j);

        i = m - 1;
        for (int j = 0; j < n; j++) dfs(heights, visit, 0x02, i, j);
        j = n - 1;
        for (int i = 0; i < m; i++) dfs(heights, visit, 0x02, i, j);

        return result;
    }
};
// @lc code=end

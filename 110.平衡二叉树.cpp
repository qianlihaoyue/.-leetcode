/*
 * @lc app=leetcode.cn id=110 lang=cpp
 *
 * [110] 平衡二叉树
 */

#include <algorithm>
#include <complex>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // int maxHeight(TreeNode *root) { return root ? std::max(maxHeight(root->left), maxHeight(root->right)) + 1 : 0; }

    // bool isBalanced(TreeNode *root) {
    //     if (!root) return true;
    //     if (abs(maxHeight(root->left) - maxHeight(root->right)) > 1)
    //         return false;
    //     else
    //         return isBalanced(root->left) && isBalanced(root->right);
    // }

    int maxHeight(TreeNode *root) {
        if (!root) return 0;
        int lh = maxHeight(root->left);
        if (lh == -1) return -1;
        int rh = maxHeight(root->right);
        if (rh == -1) return -1;
        if (std::abs(lh - rh) > 1) return -1;
        return std::max(lh, rh) + 1;
    }

    bool isBalanced(TreeNode *root) { return maxHeight(root) != -1; }
};
// @lc code=end

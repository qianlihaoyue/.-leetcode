/*
 * @lc app=leetcode.cn id=724 lang=cpp
 *
 * [724] 寻找数组的中心下标
 */

#include <queue>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int pivotIndex(vector<int>& nums) {
        int left_sum = 0, sum = 0;
        for (auto& n : nums) sum += n;  // 求总和
        for (int i = 0; i < nums.size(); i++) {
            if (left_sum * 2 + nums[i] == sum) return i;
            left_sum += nums[i];
        }
        return -1;

        // int left_sum = 0, right_sum = 0;
        // int index = nums.size() / 2;
        // int last_index, last_index2;

        // for (int i = 0; i < index; i++) left_sum += nums[i];
        // for (int i = index + 1; i < nums.size(); i++) right_sum += nums[i];

        // int cnt = 0;
        // while (1) {
        //     if (left_sum == right_sum)
        //         return index;
        //     else {
        //         // 折返了
        //         if (last_index2 == index) return -1;
        //         last_index2 = last_index;
        //         last_index = index;
        //         if (left_sum > right_sum) {
        //             index--;
        //             left_sum -= nums[index];
        //             right_sum += nums[index + 1];
        //         } else {
        //             index++;
        //             right_sum -= nums[index];
        //             left_sum += nums[index - 1];
        //         }
        //         // if (index == 0 || index == nums.size()) return -1;
        //     }
        //     if (index < 0 || index >= nums.size()) return -1;
        //     if (cnt++ > nums.size()) return -1;
        // }
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=721 lang=cpp
 *
 * [721] 账户合并
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    unordered_map<string, string> parent;
    unordered_map<string, string> names;

    string Find(string idx) {
        if (parent[idx] != idx) parent[idx] = Find(parent[idx]);  // 路径压缩
        return parent[idx];
    }

    void Union(string idx1, string idx2) {
        parent[Find(idx1)] = Find(idx2);  // 直接祖宗相连
    }

    vector<vector<string>> accountsMerge(vector<vector<string>> &accounts) {
        // 初始化
        for (auto &acc : accounts) {
            // for (auto &str : acc) parent[str] = str;
            // parent[acc[0]] = acc[1];
            for (int i = 1; i < acc.size(); i++) {
                parent[acc[i]] = acc[i];
                names[acc[i]] = acc[0];  // 名字
            }
        }

        // 合并
        for (auto &acc : accounts) {
            if (acc.size() > 2)
                for (int i = 1; i < acc.size() - 1; i++) Union(acc[i], acc[i + 1]);
        }

        // 输出
        map<string, vector<string>> strans;
        for (auto &vec : parent) strans[Find(vec.first)].push_back(vec.first);
        vector<vector<string>> ans;
        for (auto &vec : strans) {
            vec.second.insert(vec.second.begin(), names[vec.second[0]]);
            ans.push_back(vec.second);
        }
        return ans;
    }
};
// @lc code=end


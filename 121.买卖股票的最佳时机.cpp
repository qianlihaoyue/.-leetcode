/*
 * @lc app=leetcode.cn id=121 lang=cpp
 *
 * [121] 买卖股票的最佳时机
 */

#include <algorithm>
#include <iostream>

#include <iterator>
#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    // --也就是求所有的上升沿之和--,并不是，只能买卖一次
    // 所以需要改成贪心算法
    int maxProfit(vector<int>& prices) {
        if (prices.size() == 1) return 0;

        // 买的那天一定是卖的那天之前的最小值。 每到一天，维护那天之前的最小值即可
        int min_v = prices[0];
        int max_diff = 0;
        for (int i = 1; i < prices.size(); i++) {
            min_v = min(min_v, prices[i]);
            max_diff = max(max_diff, prices[i] - min_v);
        }
        return max_diff;
    }
};
// @lc code=end

int main(int argc, char* argv[]) {
    Solution solu;

    vector<int> prices = {7, 1, 5, 3, 6, 4};

    cout << solu.maxProfit(prices);

    return 0;
}

/*
 * @lc app=leetcode.cn id=134 lang=cpp
 *
 * [134] 加油站
 */

#include <algorithm>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int canCompleteCircuit(vector<int>& gas, vector<int>& cost) {
        int sum = 0, cur = 0;
        int start_idx = 0;
        for (int i = 0; i < gas.size(); i++) {
            sum += gas[i] - cost[i];
            cur += gas[i] - cost[i];
            if (cur < 0) {
                start_idx = i + 1;
                cur = 0;
            }
        }
        if (sum < 0)
            return -1;
        else
            return start_idx;
    }
};
// @lc code=end

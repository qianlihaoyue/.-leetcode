/*
 * @lc app=leetcode.cn id=343 lang=cpp
 *
 * [343] 整数拆分
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    int integerBreak(int n) {
        // // 数论+贪心
        // if (n == 2) return 1;
        // if (n == 3) return 2;
        // if (n == 4) return 4;
        // int sum = 1;
        // while (n > 4) {
        //     sum *= 3;
        //     n -= 3;
        // }
        // sum *= n;
        // return sum;

        // dp
        // 1.dp[i]：分拆数字i，可以得到的最大乘积为dp[i]
        // dp[i] = max dp[i-j]*j  j=1

        vector<int> dp(n + 1, 0);
        dp[2] = 1;
        for (int i = 3; i <= n; i++) {
            for (int j = 1; j <= i / 2; j++) {
                dp[i] = max(dp[i], max(dp[i - j] * j, (i - j) * j));
            }
        }
        return dp[n];
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=530 lang=cpp
 *
 * [530] 二叉搜索树的最小绝对差
 */

#include <algorithm>
#include <iterator>
#include <memory>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void traversal(TreeNode* root, std::vector<int>& vec) {
        if (!root) return;
        traversal(root->left, vec);
        vec.push_back(root->val);  // 中序遍历
        traversal(root->right, vec);
    }

    int getMinimumDifference(TreeNode* root) {
        std::vector<int> vec;
        traversal(root, vec);
        int min_value = 10e5;
        for (int i = 0; i < vec.size() - 1; i++) min_value = min(min_value, vec[i + 1] - vec[i]);
        return min_value;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=205 lang=cpp
 *
 * [205] 同构字符串
 */

#include <unordered_map>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    bool isIsomorphic(string s, string t) {
        std::unordered_map<char, char> map;
        // 创建重映射map
        std::unordered_map<char, char> re_map;
        for (int i = 0; i < s.size(); i++) {
            // 如果能找到
            if (map.find(s[i]) != map.end()) {
                if (map[s[i]] != t[i]) return false;
            }
            // 找不到就创立映射
            else {
                // 判断创立的有没有已经建立映射
                if (re_map.find(t[i]) != re_map.end()) return false;
                map[s[i]] = t[i];
                re_map[t[i]] = s[i];
            }
        }
        return true;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=93 lang=cpp
 *
 * [93] 复原 IP 地址
 */

#include <algorithm>
using namespace std;

// @lc code=start
class Solution {
public:
    // 应该是使用dp+递归来解

    vector<string> ans;

    void dfs(const string& ori, int i, string ip, int seg) {
        // 结束符
        if (seg == 4) {
            if (i == ori.size()) ans.push_back(ip);
            return;  // 成功及其他情况，都结束
        }

        // 第几段
        if (seg) ip.push_back('.');
        seg++;

        if (i < ori.size()) dfs(ori, i + 1, ip + ori.substr(i, 1), seg);

        if (ori[i] == '0') return;  // 2位及3位数，不能以0开头

        if (i + 1 < ori.size()) dfs(ori, i + 2, ip + ori.substr(i, 2), seg);

        if (i + 2 < ori.size()) {
            int num = 0;
            for (int j = 0; j < 3; j++) num = num * 10 + (ori[i + j] - '0');
            if (num > 255) return;  // 3位数，则不能大于255

            dfs(ori, i + 3, ip + ori.substr(i, 3), seg);
        }
    }

    vector<string> restoreIpAddresses(string s) {
        string tmp;
        dfs(s, 0, tmp, 0);
        return ans;
    }
};
// @lc code=end

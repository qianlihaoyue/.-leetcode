/*
 * @lc app=leetcode.cn id=226 lang=cpp
 *
 * [226] 翻转二叉树
 */

#include <algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode *invertTree(TreeNode *root) {
        if (!root) return root;

        // 前序递归
        std::swap(root->left, root->right);
        invertTree(root->left);
        invertTree(root->right);

        // 层序遍历
        // vector<TreeNode *> group;
        // group.push_back(root);
        // while (!group.empty()) {
        //     vector<TreeNode *> group_next;
        //     for (auto &node : group) {
        //         std::swap(node->left, node->right);
        //         if (node->left) group_next.push_back(node->left);
        //         if (node->right) group_next.push_back(node->right);
        //     }
        //     group = group_next;
        // }

        return root;
    }
};
// @lc code=end

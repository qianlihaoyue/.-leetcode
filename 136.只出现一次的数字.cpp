/*
 * @lc app=leetcode.cn id=136 lang=cpp
 *
 * [136] 只出现一次的数字
 */

#include <algorithm>
#include <iostream>

#include <iterator>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    int singleNumber(vector<int>& nums) {
        // 使用set来进行抵消，但力扣的性能比较不切实际
        // 使用STL后性能直接就比不过了，但可读性也相当重要
        // set<int> st;
        // for (auto& n : nums) {
        //     auto it = st.find(n);
        //     if (it == st.end())
        //         st.insert(n);
        //     else
        //         st.erase(it);
        // }
        // return *st.begin();

        // 使用异或 位操作
        int res = 0;
        for (auto i : nums) res ^= i;
        return res;
    }
};
// @lc code=end

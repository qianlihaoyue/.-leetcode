/*
 * @lc app=leetcode.cn id=16 lang=cpp
 *
 * [16] 最接近的三数之和
 */

#include <algorithm>
#include <cstdlib>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int threeSumClosest(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        int min_diff = 10e4;
        int n = nums.size();
        for (int i = 0; i < n - 2; i++) {
            // 加速
            int dmin = nums[i] + nums[i + 1] + nums[i + 2] - target;
            if (dmin > 0) {
                if (abs(dmin) < abs(min_diff)) min_diff = dmin;
                return target + min_diff;
            }

            int l = i + 1, r = n - 1;
            while (l < r) {
                int diff = nums[i] + nums[l] + nums[r] - target;
                if (diff == 0) return target;
                if (abs(diff) < abs(min_diff)) min_diff = diff;
                if (diff > 0)
                    r--;
                else
                    l++;
            }
        }
        return target + min_diff;
    }
};
// @lc code=end

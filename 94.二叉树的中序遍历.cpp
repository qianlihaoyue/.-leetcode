/*
 * @lc app=leetcode.cn id=94 lang=cpp
 *
 * [94] 二叉树的中序遍历
 */

#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void recur_traversal(TreeNode *root, vector<int> &result) {
        if (root == nullptr) return;
        recur_traversal(root->left, result);
        result.push_back(root->val);  // 中
        recur_traversal(root->right, result);
    }

    vector<int> inorderTraversal(TreeNode *root) {
        vector<int> result;
        recur_traversal(root, result);
        return result;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=1 lang=cpp
 *
 * [1] 两数之和
 */

// using namespace std;
// #include <vector>
// #include <map>

#include <algorithm>
#include <vector>
#include <iostream>
#include <map>
using namespace std;

// @lc code=start
class Solution {
    // hash[value]=index
public:
    vector<int> twoSum(vector<int>& nums, int target) {
        std::map<int, int> hash_map;
        for (int i = 0; i < nums.size(); i++) {
            int complement = target - nums[i];
            if (hash_map.find(complement) != hash_map.end()) {
                return {hash_map[complement], i};
            }
            hash_map[nums[i]] = i;
        }
        return {};
    }
};
// @lc code=end

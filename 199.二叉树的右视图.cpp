/*
 * @lc app=leetcode.cn id=199 lang=cpp
 *
 * [199] 二叉树的右视图
 */

#include <algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> ans;

    // 递归+记录 法，从右往左遍历，同时记录最大深度
    void dfs(TreeNode *root, int depth) {
        if (!root) return;
        if (depth >= ans.size()) ans.push_back(root->val);
        if (root->right) dfs(root->right, depth + 1);
        if (root->left) dfs(root->left, depth + 1);
    }
    vector<int> rightSideView(TreeNode *root) {
        dfs(root, 0);
        return ans;
    }

    // 层序遍历取最右
    // vector<int> rightSideView(TreeNode *root) {
    //     vector<int> result;
    //     if (!root) return result;

    //     vector<TreeNode *> node_group;
    //     node_group.push_back(root);
    //     while (node_group.size()) {
    //         result.push_back(node_group.back()->val);

    //         vector<TreeNode *> tmp_group;
    //         for (auto &node : node_group) {
    //             if (node->left) tmp_group.push_back(node->left);
    //             if (node->right) tmp_group.push_back(node->right);
    //         }
    //         node_group = tmp_group;
    //     }

    //     return result;
    // }
};
// @lc code=end

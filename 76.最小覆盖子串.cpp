/*
 * @lc app=leetcode.cn id=76 lang=cpp
 *
 * [76] 最小覆盖子串
 */

#include <map>
#include <set>
#include <string>
using namespace std;

// @lc code=start
class Solution {
public:
    std::map<char, int> comp, buff;
    bool able() {
        for (auto &iter : comp) {
            auto it = buff.find(iter.first);
            if (it == buff.end()) return false;
            if (it->second < iter.second) return false;
        }
        return true;
    }
    string minWindow(string s, string t) {
        if (s.size() < t.size()) return "";

        int start = 0;
        string min_str = "";
        int min_len = s.size();

        for (auto &c : t) comp[c]++;

        for (int i = 0; i < s.size(); i++) {
            if (comp.find(s[i]) != comp.end()) buff[s[i]]++;  // 覆盖
            while (able()) {
                int len = i - start + 1;
                if (len <= min_len) {
                    min_str = s.substr(start, i - start + 1);
                    min_len = len;
                }
                if (comp.find(s[start]) != comp.end()) buff[s[start]]--;
                start++;
            }
        }
        return min_str;
    }
};

// class Solution {
//     bool is_covered(int cnt_s[], int cnt_t[]) {
//         for (int i = 'A'; i <= 'Z'; i++) {
//             if (cnt_s[i] < cnt_t[i]) {
//                 return false;
//             }
//         }
//         for (int i = 'a'; i <= 'z'; i++) {
//             if (cnt_s[i] < cnt_t[i]) {
//                 return false;
//             }
//         }
//         return true;
//     }

// public:
//     string minWindow(string s, string t) {
//         int m = s.length();
//         int ans_left = -1, ans_right = m, left = 0;
//         int cnt_s[128]{}, cnt_t[128]{};
//         for (char c : t) {
//             cnt_t[c]++;
//         }
//         for (int right = 0; right < m; right++) { // 移动子串右端点
//             cnt_s[s[right]]++; // 右端点字母移入子串
//             while (is_covered(cnt_s, cnt_t)) { // 涵盖
//                 if (right - left < ans_right - ans_left) { // 找到更短的子串
//                     ans_left = left; // 记录此时的左右端点
//                     ans_right = right;
//                 }
//                 cnt_s[s[left++]]--; // 左端点字母移出子串
//             }
//         }
//         return ans_left < 0 ? "" : s.substr(ans_left, ans_right - ans_left + 1);
//     }
// };

 
// @lc code=end

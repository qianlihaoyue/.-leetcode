/*
 * @lc app=leetcode.cn id=515 lang=cpp
 *
 * [515] 在每个树行中找最大值
 */

#include <algorithm>
#include <any>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> largestValues(TreeNode *root) {
        vector<int> result;
        if (root == nullptr) return result;
        vector<TreeNode *> node_group;
        node_group.push_back(root);
        while (!node_group.empty()) {
            vector<int> num_group;
            vector<TreeNode *> tmp_group;
            for (auto &node : node_group) {
                num_group.push_back(node->val);
                if (node->left) tmp_group.push_back(node->left);
                if (node->right) tmp_group.push_back(node->right);
            }
            node_group = tmp_group;
            result.push_back(*std::max_element(num_group.begin(), num_group.end()));
        }

        return result;
    }
};
// @lc code=end

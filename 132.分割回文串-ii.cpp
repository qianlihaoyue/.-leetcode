/*
 * @lc app=leetcode.cn id=132 lang=cpp
 *
 * [132] 分割回文串 II
 */

#include <algorithm>
#include <cstdint>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int ans = INT32_MAX;
    vector<string> path;

    bool isLoop(const string& s) {
        int i = 0, j = s.length() - 1;
        while (i < j)
            if (s[i++] != s[j--]) return false;
        return true;
    }

    void dfs(const string& s, int i) {
        if (i == s.size()) {
            ans = std::min(ans, (int)path.size() - 1);
            return;
        }
        for (int j = i; j < s.size(); j++) {
            auto str = s.substr(j, j - i + 1);
            if (isLoop(str)) {
                path.push_back(str);
                dfs(s, j + 1);
                path.pop_back();
            }
        }
    }

    int minCut(string s) {
        dfs(s, 0);
        return ans;
    }
};
// @lc code=end

int main() {
    Solution solu;
    cout << solu.isLoop("aab") << endl;
}
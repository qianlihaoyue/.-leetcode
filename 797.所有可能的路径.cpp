/*
 * @lc app=leetcode.cn id=797 lang=cpp
 *
 * [797] 所有可能的路径
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> result;
    vector<int> path;  // 0节点到终点的路径
    void dfs(vector<vector<int>>& graph, int idx) {
        // 递归结束的条件
        if (idx == graph.size() - 1) {
            result.push_back(path);
            return;
        }
        // 继续递归
        for (auto& node : graph[idx]) {
            path.push_back(node);  // 遍历到的节点加入到路径中来
            dfs(graph, node);
            path.pop_back();  // 回溯，撤销本节点
        }
    }

    vector<vector<int>> allPathsSourceTarget(vector<vector<int>>& graph) {
        path.push_back(0);  // 无论什么路径已经是从0节点出发
        dfs(graph, 0);
        return result;
    }
};
// @lc code=end

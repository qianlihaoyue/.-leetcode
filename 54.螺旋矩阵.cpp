/*
 * @lc app=leetcode.cn id=54 lang=cpp
 *
 * [54] 螺旋矩阵
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <map>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> spiralOrder(vector<vector<int>>& matrix) {
        vector<int> res;
        int r = 0, c = 0, dir = 1;
        int m = matrix.size(), n = matrix[0].size(), ml = 0, nl = -1;

        int all = m * n;
        while (all--) {
            res.push_back(matrix[r][c]);

            // 方向变化
            if (dir == 1 && c + 1 >= n) {
                dir = 2;
                n--;
            }  // right
            if (dir == 2 && r + 1 >= m) {
                dir = 3;
                m--;
            }  // down
            if (dir == 3 && c - 1 <= nl) {
                dir = 4;
                nl++;
            }  // left
            if (dir == 4 && r - 1 <= ml) {
                dir = 1;
                ml++;
            }  // up

            if (dir == 1) c++;
            if (dir == 2) r++;
            if (dir == 3) c--;
            if (dir == 4) r--;

            // 实际上是有bug
        }
        return res;
    }
};
// @lc code=end

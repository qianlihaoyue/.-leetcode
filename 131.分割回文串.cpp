/*
 * @lc app=leetcode.cn id=131 lang=cpp
 *
 * [131] 分割回文串
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<string>> ans;
    vector<string> path;

    bool isLoop(const string& s) {
        int i = 0, j = s.length() - 1;
        while (i < j)
            if (s[i++] != s[j--]) return false;
        return true;
    }

    // 假设每两个单词中间有个‘，’，是否选择
    void dfs(const string& s, int i) {
        if (i == s.size()) {
            ans.push_back(path);
            return;
        }
        for (int j = i; j < s.size(); j++) {
            auto str = s.substr(i, j - i + 1);
            // 只有切割的子串是回文，才继续
            if (isLoop(str)) {
                path.push_back(str);
                dfs(s, j + 1);
                path.pop_back();
            }
        }
    }

    vector<vector<string>> partition(string s) {
        dfs(s, 0);
        return ans;
    }
};
// @lc code=end

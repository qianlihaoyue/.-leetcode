/*
 * @lc app=leetcode.cn id=21 lang=cpp
 *
 * [21] 合并两个有序链表
 */

#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2) {
        // ListNode* dummy = new ListNode();
        // ListNode* cur = dummy;

        // while (list1 && list2) {
        //     if (list1->val < list2->val) {
        //         cur->next = list1;
        //         list1 = list1->next;
        //     } else {
        //         cur->next = list2;
        //         list2 = list2->next;
        //     }
        //     cur = cur->next;
        // }
        // // 处理剩余的
        // if (list1) cur->next = list1;
        // if (list2) cur->next = list2;

        // return dummy->next;

        // 递归法
        if (list1 == nullptr) return list2;
        if (list2 == nullptr) return list1;
        // ListNode* res = list1->val < list2->val ? list1 : list2;
        // res->next = mergeTwoLists(res->next, list1->val >= list2->val ? list1 : list2);
        // return res;

        if (list1->val < list2->val) {
            list1->next = mergeTwoLists(list1->next, list2);
            return list1;
        } else {
            list2->next = mergeTwoLists(list1, list2->next);
            return list2;
        }
    }
};
// @lc code=end

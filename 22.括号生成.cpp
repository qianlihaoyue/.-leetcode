/*
 * @lc app=leetcode.cn id=22 lang=cpp
 *
 * [22] 括号生成
 */

#include <algorithm>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<string> ans;
    // DFS+少量的剪枝
    // 剪枝的条件为：左括号的数目一旦小于右括号的数目，
    // 以及，左括号的数目和右括号数目均小于n。

    void dfs(const int n, string str, int l, int r) {
        if (l > n || r > n || r > l) return;
        if (l == n && r == n) {
            ans.push_back(str);
            return;
        }
        dfs(n, str + '(', l + 1, r);
        dfs(n, str + ')', l, r + 1);
    }

    // void dfs(const int n, string path, int i, int open) {
    //     if (i == 2 * n) {
    //         ans.push_back(path);
    //         return;
    //     }
    //     if (open < n) dfs(n, path + '(', i + 1, open + 1);
    //     if (i - open < open) dfs(n, path + ')', i + 1, open);
    // }

    vector<string> generateParenthesis(int n) {
        dfs(n, "", 0, 0);
        return ans;
    }
};
// @lc code=end

int main() {
    Solution solu;
    auto res = solu.generateParenthesis(3);
    for (auto &n : res) cout << n << " ";
    // cout << "res: " << res << endl;
}

// (())(())","()(())()
// ()(())()","()(())()

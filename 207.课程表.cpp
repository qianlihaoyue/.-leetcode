/*
 * @lc app=leetcode.cn id=207 lang=cpp
 *
 * [207] 课程表
 */

#include <bits/stdc++.h>
#include <unordered_map>
using namespace std;

// @lc code=start
class Solution {
public:
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        if (prerequisites.size() == 0) return true;
        // 可以转换为看有没有环（锁）

        // 有向图

        // 通过入度出度判断
        std::vector<int> deg(numCourses, 0);
        std::vector<vector<int>> tab(numCourses);  // 邻接表
                                                   // 添加入度 及 别人对它的依赖
        for (auto& pair : prerequisites) {
            deg[pair[0]]++;
            tab[pair[1]].push_back(pair[0]);
        }

        // 必须按顺序推出
        std::queue<int> que;
        // 初始值，第一轮度为0的
        for (int i = 0; i < numCourses; i++)
            if (deg[i] == 0) que.push(i);
        // DFS
        while (que.size()) {
            int idx = que.front();
            que.pop();
            // 对其上一层进行出度
            for (auto& i : tab[idx]) {
                deg[i]--;
                if (deg[i] == 0) que.push(i);
            }
        }
        // 判断有没有还有度的
        for (auto& d : deg)
            if (d) return false;
        return true;
    }
};
// @lc code=end

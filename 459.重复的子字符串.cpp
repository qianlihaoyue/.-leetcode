/*
 * @lc app=leetcode.cn id=459 lang=cpp
 *
 * [459] 重复的子字符串
 */

#include <algorithm>
#include <vector>
#include <iostream>
#include <map>
#include <unordered_map>
#include <string>
using namespace std;

// @lc code=start
class Solution {
public:
    bool repeatedSubstringPattern(string s) {
        if (s.size() == 1) return false;

        // KMP


        // 拼接法
        string t = s + s;    // 拼接
        t.erase(t.begin());  // 掐头去尾, 无法直接找到子串
        t.erase(t.end() - 1);
        if (t.find(s) != std::string::npos) return true;
        return false;
    }
};
// @lc code=end

int main() {
    Solution solute;
    string s = "abab";
    cout << solute.repeatedSubstringPattern(s);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

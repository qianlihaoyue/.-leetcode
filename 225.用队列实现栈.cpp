/*
 * @lc app=leetcode.cn id=225 lang=cpp
 *
 * [225] 用队列实现栈
 */

#include <queue>
#include <stack>
#include <iostream>
using namespace std;

// @lc code=start
class MyStack {
public:
    MyStack() {}

    void push(int x) { qu.push(x); }

    int pop() {
        // 轮换
        int cnt = qu.size() - 1;
        while (cnt--) {
            qu.push(qu.front()), qu.pop();
        }
        int x = qu.front();
        qu.pop();
        return x;
    }

    int top() {
        return qu.back();

        // int cnt = qu.size() - 1;
        // while (cnt--) {
        //     qu.push(qu.front()), qu.pop();
        // }
        // int x = qu.front();
        // qu.push(qu.front()), qu.pop();
        // return x;
    }

    bool empty() { return qu.empty(); }

private:
    std::queue<int> qu;
};

/**
 * Your MyStack object will be instantiated and called as such:
 * MyStack* obj = new MyStack();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->top();
 * bool param_4 = obj->empty();
 */
// @lc code=end

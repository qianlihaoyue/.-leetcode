/*
 * @lc app=leetcode.cn id=9 lang=cpp
 *
 * [9] 回文数
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    bool isPalindrome(int x) {
        if (x < 0) return false;

        // 逐个手算版
        // vector<int> buff;
        // while (x >= 10) {
        //     buff.push_back(x % 10);
        //     x /= 10;
        // }
        // buff.push_back(x);
        // for (int i = 0; i < buff.size(); i++) {
        //     if (buff[i] != buff[buff.size() - 1 - i]) return false;
        // }
        // return true;

        // 转字符串版
        std::string buff = std::to_string(x);
        for (int i = 0; i < buff.size(); i++) {
            if (buff[i] != buff[buff.size() - 1 - i]) return false;
        }
        return true;

        // 还有计算数字反向值的
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=167 lang=cpp
 *
 * [167] 两数之和 II - 输入有序数组
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <map>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> twoSum(vector<int>& numbers, int target) {
        // 经典解法，无序
        // std::map<int, int> mymap;
        // for (int i = 0; i < numbers.size(); i++) {
        //     auto iter = mymap.find(numbers[i]);
        //     if (iter != mymap.end()) {
        //         return {iter->second + 1, i + 1};
        //     }
        //     mymap[target - numbers[i]] = i;
        // }
        // return {1, 2};

        // 双指针解法,直接从两遍往里走
        int l = 0, r = numbers.size() - 1;
        while (numbers[l] + numbers[r] != target) {
            if (numbers[l] + numbers[r] < target)
                ++l;
            else
                --r;
        }

        // while (l < r) {
        //     if (numbers[l] == target - numbers[r])
        //         break;
        //     else if (numbers[l] < target - numbers[r])
        //         ++l;
        //     else
        //         --r;
        // }
        return {l + 1, r + 1};
    }
};
// @lc code=end

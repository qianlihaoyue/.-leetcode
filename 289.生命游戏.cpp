/*
 * @lc app=leetcode.cn id=289 lang=cpp
 *
 * [289] 生命游戏
 */

#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    // 特定位 置1：num |= 1 << i;
    // 特定位 置0：num &= ~(1 << i);
    // 特定位翻转：num ^= 1 << i;

    // main
    void gameOfLife(vector<vector<int>>& board) {
        if (board.size() < 1) return;
        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board[0].size(); j++) {
                int cnt = countLives(board, i, j);
                // 活细胞
                if (board[i][j] & 0x00000001) {
                    if (cnt == 2 || cnt == 3) board[i][j] |= (1 << 1);
                }
                // 死细胞
                else {
                    if (cnt == 3) board[i][j] |= (1 << 1);
                }
            }
        }
        // Update
        for (int i = 0; i < board.size(); i++)
            for (int j = 0; j < board[0].size(); j++)
                // C++中移位操作是基于二进制的
                board[i][j] >>= 1;
    }

private:
    int countLives(vector<vector<int>>& b, int i, int j) {
        int count = 0;
        int dirs[8][2] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}, {1, 1}, {1, -1}, {-1, 1}, {-1, -1}};
        for (auto& dir : dirs) {
            int x = i + dir[0];
            int y = j + dir[1];
            if (x < 0 || x > b.size() - 1 || y < 0 || y > b[0].size() - 1) continue;
            count += (b[x][y] & 1);
        }
        return count;
    }
};

// @lc code=end

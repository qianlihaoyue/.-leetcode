/*
 * @lc app=leetcode.cn id=52 lang=cpp
 *
 * [52] N 皇后 II
 */

#include <bits/stdc++.h>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int ans = 0;

    bool isVaild(vector<vector<bool>>& cb, int lv, int j) {
        for (int r = lv - 1; r >= 0; r--)
            if (cb[r][j]) return false;
        for (int r = lv - 1, c = j - 1; r >= 0 && c >= 0; r--, c--)
            if (cb[r][c]) return false;
        for (int r = lv - 1, c = j + 1; r >= 0 && c < cb.size(); r--, c++)
            if (cb[r][c]) return false;
        return true;
    }

    void dfs(vector<vector<bool>>& cb, int lv) {
        if (lv == cb.size()) {
            ans++;
            return;
        }
        for (int j = 0; j < cb.size(); j++) {
            if (isVaild(cb, lv, j)) {
                cb[lv][j] = true;
                dfs(cb, lv + 1);
                cb[lv][j] = false;
            }
        }
    }

    int totalNQueens(int n) {
        vector<vector<bool>> cb(n, vector<bool>(n, false));
        dfs(cb, 0);
        return ans;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=654 lang=cpp
 *
 * [654] 最大二叉树
 */

#include <algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // TreeNode* constructMaxTree(vector<int>& nums, int begin, int end) {
    //     if (begin >= end) return nullptr;  // 边界条件，子数组为空

    //     auto max_it = max_element(nums.begin() + begin, nums.begin() + end);
    //     int max_index = std::distance(nums.begin(), max_it);

    //     TreeNode* node = new TreeNode(*max_it);                 // 创建新节点 ！！！！！！
    //     node->left = constructMaxTree(nums, begin, max_index);  // 注意开闭区间 [begin,end)
    //     node->right = constructMaxTree(nums, max_index + 1, end);
    //     return node;
    // }

    TreeNode* constructMaxTree(vector<int>& nums, int begin, int end) {
        if (begin >= end) return nullptr;  // 边界条件，子数组为空

        auto max_it = max_element(nums.begin() + begin, nums.begin() + end);
        int max_index = std::distance(nums.begin(), max_it);

        TreeNode* node = new TreeNode(*max_it);                 // 创建新节点 ！！！！！！
        node->left = constructMaxTree(nums, begin, max_index);  // 注意开闭区间 [begin,end)
        node->right = constructMaxTree(nums, max_index + 1, end);
        return node;
    }

    TreeNode* constructMaximumBinaryTree(vector<int>& nums) {
        //
        return constructMaxTree(nums, 0, nums.size());
    }
};
// @lc code=end

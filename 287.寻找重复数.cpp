/*
 * @lc app=leetcode.cn id=287 lang=cpp
 *
 * [287] 寻找重复数
 */

#include <algorithm>
#include <set>
#include <unordered_set>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int findDuplicate(vector<int>& nums) {
        // 排序法,修改了原数组，不行
        // sort(nums.begin(), nums.end());
        // for (int i = 1; i < nums.size(); i++) {
        //     if (nums[i] == nums[i - 1]) return nums[i];
        // }
        // return nums.size();

        // 总和

        // set查找，更慢了
        // unordered_set<int> myset;
        // for (auto& n : nums) {
        //     if (myset.find(n) != myset.end())
        //         return n;
        //     else
        //         myset.insert(n);
        // }
        // return nums.size();

        // 快慢指针
        // 因为必定有环，to 142
        int slow = 0, fast = 0;
        do {
            slow = nums[slow];
            fast = nums[nums[fast]];
        } while (slow != fast);
        slow = 0;  // 慢指针归0，再跑一圈
        while (slow != fast) {
            slow = nums[slow];
            fast = nums[fast];
        }
        return slow;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=101 lang=cpp
 *
 * [101] 对称二叉树
 */

#include <algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool compareTwoTree(TreeNode *root1, TreeNode *root2) {
        // 先比较有没有
        if (!root1 && !root2) return true;
        if (!root1 || !root2) return false;
        // 比较数值
        if (root1->val != root2->val)
            return false;
        else
            return compareTwoTree(root1->left, root2->right) && compareTwoTree(root1->right, root2->left);

        return true;
    }

    bool isSymmetric(TreeNode *root) {
        //
        return compareTwoTree(root->left, root->right);
    }
};
// @lc code=end

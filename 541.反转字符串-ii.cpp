/*
 * @lc app=leetcode.cn id=541 lang=cpp
 *
 * [541] 反转字符串 II
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    string reverseStr(string s, int k) {
        int end = 0;
        for (int i = 0; i < s.length() / k; i += 2) {
            std::reverse(s.begin() + i * k, s.begin() + i * k + k);
            end = (i + 2) * k;
        }
        if (end != s.length()) std::reverse(s.begin() + end, s.end());

        return s;
    }
};
// @lc code=end

int main() {
    Solution solute;
    string s = "abcdefg";
    std::cout << solute.reverseStr(s, 2);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

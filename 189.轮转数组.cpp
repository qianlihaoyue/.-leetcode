/*
 * @lc app=leetcode.cn id=189 lang=cpp
 *
 * [189] 轮转数组
 */

#include <algorithm>
#include <tuple>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        k %= nums.size();  // 消除多轮

        // 新建法
        std::vector<int> nums2(nums.begin(), nums.end());
        for (int i = 0; i < nums.size(); i++) {
            nums[i] = nums2[(i + nums.size() - k) % nums.size()];
        }


        // 翻转法
        // std::reverse(nums.begin(), nums.end());
        // std::reverse(nums.begin(), nums.begin() + k);
        // std::reverse(nums.begin() + k, nums.end());
    }
};
// @lc code=end

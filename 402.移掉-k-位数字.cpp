/*
 * @lc app=leetcode.cn id=402 lang=cpp
 *
 * [402] 移掉 K 位数字
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    string removeKdigits(string num, int k) {
        if (num.size() <= k) return "0";
        // 也就是从前到后，依次移除最大的数
        int j = 1;
        for (int i = 0; i < k; i++) {
            bool flg = false;
            for (; j < num.size(); j++) {
                if (num[j] < num[j - 1]) {
                    num.erase(num.begin() + j - 1);
                    flg = true;
                    j = max(1, j - 1);  // 回退一格
                    break;
                }
            }
            if (flg == false) num.erase(num.begin() + num.size() - 1);
        }

        for (int i = 0; i < num.size(); i++) {
            if (num[i] != '0') return num.substr(i, num.size() - i);
        }

        return "0";

    }
};
// @lc code=end

int main() {
    Solution solu;
    auto res = solu.removeKdigits("112", 1);
    cout << "res: " << res << endl;
}

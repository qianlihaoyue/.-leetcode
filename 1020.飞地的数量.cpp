/*
 * @lc app=leetcode.cn id=1020 lang=cpp
 *
 * [1020] 飞地的数量
 */

#include <algorithm>
#include <iostream>

#include <array>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <string>
#include <utility>

using namespace std;

// @lc code=start
class Solution {
public:
    void dfs(vector<vector<int>>& grid, int i, int j) {  // y,x
        if (!grid[i][j]) return;
        grid[i][j] = 0;
        if (i - 1 >= 0) dfs(grid, i - 1, j);
        if (j - 1 >= 0) dfs(grid, i, j - 1);
        if (i + 1 < grid.size()) dfs(grid, i + 1, j);
        if (j + 1 < grid[0].size()) dfs(grid, i, j + 1);
        return;
    }

    int numEnclaves(vector<vector<int>>& grid) {
        // 先消除四个边
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if ((i == 0 || i == grid.size() - 1 || j == 0 || j == grid[0].size() - 1)) {
                    if (grid[i][j]) dfs(grid, i, j);
                }
            }
        }
        // int i = 0;
        // for (int j = 0; j < grid[0].size(); j++)
        //     if (grid[i][j]) dfs(grid, i, j);
        // i = grid.size() - 1;
        // for (int j = 0; j < grid[0].size(); j++)
        //     if (grid[i][j]) dfs(grid, i, j);
        // int j = 0;
        // for (int i = 1; i < grid.size() - 1; i++)
        //     if (grid[i][j]) dfs(grid, i, j);
        // j = grid[0].size() - 1;
        // for (int i = 1; i < grid.size() - 1; i++)
        //     if (grid[i][j]) dfs(grid, i, j);

        // 再统计
        int cnt = 0;
        for (int i = 1; i < grid.size() - 1; i++) {
            for (int j = 1; j < grid[0].size() - 1; j++)
                if (grid[i][j]) cnt++;
        }
        return cnt;
    }
};
// @lc code=end

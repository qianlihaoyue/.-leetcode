/*
 * @lc app=leetcode.cn id=71 lang=cpp
 *
 * [71] 简化路径
 */

#include <sstream>
#include <stack>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

// @lc code=start
class Solution {
public:
    string simplifyPath(string path) {
        std::stack<std::string> path_sub;

        // 字符串分割
        string dir;
        std::istringstream iss(path);
        while (std::getline(iss, dir, '/')) {
            if (!dir.empty()) {
                if (dir == "..") {
                    if (path_sub.size()) path_sub.pop();
                } else if (dir == ".") {
                } else {
                    path_sub.push(dir);
                }
            }
        }

        string new_path;
        if (path_sub.empty()) return "/";
        while (path_sub.size()) {
            new_path = "/" + path_sub.top() + new_path;
            path_sub.pop();
        }

        return new_path;
    }
};
// @lc code=end

int main() {
    Solution solu;
    solu.simplifyPath("/../");
}
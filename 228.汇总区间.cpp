/*
 * @lc app=leetcode.cn id=228 lang=cpp
 *
 * [228] 汇总区间
 */

#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<string> summaryRanges(vector<int>& nums) {
        // to_string 输入为int数值！！！！！
        vector<string> ans;
        int i = 0, n = nums.size();
        while (i < n) {
            int start = i;
            // 移动
            while (i + 1 < n && nums[i] + 1 == nums[i + 1]) i++;
            //  只有一位数字
            if (start == i)
                ans.push_back(to_string(nums[i]));
            else
                ans.push_back(to_string(nums[start]) + "->" + to_string(nums[i]));
            i++;  // 移动初始点
        }
        return ans;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=88 lang=cpp
 *
 * [88] 合并两个有序数组
 */

#include <algorithm>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
        // 删除还是合并等操作，倒序不会改变索引
        int idx1 = m - 1, idx2 = n - 1;
        for (int i = m + n - 1; i >= 0; i--) {
            if (idx1 >= 0 && idx2 >= 0) {
                if (nums1[idx1] > nums2[idx2])
                    nums1[i] = nums1[idx1--];
                else
                    nums1[i] = nums2[idx2--];
            } else if (idx2 >= 0)
                nums1[i] = nums2[idx2--];
            else  // 只有1，加速
                break;
        }

        // // 经典倒序合并
        // int idx1 = m - 1, idx2 = n - 1;
        // for (int i = m + n - 1; i >= 0; i--) {
        //     if (idx1 < 0)
        //         for (; i >= 0; i--) nums1[i] = nums2[idx2--];
        //     if (idx2 < 0) return;
        //     if (nums2[idx2] > nums1[idx1])
        //         nums1[i] = nums2[idx2--];
        //     else
        //         nums1[i] = nums1[idx1--];
        // }
    }
};
// @lc code=end

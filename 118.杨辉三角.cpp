/*
 * @lc app=leetcode.cn id=118 lang=cpp
 *
 * [118] 杨辉三角
 */

#include <algorithm>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> result;
        result.push_back({1});
        // if (numRows == 1) return result;
        // result.push_back({1, 1});
        // if (numRows == 2) return result;
        // result.push_back({1, 2, 1});
        // if (numRows == 3) return result;
        // for (int r = 3; r < numRows; r++) {
        //     vector<int> group(r + 1);
        //     group[0] = 1;
        //     int mid = (r + 1) / 2;
        //     for (int i = 1; i < r + 1; i++) {
        //         // 返回对称值
        //         if (i > mid) group[i] = group[r - i];
        //         // 从上一级计算
        //         else
        //             group[i] = result[r - 1][i - 1] + result[r - 1][i];
        //     }
        //     result.push_back(group);
        // }

        // dp
        // for (int i = 1; i < numRows; i++) {
        //     vector<int> group;
        //     group.push_back(1);
        //     auto &last = result[i - 1];
        //     for (int j = 0; j < last.size() - 1; j++) {
        //         group.push_back(last[j] + last[j + 1]);
        //     }
        //     group.push_back(1);
        //     result.push_back(group);
        // }

        for (int i = 1; i < numRows; i++) {
            vector<int> group(i + 1, 1);
            auto &last = result[i - 1];
            for (int j = 0; j < last.size() - 1; j++) {
                group[j + 1] = (last[j] + last[j + 1]);
            }
            result.push_back(group);
        }

        return result;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=414 lang=cpp
 *
 * [414] 第三大的数
 */

#include <algorithm>
#include <any>
#include <set>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int thirdMax(vector<int>& nums) {
        // // 改成从大到小排列
        // std::set<int, greater<int>> myset(nums.begin(), nums.end());
        // for (int i = 0; i < nums.size(); i++) myset.insert(nums[i]);
        // if (myset.size() < 3)
        //     return *myset.begin();
        // else {
        //     auto it = myset.begin();
        //     it++;
        //     it++;
        //     return *it;
        // }
        const int kMaxN = 3;
        set<int> max_num;
        max_num.insert(nums[0]);

        for (int i = 1; i < nums.size(); i++) {
            if (max_num.size() == kMaxN) {
                // 如果数字大于第一位（最小）
                auto it = max_num.begin();
                // 删除最小值并添加新值，新值会自动按顺序排列
                if (nums[i] > *it && max_num.count(nums[i]) == 0) {
                    max_num.erase(it);
                    max_num.insert(nums[i]);
                }
            } else
                max_num.insert(nums[i]);
        }
        if (max_num.size() == kMaxN)
            return *max_num.begin();
        else
            return *max_num.rbegin();
    }
};
// @lc code=end

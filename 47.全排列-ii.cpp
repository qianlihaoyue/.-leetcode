/*
 * @lc app=leetcode.cn id=47 lang=cpp
 *
 * [47] 全排列 II
 */

#include <algorithm>
#include <bits/stdc++.h>
#include <set>
#include <unordered_set>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> ans;
    void perm_dfs(vector<int>& nums, int i) {
        if (i == nums.size() - 1) {
            ans.push_back(nums);
            return;
        }
        unordered_set<int> st;
        for (int j = i; j < nums.size(); ++j) {
            if (st.find(nums[j]) != st.end()) continue;
            st.insert(nums[j]);

            swap(nums[i], nums[j]);
            perm_dfs(nums, i + 1);
            swap(nums[i], nums[j]);  // 恢复
        }
    }

    vector<vector<int>> permuteUnique(vector<int>& nums) {
        // sort(nums.begin(), nums.end());
        perm_dfs(nums, 0);
        return ans;
    }
};
// @lc code=end

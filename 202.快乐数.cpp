/*
 * @lc app=leetcode.cn id=202 lang=cpp
 *
 * [202] 快乐数
 */

#include <algorithm>
#include <unordered_set>
#include <vector>
#include <iostream>
using namespace std;

// @lc code=start
class Solution {
public:
    bool isHappy(int n) {
        // 判断循环可以用快慢指针，找环方法
        int fast = n, slow = n;
        while (1) {
            fast = getSum(getSum(fast));
            slow = getSum(slow);
            if (fast == 1) return true;
            if (fast == slow) return false;
        }

        // 还可用哈希表记录，标记法
        // unordered_set<int> map;
        // while (1) {
        //     n = getSum(n);
        //     if (n == 1) return true;
        //     if (map.find(n) != map.end()) return false;
        //     map.insert(n);
        // }

        // 特例法，但不好背
        // while (1) {
        //     if (n == 1 || n == 7 || n == 13 || n == 19) return true;
        //     if (n == 2 || n == 3 || n == 4 || n == 5 || n == 6 || n == 8 || n == 9) return false;
        //     n = getSum(n);
        // }
    }

    int getSum(int n) {
        int sum = 0;
        while (n) {
            sum += (n % 10) * (n % 10);
            n /= 10;
        }
        return sum;
    }
};
// @lc code=end

int main() {
    Solution solute;

    auto result = solute.isHappy(22);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    cout << result << endl;

    return 0;
}

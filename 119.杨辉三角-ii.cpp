/*
 * @lc app=leetcode.cn id=119 lang=cpp
 *
 * [119] 杨辉三角 II
 */

#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> getRow(int rowIndex) {
        if (rowIndex == 0)
            return {1};
        else if (rowIndex == 1)
            return {1, 1};
        vector<vector<int>> result;
        result.push_back({1});
        result.push_back({1, 1});

        for (int r = 2; r < rowIndex + 1; r++) {
            vector<int> group(r + 1);
            group[0] = 1;
            int mid = (r + 1) / 2;
            for (int i = 1; i < r + 1; i++) {
                // 返回对称值
                if (i > mid) group[i] = group[r - i];
                // 从上一级计算
                else
                    group[i] = result[r - 1][i - 1] + result[r - 1][i];
            }
            result.push_back(group);
        }
        return result[rowIndex];
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=2545 lang=cpp
 *
 * [2545] 根据第 K 场考试的分数排序
 */
#include <algorithm>
#include <functional>
#include <iostream>
#include <utility>
#include <vector>
using namespace std;

// @lc code=start

class Solution {
public:
    vector<vector<int>> sortTheStudents(vector<vector<int>> &score, int k) {
        // int n = score.size();
        // std::vector<std::pair<int, int>> pir;
        // for (int i = 0; i < n; i++) pir.push_back({score[i][k], i});
        // sort(pir.begin(), pir.end(), greater<std::pair<int, int>>());

        // vector<vector<int>> res;
        // for (auto& p : pir) res.push_back(score[p.second]);
        // return res;

        // sort(score.begin(), score.end(), [=](auto &a, auto &b) -> bool { return a[k] > b[k]; });

        sort(score.begin(), score.end(), std::function<bool >() { return a[k] > b[k]; });

        return score;
    }
};
// @lc code=end

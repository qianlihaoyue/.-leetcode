/*
 * @lc app=leetcode.cn id=389 lang=cpp
 *
 * [389] 找不同
 */

#include <map>
#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    char findTheDifference(string s, string t) {
        // 全统计
        // vector<int> mapcnt(26, 0);
        // for (auto &c : s) mapcnt[c - 'a']++;
        // for (auto &c : t) mapcnt[c - 'a']--;
        // for (int i = 0; i < 26; i++) {
        //     if (mapcnt[i]) return 'a' + i;
        // }
        // return 0;

        // 单统计
        int sum = 0;
        for (auto &c : s) sum -= c;
        for (auto &c : t) sum += c;
        return sum;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=862 lang=cpp
 *
 * [862] 和至少为 K 的最短子数组
 */

#include <algorithm>
#include <bits/stdc++.h>
#include <cstdint>
#include <deque>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int shortestSubarray(vector<int>& nums, int k) {
        // 维护一个单增的单调队列存前缀和，
        // 如果队尾减队首大于等于k，那么队首就找到了以他开始的最短的子数组，队首就可以退位了

        // 前缀和
        long s[nums.size() + 1];
        s[0] = 0;
        for (int i = 0; i < nums.size(); ++i) s[i + 1] = s[i] + nums[i];  // 计算前缀和

        int ans = INT32_MAX;
        deque<int> q;
        for (int i = 0; i <= nums.size(); ++i) {
            while (q.size() && s[i] - s[q.front()] >= k) {
                ans = min(ans, i - q.front());
                q.pop_front();  // 关键在于
            }
            while (q.size() && s[q.back()] >= s[i]) q.pop_back();  // 优化二
            q.push_back(i);
        }
        return ans == INT32_MAX ? -1 : ans;

        // 标准滑窗，只能通过一半
        // 添加负数之后窗口的滑动就没有单向性了，因此无法使用滑动窗口解决
        // int min_len = INT32_MAX;
        // int sum = 0, start = 0;
        // for (int i = 0; i < nums.size(); i++) {
        //     sum += nums[i];
        //     while (sum >= k) {
        //         min_len = min(min_len, i - start + 1);
        //         sum -= nums[start++];
        //     }
        // }
        // return min_len == INT32_MAX ? -1 : min_len;
    }
};
// @lc code=end

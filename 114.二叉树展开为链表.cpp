/*
 * @lc app=leetcode.cn id=114 lang=cpp
 *
 * [114] 二叉树展开为链表
 */

#include <algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 数组法
    // vector<TreeNode *> data;
    // void travel(TreeNode *root) {
    //     if (!root) return;
    //     data.push_back(root);
    //     travel(root->left);
    //     travel(root->right);
    // }

    // void flatten(TreeNode *root) {
    //     if (!root) return;
    //     travel(root);
    //     for (int i = 0; i < data.size() - 1; i++) {
    //         data[i]->left = nullptr;
    //         data[i]->right = data[i + 1];
    //     }
    //     data[data.size() - 1]->left = nullptr;
    //     data[data.size() - 1]->right = nullptr;
    // }

    // 原地展开法
    TreeNode *last = nullptr;
    void flatten(TreeNode *root) {
        if (!root) return;
        flatten(root->right);
        flatten(root->left);
        root->right = last;
        root->left = nullptr;
        last = root;
    }
};
// @lc code=end

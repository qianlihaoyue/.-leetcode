/*
 * @lc app=leetcode.cn id=827 lang=cpp
 *
 * [827] 最大人工岛
 */

#include <algorithm>
#include <cstdint>
#include <iostream>

#include <array>
#include <unordered_map>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <string>
#include <utility>

using namespace std;

// @lc code=start
class Solution {
public:
    void dfs_cnt(vector<vector<int>>& grid, int& cnt, int mark, int i, int j) {
        if (grid[i][j] != 1) return;
        grid[i][j] = mark;
        cnt++;
        if (i - 1 >= 0) dfs_cnt(grid, cnt, mark, i - 1, j);
        if (j - 1 >= 0) dfs_cnt(grid, cnt, mark, i, j - 1);
        if (i + 1 < grid.size()) dfs_cnt(grid, cnt, mark, i + 1, j);
        if (j + 1 < grid[0].size()) dfs_cnt(grid, cnt, mark, i, j + 1);
    }

    int largestIsland(vector<vector<int>>& grid) {
        std::map<int, int> mp_cnt;
        int m = grid.size(), n = grid[0].size();
        // vector<vector<int>> visit(m, vector<int>(n, 0));

        // 给每个岛编个号,并计数
        int mark = -1;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    int cnt = 0;
                    dfs_cnt(grid, cnt, mark, i, j);
                    mp_cnt[mark--] = cnt;
                }
            }
        }

        // cout << mp_cnt.size() < < < < endl;

        // 特殊情况
        const int kMaxCnt = m * n;
        if (kMaxCnt == 1) return 1;  // 地方较小
        if (mp_cnt.size() == 0) return 1;
        if (mp_cnt.size() == 1) {
            int size = mp_cnt[-1];
            if (size == kMaxCnt)
                return kMaxCnt;
            else
                return size + 1;
        }

        // 再次遍历
        int max_cnt = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0) {
                    set<int> to_add;
                    if (i - 1 >= 0)
                        if (grid[i - 1][j]) to_add.insert(grid[i - 1][j]);
                    if (j - 1 >= 0)
                        if (grid[i][j - 1]) to_add.insert(grid[i][j - 1]);
                    if (i + 1 < grid.size())
                        if (grid[i + 1][j]) to_add.insert(grid[i + 1][j]);
                    if (j + 1 < grid[0].size())
                        if (grid[i][j + 1]) to_add.insert(grid[i][j + 1]);

                    int cnt = 0;
                    for (auto& s : to_add) cnt += mp_cnt[s];

                    max_cnt = max(max_cnt, cnt);
                }
            }
        }

        return max_cnt + 1;
    }

    // vector<std::pair<int, int>> links;

    // void dfs_cnt(vector<vector<int>>& grid, vector<vector<int>>& visit, int& cnt, int mark, int i, int j) {
    //     if (grid[i][j] == 0) {
    //         // 这个mark不对
    //         if (visit[i][j] && visit[i][j] != mark) {
    //             // 如果遇到其他岛屿的扩展
    //             // links.insert(visit[i][j], mark);
    //             links.push_back(std::make_pair(visit[i][j], mark));
    //         }
    //         visit[i][j] = mark;
    //         return;
    //     }
    //     if (visit[i][j] == mark) return;
    //     visit[i][j] = mark;
    //     cnt++;
    //     if (i - 1 >= 0) dfs_cnt(grid, visit, cnt, mark, i - 1, j);
    //     if (j - 1 >= 0) dfs_cnt(grid, visit, cnt, mark, i, j - 1);
    //     if (i + 1 < grid.size()) dfs_cnt(grid, visit, cnt, mark, i + 1, j);
    //     if (j + 1 < grid[0].size()) dfs_cnt(grid, visit, cnt, mark, i, j + 1);
    // }

    // int largestIsland(vector<vector<int>>& grid) {
    //     // 建立碰撞箱？膨胀一下，膨胀完之后再算最大的岛屿
    //     // 使用map建立一个计数，收集单个岛的大小
    //     // 使用vector建立哪些岛屿存在连接关系
    //     std::map<int, int> mp_cnt;
    //     int m = grid.size(), n = grid[0].size();
    //     vector<vector<int>> visit(m, vector<int>(n, 0));

    //     // 给每个岛编个号,并计数
    //     int mark = 0;
    //     for (int i = 0; i < m; i++) {
    //         for (int j = 0; j < n; j++) {
    //             if (grid[i][j] && visit[i][j] == 0) {
    //                 int cnt = 0;
    //                 mark++;
    //                 dfs_cnt(grid, visit, cnt, mark, i, j);
    //                 mp_cnt[mark] = cnt;
    //             }
    //         }
    //     }

    //     // 特殊情况
    //     const int kMaxCnt = m * n;
    //     if (kMaxCnt == 1) return 1;  // 地方较小
    //     if (mp_cnt.size() == 0) return 1;
    //     if (mp_cnt.size() == 1) {
    //         int size = mp_cnt[1];
    //         if (size == kMaxCnt)
    //             return kMaxCnt;
    //         else
    //             return size + 1;
    //     }

    //     // 然后遍历links,计算最大和,
    //     // 没有考虑到，添加一个点，链接多个岛
    //     int max_cnt = 0;
    //     for (auto& pair : links) max_cnt = max(max_cnt, mp_cnt[pair.first] + mp_cnt[pair.second]);
    //     return max_cnt + 1;
    // }
};
// @lc code=end

int main(int argc, char* argv[]) {
    vector<vector<int>> grid = {{1, 1}, {1, 0}};

    Solution solu;
    cout << solu.largestIsland(grid);

    return 0;
}
/*
 * @lc app=leetcode.cn id=133 lang=cpp
 *
 * [133] 克隆图
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};

// @lc code=start
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> neighbors;
    Node() {
        val = 0;
        neighbors = vector<Node*>();
    }
    Node(int _val) {
        val = _val;
        neighbors = vector<Node*>();
    }
    Node(int _val, vector<Node*> _neighbors) {
        val = _val;
        neighbors = _neighbors;
    }
};
*/

class Solution {
public:
    std::map<int, Node*> visit;

    Node* cloneGraph(Node* node) {
        // 递归结束条件
        if (node == nullptr) return nullptr;
        if (visit.find(node->val) != visit.end()) return visit[node->val];

        // dfs
        Node* newG = new Node(node->val);
        visit[node->val] = newG;
        for (auto& n : node->neighbors) {
            auto res = cloneGraph(n);
            newG->neighbors.push_back(res);
        }
        return newG;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=198 lang=cpp
 *
 * [198] 打家劫舍
 */

#include <algorithm>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int rob(vector<int>& nums) {
        // if (nums.size() == 1) return nums[0];
        // vector<int> dp(nums.size(), 0);
        // dp[0] = nums[0];
        // dp[1] = max(nums[1], dp[0]);
        // for (int i = 2; i < nums.size(); i++) {
        //     // dp表的含义是，迄今为止，得到的最大值
        //     dp[i] = max(nums[i] + dp[i - 2], dp[i - 1]);
        // }
        // return dp[nums.size() - 1];

        // 时间复杂度：状态个数 * 计算单个状态的时间 O(n)

        // 优化空间O(1),类似斐波那契，只与前两个状态有关
        if (nums.size() == 1) return nums[0];
        vector<int> dp(2, 0);
        dp[0] = nums[0];
        dp[1] = max(nums[1], dp[0]);
        for (int i = 2; i < nums.size(); i++) {
            // dp表的含义是，迄今为止，得到的最大值
            int dpv = max(nums[i] + dp[0], dp[1]);
            dp[0] = dp[1], dp[1] = dpv;
        }
        return dp[1];
    }
};
// @lc code=end

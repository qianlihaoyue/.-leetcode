/*
 * @lc app=leetcode.cn id=704 lang=cpp
 *
 * [704] 二分查找
 */

#include <algorithm>
#include <iostream>

using namespace std;

// @lc code=start
class Solution {
public:
    int search(vector<int>& nums, int target) {
        // auto iter = std::lower_bound(nums.begin(), nums.end(), target);
        // if (iter != nums.end()) {
        //     if (*iter == target) return std::distance(nums.begin(), iter);
        // }
        // return -1;

        int l = 0, r = nums.size() - 1;
        while (l <= r) {
            int mid = (l + r) >> 1;
            if (nums[mid] > target)
                r = mid - 1;
            else if (nums[mid] < target)
                l = mid + 1;
            else
                return mid;
        }
        return -1;
    }
};
// @lc code=end

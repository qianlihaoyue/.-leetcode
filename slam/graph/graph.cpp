#include <bits/stdc++.h>
using namespace std;

// 带边
struct Edge {
    int idx;
    int dis;
};

// pair<int, float>

// 邻接表：
// vector<set<int>> adj(n);
// unordered_map<int, vector<Edge>> adj;
unordered_map<int, set<Edge>> adj;

// 邻接矩阵：
// 单向：即一半置一
// 双向：两边不同
// 无向：两边都置1
// 带边，直接设置value

// visit
// unordered_set<int> visit;
// vector<bool> visit(n);

// 回溯型写法
// void bfs(参数) {
//     if (终止条件) {
//         存放结果; return;
//     }
//     for (选择：本层集合中元素（树中节点孩子的数量就是集合的大小）) {
//         处理节点;
//         bfs(路径，选择列表); // 递归
//         回溯，撤销处理结果
//     }
// }

// void dfs(参数) {
//     if (终止条件) {
//         存放结果; return;
//     }

//     for (选择：本节点所连接的其他节点) {
//         处理节点;
//         dfs(图，选择的节点); // 递归
//         回溯，撤销处理结果
//     }
// }

// 遍历型写法
int dfs(int src, int dest) {
    stack<pair<int, int>> s;
    unordered_set<int> visited;
    s.push({src, 0});  // 起始节点的权重为0

    while (!s.empty()) {
        int node = s.top().first;
        int dist_sum = s.top().second;
        s.pop();

        if (node == dest) return dist_sum;

        if (visited.find(node) != visited.end()) continue;

        visited.insert(node);

        for (Edge edge : adj[node]) {
            s.push({edge.idx, dist_sum + edge.dis});
        }
    }

    return -1;
}

int bfs(int src, int dest) {
    queue<pair<int, int>> q;
    unordered_set<int> visited;
    q.push({src, 0});  // 起始节点的权重为0

    while (!q.empty()) {
        int node = q.front().first;
        int dist_sum = q.front().second;
        q.pop();

        if (node == dest) return dist_sum;

        if (visited.find(node) != visited.end()) continue;
        visited.insert(node);

        for (Edge edge : adj[node]) {
            q.push({edge.idx, dist_sum + edge.dis});
        }
    }

    return -1;
}

int dijkstra(int src, int dest) {
    priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> pq;
    unordered_map<int, int> dist;  // 记录到达每个节点的最短距离

    // 初始化距离为无穷大
    for (auto& pair : adj) dist[pair.first] = INT_MAX;
    dist[src] = 0;  // 起始节点的距离为 0
    pq.push({0, src});
    while (!pq.empty()) {
        int dist_sum = pq.top().first;  // first，方便按照距离排序
        int node = pq.top().second;
        pq.pop();

        // 到达目标，返回最短路径权重
        if (node == dest) return dist_sum;
        // 如果当前节点的距离已经更新过，就不再处理
        if (dist_sum > dist[node]) continue;

        // 遍历当前节点的邻居节点
        for (auto& edge : adj[node]) {
            int new_dis = dist_sum + edge.dis;
            // 如果新路径的权重比之前的路径权重小，就更新距离
            // 经过累加，所以不会折返
            if (new_dis < dist[edge.idx]) {
                dist[edge.idx] = new_dis;
                pq.push({new_dis, edge.idx});
            }
        }
    }

    return -1;  // 如果无法到达目标节点，则返回 -1
}

class Graph {
public:
    // 构造函数初始化图的顶点数
    Graph(int V) : V(V), adj(V) {}

    // 添加边
    void addEdge(int v, int w) {
        adj[v].insert(w);  // 将w添加到v的列表中
        adj[w].insert(v);  // 由于是无向图，也要将v添加到w的列表中

        // adj[v].insert(w); // 由于是有向图，只添加一次，表示v指向w
    }

    // 打印图
    void printGraph() {
        for (int v = 0; v < V; ++v) {
            std::cout << "Adj of v[" << v << "]:";
            for (auto x : adj[v]) std::cout << " - " << x;
            std::cout << std::endl;
        }
    }

private:
    int V;                           // 图的顶点数
    std::vector<std::set<int>> adj;  // 邻接表,避免重复添加
};

int main() {
    // 创建一个包含5个顶点的图
    Graph g(5);

    // 添加边
    g.addEdge(0, 1);
    g.addEdge(0, 1);
    g.addEdge(0, 1);
    g.addEdge(0, 4);
    g.addEdge(1, 2);
    g.addEdge(1, 3);
    g.addEdge(1, 4);
    g.addEdge(2, 3);
    g.addEdge(3, 4);

    // 打印图的邻接表表示
    g.printGraph();

    return 0;
}

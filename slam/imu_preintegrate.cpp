#include <iostream>
#include </usr/include/eigen3/Eigen/Dense>
#include <vector>

// 假设IMU数据由以下结构体表示
struct IMUData {
    Eigen::Vector3d linear_acceleration;  // 线加速度
    Eigen::Vector3d angular_velocity;     // 角速度
    double timestamp;                     // 时间戳
};

// IMU预积分类
class IMUPreintegrator {
public:
    // 构造函数
    IMUPreintegrator() { Reset(); }

    // 重置预积分器状态
    void Reset() {
        delta_p_.setZero();
        delta_v_.setZero();
        delta_R_.setIdentity();
        bias_a_.setZero();
        bias_g_.setZero();
    }

    // 设置IMU偏置
    void SetBias(const Eigen::Vector3d& bias_a, const Eigen::Vector3d& bias_g) {
        bias_a_ = bias_a;
        bias_g_ = bias_g;
    }

    // 对IMU数据进行预积分
    void Integrate(const IMUData& imu_data, double dt) {
        // 校正加速度和角速度，去除偏置
        Eigen::Vector3d corrected_a = imu_data.linear_acceleration - bias_a_;
        Eigen::Vector3d corrected_w = imu_data.angular_velocity - bias_g_;

        // 更新旋转
        // 计算小的角速度增量，近似为连续时间下的旋转矩阵
        // dR = exp(ω̂ * dt) ≈ I + ω̂ * dt （一阶近似）
        // 其中 ω̂ 是角速度的反对称矩阵（skew-symmetric matrix），这里使用罗德里格斯公式（Rodrigues' rotation formula）进行近似
        // clang-format off
        Eigen::Matrix3d dR = (
              Eigen::AngleAxisd(corrected_w(0) * dt, Eigen::Vector3d::UnitX())
            * Eigen::AngleAxisd(corrected_w(1) * dt, Eigen::Vector3d::UnitY())
            * Eigen::AngleAxisd(corrected_w(2) * dt, Eigen::Vector3d::UnitZ())
            ).toRotationMatrix();
        // clang-format on

        // 将小的旋转增量应用到当前的旋转上
        delta_R_ = delta_R_ * dR;

        // 更新速度，使用当前的旋转将加速度从IMU坐标系转换到全局坐标系
        // Δv = Δv + R*a * dt
        delta_v_ += delta_R_ * corrected_a * dt;
        // 更新位置，考虑当前速度和加速度影响
        // Δp = Δp + Δv * dt + 1/2 * R*a * dt^2  （速度项 和 加速度项）
        delta_p_ += delta_v_ * dt + 0.5 * delta_R_ * corrected_a * dt * dt;
    }

    // 获取预积分的结果
    Eigen::Vector3d GetDeltaP() const { return delta_p_; }
    Eigen::Vector3d GetDeltaV() const { return delta_v_; }
    Eigen::Matrix3d GetDeltaR() const { return delta_R_; }

private:
    Eigen::Vector3d delta_p_;  // 位置增量
    Eigen::Vector3d delta_v_;  // 速度增量
    Eigen::Matrix3d delta_R_;  // 旋转增量

    Eigen::Vector3d bias_a_;  // 加速度计偏置
    Eigen::Vector3d bias_g_;  // 陀螺仪偏置
};

int main() {
    // 创建IMU预积分器实例
    IMUPreintegrator integrator;

    // 假设这是从IMU传感器接收到的数据流
    std::vector<IMUData> imu_data_stream = {
        // 时间戳, 线加速度 (m/s^2), 角速度 (rad/s)
        {Eigen::Vector3d(0.1, 0.2, 9.8), Eigen::Vector3d(0.01, 0.02, 0.03), 0.00}, {Eigen::Vector3d(0.1, 0.2, 9.8), Eigen::Vector3d(0.01, 0.02, 0.03), 0.01},
        {Eigen::Vector3d(0.1, 0.2, 9.8), Eigen::Vector3d(0.01, 0.02, 0.03), 0.02}, {Eigen::Vector3d(0.1, 0.2, 9.8), Eigen::Vector3d(0.01, 0.02, 0.03), 0.03},
        {Eigen::Vector3d(0.1, 0.2, 9.8), Eigen::Vector3d(0.01, 0.02, 0.03), 0.04},
        // 填充IMU数据和时间戳...
    };

    // 假设我们已经知道了IMU的偏置
    Eigen::Vector3d bias_a(0.01, -0.01, 0.02);
    Eigen::Vector3d bias_g(0.001, -0.001, 0.001);
    integrator.SetBias(bias_a, bias_g);

    // 遍历IMU数据流进行预积分
    for (size_t i = 1; i < imu_data_stream.size(); ++i) {
        double dt = imu_data_stream[i].timestamp - imu_data_stream[i - 1].timestamp;
        integrator.Integrate(imu_data_stream[i], dt);
    }

    // 输出预积分的结果
    std::cout << "Delta Position: " << integrator.GetDeltaP().transpose() << std::endl;
    std::cout << "Delta Velocity: " << integrator.GetDeltaV().transpose() << std::endl;
    std::cout << "Delta Rotation matrix: " << std::endl << integrator.GetDeltaR() << std::endl;

    return 0;
}

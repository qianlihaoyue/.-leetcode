#include </usr/include/eigen3/Eigen/Dense>
#include <complex>
#include <cstdlib>
#include <eigen3/Eigen/src/Core/Matrix.h>
#include <eigen3/Eigen/src/Core/util/Constants.h>
#include <eigen3/Eigen/src/Eigenvalues/SelfAdjointEigenSolver.h>
#include <iostream>
#include <math.h>
#include <vector>

using Point = Eigen::Vector3f;
using Parama = Eigen::Vector4f;

#include "../timer.h"

#include <random>
std::vector<Point> generateNoisyPoints(const Parama &plane, int num_points, float noise_level) {
    std::vector<Point> data;
    data.reserve(num_points);

    // 随机数生成器
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(-1, 1);
    std::normal_distribution<> noise(0, noise_level);

    // 平面法向量和距离
    Eigen::Vector3f normal(plane[0], plane[1], plane[2]);
    float d = plane[3];

    // 随机生成点
    for (int i = 0; i < num_points; ++i) {
        // 随机生成一个点在平面上
        Eigen::Vector3f point;
        point[0] = dis(gen);  // x
        point[1] = dis(gen);  // y
        // z 根据平面方程计算
        point[2] = (-d - normal[0] * point[0] - normal[1] * point[1]) / normal[2];
        // 添加噪声
        Point noisy_point = point + Eigen::Vector3f(noise(gen), noise(gen), noise(gen));
        // 存储带噪声的点
        data.push_back(noisy_point);
    }

    return data;
}

float disPoint2Line(const Eigen::Vector3f normal, float d, const Eigen::Vector3f pt) { return std::fabs(pt.dot(normal) + d); }

bool esti_plane_gn(const std::vector<Point> &points, Parama &param, float kDisThre = 1e-1) {
    const int num_points = points.size();
    Eigen::MatrixXf J(num_points, 4);  // 构建雅可比矩阵
    Eigen::VectorXf r(num_points);     // 构建残差向量

    param << 1, 1, 1, 0;  // 随机初始化 A, B, C, D

    for (int iteration = 0; iteration < 10; ++iteration) {  // 假设最多迭代10次
        for (int i = 0; i < num_points; ++i) {
            const auto &p = points[i];
            // 计算雅可比矩阵的每一行
            J(i, 0) = p.x();
            J(i, 1) = p.y();
            J(i, 2) = p.z();
            J(i, 3) = 1;

            // 计算残差
            r(i) = p.dot(param.head(3)) + param(3);
        }

        // 使用高斯-牛顿公式更新参数
        Eigen::VectorXf delta = (J.transpose() * J).ldlt().solve(-J.transpose() * r);
        param += delta;

        // 检查收敛,提前跳出
        if (delta.norm() < kDisThre) {
            param /= param.head(3).norm();
            return true;
        }
    }

    return false;
}

/*
Eigen::MatrixXf H = Eigen::MatrixXf::Zero(4, 4);
Eigen::VectorXf b = Eigen::VectorXf::Zero(4);

for (int i = 0; i < num_points; ++i) {
    const auto &p = points[i];
    Eigen::Vector4f J;
    // 计算雅可比矩阵的每一行
    J(0) = p.x();
    J(1) = p.y();
    J(2) = p.z();
    J(3) = 1;

    // 计算残差
    float error = p.dot(param.head(3)) + param(3);
    H += J * J.transpose();  // 这种写法就是J * J^T,矩阵
    b += error * J;  // 同样
}

// 使用高斯-牛顿公式更新参数
Eigen::VectorXf delta = H.ldlt().solve(-b);
param += delta;

*/

// 最小二乘拟合？这真的是吗,好像没有构建正规方程，好像就是求了个Ax=b，使用Qr分解求解
bool esti_plane_cal(const std::vector<Point> &data, Parama &param, float kDisThre = 0.1, int mode = 1) {
    // 数据点不足以定义一个平面
    if (data.size() < 3) return false;

    // 初始化
    Eigen::MatrixXf A(data.size(), 3);
    Eigen::MatrixXf b(data.size(), 1);
    A.setZero(), b.setZero();

    // Ax+By+Cz+D=0     A/D x+ .. = -1
    for (int i = 0; i < data.size(); i++) {
        A(i, 0) = data[i].x(), A(i, 1) = data[i].y(), A(i, 2) = data[i].z();
        b(i, 0) = -1;
    }

    Eigen::Vector3f normal;
    if (mode == 1) normal = A.colPivHouseholderQr().solve(b).normalized();                                 // QR求解 A/D
    if (mode == 2) normal = A.jacobiSvd(Eigen::ComputeThinU | Eigen::ComputeThinV).solve(b).normalized();  // SVD

    // 归一化，不同的法向量就可以对比了
    param.head<3>() = normal;
    param(3) = -normal.dot(data[0]);

    // 检测异常值, 通过计算每个点距离平面的距离  d = | Ax + By + Cz + 1/n |
    for (auto &pt : data) {
        if (std::fabs(pt.dot(normal) + param(3)) > kDisThre) return false;
    }
    return true;
}

bool esti_plane_pca_svd(const Eigen::Matrix3Xf &data, Eigen::Vector4f &param, float kDisThre = 0.1) {
    if (data.size() < 3) return false;

    // 中心化数据
    Eigen::Vector3f center = data.rowwise().mean();
    Eigen::Matrix3Xf center_data = data.colwise() - center;

    // 使用SVD分解中心化数据矩阵
    Eigen::JacobiSVD<Eigen::MatrixXf> svd(center_data, Eigen::ComputeFullV);
    // 最小奇异值对应的奇异向量就是法线向量
    Eigen::Vector3f normal = svd.matrixV().col(2);

    param.head<3>() = normal;  // 通常已经归一化了
    param(3) = -normal.dot(center);

    for (int i = 0; i < data.cols(); i++)
        if (std::fabs(data.col(i).dot(normal) + param(3)) > kDisThre) return false;
    return true;
}

// mode = 1, PCA拟合平面，使用 特征值分解 实现PCA
// mode = 1, PCA拟合平面，使用SVD求解PCA
bool esti_plane_pca(const std::vector<Point> &data, Parama &param, float kDisThre = 0.1, int mode = 1) {
    if (data.size() < 3) return false;

    // 计算中心点
    Point centroid = Point::Zero();
    for (auto &pt : data) centroid += pt;
    centroid /= data.size();
    // 中心化数据
    Eigen::MatrixXf center_data(data.size(), 3);
    for (int i = 0; i < data.size(); i++) center_data.row(i) = data[i] - centroid;

    Eigen::Vector3f normal;
    //---------------------------------------------//
    if (mode == 1) {
        // 构建协方差矩阵
        Eigen::Matrix3f covmat = center_data.transpose() * center_data / static_cast<float>(data.size() - 1);
        // 计算特征值和特征向量
        Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eig(covmat);
        // 特征向量按照特征值排序，Eigen默认升序排列,
        normal = eig.eigenvectors().col(0);  // 最小特征值对应的特征向量，即为法线
    }
    //---------------------------------------------//
    if (mode == 2) {
        // 使用SVD分解中心化数据矩阵
        Eigen::JacobiSVD<Eigen::MatrixXf> svd(center_data, Eigen::ComputeFullV);
        // 最小奇异值对应的奇异向量就是法线向量
        normal = svd.matrixV().col(2);
    }
    //---------------------------------------------//

    // 平面方程为 normal.dot(X) = 0，（两条直线垂直）计算常数项d
    param.head<3>() = normal;  // 通常已经归一化了
    param(3) = -normal.dot(centroid);

    // 检测异常值, 通过计算每个点距离平面的距离  d = | Ax + By + Cz + 1/n |
    for (auto &pt : data) {
        if (std::fabs(pt.dot(normal) + param(3)) > kDisThre) return false;
    }
    return true;
}

bool esti_plane_ransac(const std::vector<Point> &data, Parama &param, float kDisThre = 0.1, float min_radio = 0.9, int max_iter = 100) {
    // 数据点不足以定义一个平面
    if (data.size() < 3) return false;

    float best_radio = 0;
    std::vector<int> idx(3);
    while (max_iter--) {
        // 随机采样,三个点确定一个平面
        for (int i = 0; i < 3; i++) idx[i] = rand() % data.size();
        // 计算向量
        Eigen::Vector3f v1 = data[idx[1]] - data[idx[0]];
        Eigen::Vector3f v2 = data[idx[2]] - data[idx[1]];
        // 计算法向量，即叉乘，外积, 同时正则化
        Eigen::Vector3f norm = v1.cross(v2).normalized();
        // 处理选择相同点，即相同向量 与 零向量叉乘
        if (norm.norm() == 0) continue;
        // 计算d
        float d = -norm.dot(data[idx[0]]);

        // 计算内点比例
        int inner_cnt = 0;
        for (auto &pt : data)
            if (std::fabs(norm.dot(pt) + d) < kDisThre) inner_cnt++;
        float radio = inner_cnt / (float)data.size();

        // 是否更优
        if (radio > best_radio) {
            best_radio = radio;
            param.head<3>() = norm;
            param(3) = d;
        }

        if (radio > min_radio) return true;  // 满足要求，提前跳出迭代
    }

    return false;  // 超出迭代次数限制
}

// 计算两个平面参数之间的差异
void evaluatePlaneDifference(bool esti_sta, const Parama &param_gt, const Parama &param_est) {
    if (esti_sta == false) {
        std::cerr << "error" << std::endl;
        return;
    }

    // 提取法向量部分
    Eigen::Vector3f n1 = param_gt.head<3>();
    Eigen::Vector3f n2 = param_est.head<3>();

    // 计算法向量之间的夹角（以度为单位）
    float angle = std::acos(n1.dot(n2) / (n1.norm() * n2.norm()));
    angle = angle * 180.0f / M_PI;  // 转换为度

    // 计算到原点的距离差异
    float distanceDiff = std::abs(param_gt[3] / n1.norm() - param_est[3] / n2.norm());

    std::cout << "Param: " << param_est[0] << " " << param_est[1] << " " << param_est[2] << " " << param_est[3] << " " << "Angle diff (deg): " << angle
              << " Dist to origin diff: " << distanceDiff << std::endl;
}

int main(int argc, char *argv[]) {
    // 填充数据
    std::vector<Point> data;

    Parama param_gt(2, 4, 3, 1);
    data = generateNoisyPoints(param_gt, 5, 0.02);

    Parama param;
    bool esti_sta;
    TIMER_CREATE(time_esti);

    //---------------------------------------------//
    std::cout << "esti_plane_gn" << std::endl;
    time_esti.tic();
    esti_sta = esti_plane_gn(data, param, 0.01);
    time_esti.toc_cout();
    evaluatePlaneDifference(esti_sta, param_gt, param);

    //---------------------------------------------//
    std::cout << "esti_plane_cal_qr" << std::endl;
    time_esti.tic();
    esti_sta = esti_plane_cal(data, param, 0.1, 1);
    time_esti.toc_cout();
    evaluatePlaneDifference(esti_sta, param_gt, param);

    //---------------------------------------------//
    std::cout << "esti_plane_cal_svd" << std::endl;
    time_esti.tic();
    esti_sta = esti_plane_cal(data, param, 0.1, 2);
    time_esti.toc_cout();
    evaluatePlaneDifference(esti_sta, param_gt, param);

    //---------------------------------------------//
    std::cout << "esti_plane_pca_eig" << std::endl;
    time_esti.tic();
    esti_sta = esti_plane_pca(data, param, 0.1, 1);
    time_esti.toc_cout();
    evaluatePlaneDifference(esti_sta, param_gt, param);

    //---------------------------------------------//
    std::cout << "esti_plane_pca_svd" << std::endl;
    time_esti.tic();
    esti_sta = esti_plane_pca(data, param, 0.1, 2);
    time_esti.toc_cout();
    evaluatePlaneDifference(esti_sta, param_gt, param);

    //---------------------------------------------//
    std::cout << "esti_plane_ransac" << std::endl;
    time_esti.tic();
    esti_sta = esti_plane_ransac(data, param, 0.1);
    time_esti.toc_cout();
    evaluatePlaneDifference(esti_sta, param_gt, param);

    return 0;
}

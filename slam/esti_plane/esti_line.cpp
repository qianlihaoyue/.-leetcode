
#include <eigen3/Eigen/Eigen>
#include <eigen3/Eigen/src/Core/Matrix.h>
#include <iostream>
#include <vector>

bool esti_plane_pca_svd(Eigen::Matrix3Xf data, Eigen::Vector4f& param) {
    if (data.size() < 3) return false;

    // 中心化数据
    Eigen::Vector3f center = data.rowwise().mean();
    data.colwise() -= center;

    // 使用SVD分解中心化数据矩阵
    Eigen::JacobiSVD<Eigen::MatrixXf> svd(data, Eigen::ComputeFullV);
    // 最小奇异值对应的奇异向量就是法线向量
    Eigen::Vector3f normal = svd.matrixV().col(2);

    // {                                    // 拟合直线
    //     lineDir = svd.matrixV().col(0);  // 最大奇异值对应的奇异向量即为直线的方向
    //     pointOnLine = center;            // 中心点即为直线上的一个点
    // }

    param.head<3>() = normal;  // 通常已经归一化了
    param(3) = -normal.dot(center);

    return true;
}




void esti_line_gn(const Eigen::MatrixXf& Data, int maxIter = 100, float tolerance = 1e-6) {
    // 初始化k和b
    float k = 0.0, b = 0.0;
    int N = Data.cols();  // 数据点的数量
    for (int iter = 0; iter < maxIter; ++iter) {
        Eigen::MatrixXf J(N, 2);  // 雅可比矩阵
        Eigen::VectorXf r(N);     // 残差向量

        for (int i = 0; i < N; ++i) {
            float x = Data(0, i);
            float y = Data(1, i);
            r(i) = y - (k * x + b);  // 计算残差
            J(i, 0) = -x;            // 对m的偏导
            J(i, 1) = -1;            // 对b的偏导
        }

        // 解更新量
        Eigen::Vector2f delta = (J.transpose() * J).ldlt().solve(-J.transpose() * r);
        if (delta.norm() < tolerance) {
            std::cout << "Converged in " << iter << " iterations." << std::endl;
            break;
        }

        // 更新k和b
        k += delta(0);
        b += delta(1);
    }

    std::cout << "m: " << k << ", b: " << b << std::endl;
}


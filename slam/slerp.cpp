#include </usr/include/eigen3/Eigen/Dense>
#include </usr/include/eigen3/Eigen/Geometry>

// #include <Eigen/Geometry>
#include <cmath>
#include <eigen3/Eigen/src/Geometry/Quaternion.h>
#include <iostream>

// t是时间比例，介于0-1
Eigen::Quaterniond slerp(const Eigen::Quaterniond& q1, const Eigen::Quaterniond& q2, double t) {
    // 计算四元数之间的点积, 单位向量的点积，数值等于cos θ
    double dot = q1.dot(q2);

    Eigen::Quaterniond q2_eff = q2;
    if (dot < 0.0) {
        q2_eff = Eigen::Quaterniond(-q2.coeffs());  // 直接反转q2的系数
        dot = -dot;
    }

    // 如果点积非常接近1，使用线性插值
    const double DOT_THRESHOLD = 0.9995;
    if (dot > DOT_THRESHOLD) {
        return Eigen::Quaterniond((1.0 - t) * q1.coeffs() + t * q2_eff.coeffs()).normalized();
    }

    // 计算插值的角度
    double theta_0 = std::acos(std::max(-1.0, std::min(1.0, dot)));  // 防止因浮点误差超出acos函数的输入范围
    double theta = theta_0 * t;                                      // 插值角度
    double sin_theta = std::sin(theta);                              // 插值角度的正弦值
    double sin_theta_0 = std::sin(theta_0);                          // 初始角度的正弦值

    double s1 = std::cos(theta) - dot * sin_theta / sin_theta_0;  // 用于q1的缩放因子
    double s2 = sin_theta / sin_theta_0;                          // 用于q2的缩放因子

    // 计算插值后的四元数
    return Eigen::Quaterniond(s1 * q1.coeffs() + s2 * q2_eff.coeffs()).normalized();
}

int main() {
    // 定义两个四元数
    Eigen::Quaterniond q1(Eigen::AngleAxisd(M_PI / 4, Eigen::Vector3d::UnitX()));
    Eigen::Quaterniond q2(Eigen::AngleAxisd(M_PI / 2, Eigen::Vector3d::UnitX()));

    // 定义插值参数
    double t = 0.5;

    // 使用手动实现的slerp函数进行插值
    Eigen::Quaterniond q_interpolated = slerp(q1, q2, t);

    // 输出插值结果
    std::cout << "Interpolated quaternion: \n" << q_interpolated << std::endl;

    return 0;
}

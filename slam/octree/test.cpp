#include <iostream>

#include "kdtree2.hpp"

int test_kdtree2() {
    std::vector<Point> points = {{2, 3}, {5, 4}, {9, 6}, {4, 7}, {8, 1}, {7, 2}};
    // KDTree tree;
    // tree.input_data(points);

    // // 查询最近点
    // Point queryPoint(5, 4.5), result;
    // float distance = std::numeric_limits<float>::max();  // 初始化为最大浮点数
    // tree.find_nearest_point(queryPoint, result, distance);

    // // 输出结果
    // std::cout << "Nearest point to : (" << result.pt[0] << ", " << result.pt[1] << ")" << std::endl;
    // std::cout << "Distance: " << distance << std::endl;

    Point pa = {2, 3};
    Point pb = {4, 1};

    std::cout << (pa < pb);
    pa.print();

    return 0;
}

// #include "octree.hpp"
// int test_octree() {
//     // 创建一个八叉树，覆盖一个较大的空间区域
//     Octree tree(Point(0, 0, 0), Point(100, 100, 100));

//     // 插入一些点到八叉树中
//     Point* pt1 = new Point(10, 10, 10);
//     Point* pt2 = new Point(20, 20, 20);
//     Point* pt3 = new Point(30, 30, 30);

//     tree.insert(pt1);
//     tree.insert(pt2);
//     tree.insert(pt3);

//     // 测试 find 方法
//     if (tree.find(*pt1))
//         std::cout << "Point 1 found." << std::endl;
//     else
//         std::cout << "Point 1 not found." << std::endl;
//     if (tree.find(*pt2))
//         std::cout << "Point 2 found." << std::endl;
//     else
//         std::cout << "Point 2 not found." << std::endl;
//     if (tree.find(*pt3))
//         std::cout << "Point 3 found." << std::endl;
//     else
//         std::cout << "Point 3 not found." << std::endl;

//     // 测试一个不存在的点
//     Point notExist(50, 50, 50);
//     if (tree.find(notExist))
//         std::cout << "Point notExist found, which shouldn't happen." << std::endl;
//     else
//         std::cout << "Point notExist correctly not found." << std::endl;

//     return 0;
// }

int main() {
    test_kdtree2();
    // test_octree();

    return 0;
}
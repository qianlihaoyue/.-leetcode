#include <vector>

// https://github.com/brandonpelfrey/SimpleOctree

struct Point {
    float x, y, z;
    Point(float x_ = 0, float y_ = 0, float z_ = 0) : x(x_), y(y_), z(z_) {}
};

class Octree {
    Point* data = nullptr;
    Point min, mid, max;
    Octree* children[8] = {nullptr};

public:
    Octree(const Point& min_, const Point& max_) : min(min_), max(max_) { mid = {(min.x + max.x) / 2, (min.y + max.y) / 2, (min.z + max.z) / 2}; }

    ~Octree() {
        delete data;
        for (auto& child : children) delete child;
    }

    bool isLeafNode() const { return children[0] == nullptr; }

    int getOctIdx(const Point& pt) const { return (pt.x >= mid.x) << 2 | (pt.y >= mid.y) << 1 | (pt.z >= mid.z); }

    void insert(Point* pt) {
        // 空节点
        if (isLeafNode() && data == nullptr) {
            data = pt;
        } else {
            // 已存在点，分裂
            if (data != nullptr) {
                Point* oldPt = data;
                data = nullptr;
                createChildren();
                children[getOctIdx(*oldPt)]->insert(oldPt);
            }
            children[getOctIdx(*pt)]->insert(pt);
        }
    }

    bool find(const Point& pt) const {
        if (isLeafNode()) {
            if (data != nullptr && pt.x == data->x && pt.y == data->y && pt.z == data->z) return true;
            return false;
        }
        return children[getOctIdx(pt)]->find(pt);
    }

private:
    void createChildren() {
        for (int i = 0; i < 8; ++i) {
            Point newMin = min, newMax = max;
            if (i & 4)
                newMin.x = mid.x;
            else
                newMax.x = mid.x;
            if (i & 2)
                newMin.y = mid.y;
            else
                newMax.y = mid.y;
            if (i & 1)
                newMin.z = mid.z;
            else
                newMax.z = mid.z;
            children[i] = new Octree(newMin, newMax);
        }
    }
};


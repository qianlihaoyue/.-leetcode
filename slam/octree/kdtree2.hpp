/*
      1
     / \
    2   3
   / \ / \
  4  5 6  7
*/
#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>

const int kdim = 2;  // 2D-tree

struct Point {
    float pt[2];
    static int idx;                                                       // 静态，用于比较节点时指定比较的维度
    bool operator<(const Point& u) const { return pt[idx] < u.pt[idx]; }  // u是另一个要对比的点
    // bool operator()(const Point& p1, const Point& p2) { return p1.pt[idx] > p2.pt[idx]; }
    Point() : pt{0, 0} {}
    Point(float x0, float y0) : pt{x0, y0} {}
    void print(){std::cout<< pt[0] <<std::endl;}
};

int Point::idx = 0;

class KDTree {
public:
    KDTree() {}
    ~KDTree() {}

    int input_data(const std::vector<Point>& data) {
        _find_nth = data;  // deep copy

        int len = data.size();
        _data.resize(len * 4, Point());  // 假设树的深度不超过log2(N)，每层扩展两个节点，预分配足够空间
        _flag.resize(len * 4, 0);        // 将_vector的大小设置为len * 4，并用0填充

        build(0, len - 1, 1, 0);
        _find_nth.clear();
        return 0;
    }

    // 建立kd-tree，lr 为左右范围，rt为当前索引，dept为深度
    void build(int l, int r, int idx, int dept) {
        if (l > r) return;  // 终止条件

        Point::idx = dept % kdim;   // 顺序遍历x,y,z,x,y...
        int mid = l + (r - l) / 2;  // 计算中间点，(l + r + 1) >> 1;
        std::nth_element(_find_nth.begin() + l, _find_nth.begin() + mid, _find_nth.begin() + r + 1);

        _data[idx] = _find_nth[mid];  // 中点添加
        _flag[idx] = 1;               // 将该点标记为1

        build(l, mid - 1, 2 * idx, dept + 1);  // 递归左子树
        build(mid + 1, r, 2 * idx + 1, dept + 1);
    }

    void find_nearest_point(const Point& pt, Point& res, float& dist) {
        dist = std::numeric_limits<float>::max();
        query(pt, res, dist, 1, 0);
    }
    // find_k_nearest_points 需要建Heap，也就是priority_queue

    // 计算两点距离的平方
    float distance(const Point& x, const Point& y) {
        float res = 0;
        for (int i = 0; i < kdim; i++) res += (x.pt[i] - y.pt[i]) * (x.pt[i] - y.pt[i]);
        return res;
    }

    void clean() {
        _find_nth.clear();
        _data.clear();
        _flag.clear();
    }

private:
    std::vector<Point> _data;      // 用vector模拟数组，存储KD树的节点数据
    std::vector<int> _flag;        // 标记节点是否存在，0表示不存在，1表示存在
    std::vector<Point> _find_nth;  // 临时存储输入的节点，用于构建KD树

    // 查找kd-tree距离p最近的点
    void query(const Point& p, Point& res, float& dist, int idx, int dept) {
        if (_flag[idx] == 0) return;  // 不存在的节点不遍历

        int dim = dept % kdim;                                          // 和建树一样, 保证相同节点的dim值不变
        int first = 2 * idx, second = 2 * idx + 1;                      // 决定先遍历哪个子树
        if (p.pt[dim] >= _data[idx].pt[dim]) std::swap(first, second);  // 右边更大

        // if (_flag[first])
        query(p, res, dist, first, dept + 1);

        // 本节点距离
        float cur_dist = distance(_data[idx], p);
        if (cur_dist < dist) {
            res = _data[idx];
            dist = cur_dist;
        }
        // 该轴的距离
        cur_dist = (p.pt[dim] - _data[idx].pt[dim]) * (p.pt[dim] - _data[idx].pt[dim]);

        // 是否需要遍历右子树，还需要继续回溯
        if (cur_dist < dist) query(p, res, dist, second, dept + 1);
    }
};

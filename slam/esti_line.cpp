#include <cmath>
#include <cstdlib>
#include <eigen3/Eigen/src/Core/Matrix.h>
#include <iostream>
#include <vector>

struct Point {
    float x;
    float y;
};

// 直线模型参数
struct Parameter {
    float a = 0;
    float b = 0;
};

// 点线距离
inline float pointToLineDist(const Point &p, const Parameter &param) {
    // d = |ax+by+c| / sqrt(a^2+b^2)
    // d = |ax-y+b| / sqrt(a^2+1)
    return std::abs(param.a * p.x - p.y + param.b) / std::sqrt(param.a * param.a + 1);
}

// GT
#include <random>
std::vector<Point> generateNoisyData(float a, float b, int numPoints, float noiseLevel) {
    std::vector<Point> data;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::normal_distribution<> d(0, noiseLevel);  // Normal distribution centered at 0
    for (int i = 0; i < numPoints; ++i) {
        float x = i;                   // x values are from 0 to numPoints - 1
        float y = a * x + b + d(gen);  // Calculate y with added noise
        data.push_back({x, y});
    }
    return data;
}

// ransac 拟合
bool esti_line_ransac(const std::vector<Point> &data, Parameter &best_param, int kMaxIter = 100, float min_radio = 0.8, float sigma = 1.0) {
    // temp
    int p1, p2;
    float inner_radio_best = 0;

    // 迭代
    for (int iter = 0; iter < kMaxIter; iter++) {
        // 随机采样两个点
        p1 = rand() % data.size();
        p2 = rand() % data.size();
        while (p2 == p1) p2 = rand() % data.size();

        // 两点式构造  y=ax+b
        Parameter cur_param;
        cur_param.a = (data[p2].y - data[p1].y) / (data[p2].x - data[p1].x);
        cur_param.b = data[p2].y - cur_param.a * data[p2].x;

        // 遍历所有数据
        int inner_cnt = 0;
        for (auto &pt : data)
            if (pointToLineDist(pt, cur_param) < sigma) inner_cnt++;
        // 计算内点占比
        float inner_radio = (float)inner_cnt / data.size();

        // 判断是否比上次更优
        if (inner_radio > inner_radio_best) {
            inner_radio_best = inner_radio;
            best_param = cur_param;
        }

        // 判断是否可以接受
        if (inner_radio > min_radio) return true;
    }
    return false;
}

bool esti_line_ls(const std::vector<Point> &data, Parameter &best_param) {
    // y = ax+b    x (a) + 1(b) = y
    Eigen::Matrix<float, data.size(), 2> A;
    Eigen::Matrix<float, data.size(), 1> b;
    for (int i = 0; i < data.size(); i++) A(i, 0) = data[i].x, A(i, 1) = 1.0, b(i, 0) = data[i].y;

    Eigen::Matrix<float, data.size(), 2> res = 

    return true;
}

int main(int argc, char *argv[]) {
    // data
    float a = 2.0, b = 1.0, noiseLevel = 1.0;
    std::vector<Point> data = generateNoisyData(a, b, 50, noiseLevel);

    // config
    const int kMaxIter = 100;
    const float inner_radio_thre = 0.8;  // 所需内点占比阈值
    const float sigma = 1.0;             // 容许的误差
    Parameter best_param;

    if (esti_line_ransac(data, best_param, kMaxIter, inner_radio_thre, sigma))
        std::cout << "y=" << best_param.a << "x+" << best_param.b << std::endl;
    else
        std::cerr << "esti error" << std::endl;

    return 0;
}

#include </usr/include/eigen3/Eigen/Dense>

class KalmanFilter {
public:
    // 状态空间的维度n,观察模型的维度m
    KalmanFilter(int n, int m) {
        A = Eigen::MatrixXd::Identity(n, n);  // 状态转移矩阵
        H = Eigen::MatrixXd::Identity(m, n);  // 观察模型矩阵
        Q = Eigen::MatrixXd::Identity(n, n);  // 系统噪声协方差矩阵
        R = Eigen::MatrixXd::Identity(m, m);  // 观察噪声协方差矩阵
        P = Eigen::MatrixXd::Identity(n, n);  // 误差协方差矩阵
        x_hat = Eigen::VectorXd::Zero(n);     // 初始状态估计值
    }

    void predict() {
        x_hat = A * x_hat;              // 预测下一个状态
        P = A * P * A.transpose() + Q;  // 更新误差协方差
    }

    void update(const Eigen::VectorXd& y) {
        Eigen::VectorXd y_tilde = y - H * x_hat;              // 计算残差
        Eigen::MatrixXd S = H * P * H.transpose() + R;        // 计算残差协方差
        Eigen::MatrixXd K = P * H.transpose() * S.inverse();  // 计算卡尔曼增益

        x_hat = x_hat + K * y_tilde;                                      // 更新状态估计值
        P = (Eigen::MatrixXd::Identity(P.rows(), P.cols()) - K * H) * P;  // 更新误差协方差
    }

    Eigen::VectorXd state() { return x_hat; }

private:
    Eigen::MatrixXd A, H, Q, R, P;
    Eigen::VectorXd x_hat;
};

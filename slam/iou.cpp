#include <iostream>
#include <algorithm>

struct Rect {
    float x1, y1, x2, y2;
};

// calculate the area of a rectangle
float area(const Rect& rect) { return (rect.x2 - rect.x1) * (rect.y2 - rect.y1); }

float intersection_area(const Rect& rect1, const Rect& rect2) {
    // [x,y] min2 - max 1
    float x_overlap = std::min(rect1.x2, rect2.x2) - std::max(rect1.x1, rect2.x1);
    float y_overlap = std::min(rect1.y2, rect2.y2) - std::max(rect1.y1, rect2.y1);

    // 处理没有重叠
    x_overlap = std::max(0.0f, x_overlap);
    y_overlap = std::max(0.0f, y_overlap);

    return x_overlap * y_overlap;
}

float calculate_iou(const Rect& rect1, const Rect& rect2) {
    // 相交区域
    float intersection = intersection_area(rect1, rect2);
    // 合并区域
    float union_area = area(rect1) + area(rect2) - intersection;
    return intersection / union_area;
}

int main() {
    // Define two rectangles
    Rect rect1 = {0, 0, 3, 3};  // (x1, y1, x2, y2)
    Rect rect2 = {1, 1, 4, 4};

    // Calculate IoU
    float iou = calculate_iou(rect1, rect2);
    std::cout << "IoU: " << iou << std::endl;

    return 0;
}

// #include <Eigen/Dense>
// #include <Eigen/src/Core/Matrix.h>
// #include <Eigen/src/Core/util/Constants.h>
// #include <Eigen/src/SVD/JacobiSVD.h>

// #include <Eigen/src/Core/Matrix.h>
#include <eigen3/Eigen/Eigen>
#include <iostream>
#include "common.hpp"
#include "../timer.h"

// ICP解析解 - SVD
void icp_solve_svd(Eigen::Matrix3Xf src, Eigen::Matrix3Xf dst, Eigen::Matrix4f& T) {
    // 计算质心,转到质心，相当于不考虑平移，都在原点
    Eigen::Vector3f centerA = src.rowwise().mean();
    Eigen::Vector3f centerD = dst.rowwise().mean();
    src.colwise() -= centerA;
    dst.colwise() -= centerD;

    // 计算协方差矩阵
    Eigen::Matrix3f H = src * dst.transpose();
    // 对协方差矩阵进行svd, 可以得到 H 的最主要的线性特征
    // 寻找最佳的旋转  min sum(Ra-b) = 最大化 tr(R H^T)    H = U Σ V^T , U 和 V 是正交矩阵，Σ 是对角矩阵
    Eigen::JacobiSVD<Eigen::Matrix3f> svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV);
    Eigen::Matrix3f U = svd.matrixU(), V = svd.matrixV();
    // 计算旋转矩阵 R=V U^T = (U V^T)^T
    // 也可用 U V^T 也就是转置，表示dst到src 
    Eigen::Matrix3f R = V * U.transpose();
    // 确保一个合适的旋转（处理特殊情况：R包含反射），SVD也可能导致反射
    if (R.determinant() < 0) {
        V.col(2) *= -1;
        R = V * U.transpose();
    }

    // 计算平移向量
    Eigen::Vector3f t = centerD - R * centerA;
    // 构造变换矩阵
    T = Eigen::Matrix4f::Identity();
    T.block<3, 3>(0, 0) = R;
    T.block<3, 1>(0, 3) = t;
}

#define USE_SOPHUS 1

#if USE_SOPHUS
#include <sophus/so3.hpp>
#endif

#define SKEW_SYM_MATRX(v) 0.0, -v[2], v[1], v[2], 0.0, -v[0], -v[1], v[0], 0.0

inline Eigen::Matrix3f skew_sym_hat(const Eigen::Vector3f& v) {
    Eigen::Matrix3f skew;
    skew << 0, -v.z(), v.y(), v.z(), 0, -v.x(), -v.y(), v.x(), 0;
    return skew;
}

// 实现从旋转向量到旋转矩阵的转换
Eigen::Matrix3f expSO3(const Eigen::Vector3f& omega) {
    Eigen::Matrix3f R = Eigen::Matrix3f::Identity();
    float theta = omega.norm();  // 旋转角度
    if (theta > 1e-6) {
        Eigen::Vector3f axis = omega / theta;  // 旋转轴
        Eigen::Matrix3f K = skew_sym_hat(axis);
        R += std::sin(theta) * K + (1 - std::cos(theta)) * K * K;  /// Roderigous Tranformation
    }
    return R;
}

// 目标函数是 sum 原点到目标点的距离平方和
void icp_solve_GN(const Eigen::Matrix3Xf& A_, const Eigen::Matrix3Xf& B_, Eigen::Matrix4f& T) {
    // 初始化 H, b
    Eigen::Matrix<float, 6, 6> H = Eigen::Matrix<float, 6, 6>::Zero();  // J^TJ
    Eigen::Matrix<float, 6, 1> b = Eigen::Matrix<float, 6, 1>::Zero();  // J^Te

    for (size_t i = 0; i < A_.cols(); ++i) {
        Eigen::Vector3f source_pt = A_.col(i);
        Eigen::Vector3f target_pt = B_.col(i);

        // 计算雅可比矩阵
        Eigen::Matrix<float, 3, 6> jacobian = Eigen::Matrix<float, 3, 6>::Zero();
        // 旋转部分 J = -skew(a), 负号代表减少残差
#if USE_SOPHUS
        jacobian.block<3, 3>(0, 0) = -Sophus::SO3f::hat(source_pt);
#else
        jacobian.block<3, 3>(0, 0) = -skew_sym_hat(source_pt);
#endif
        // 对于平移部分，就是单位阵
        jacobian.block<3, 3>(0, 3) = Eigen::Matrix3f::Identity();

        // 计算残差，点到点的向量差异
        Eigen::Vector3f residual = source_pt - target_pt;

        // 更新 H, b   H = sum J^T J   b = -sum J^T r
        H += jacobian.transpose() * jacobian;
        b += -jacobian.transpose() * residual;
    }
    // delta_x为李代数，前三项是轴角，后三项是平移，R为对应的李群

    // 求解 Hx = b
    Eigen::Matrix<float, 6, 1> delta_x = H.ldlt().solve(b);

    // 更新 R, t
    Eigen::Matrix3f R;
#if USE_SOPHUS
    R = Sophus::SO3f::exp(delta_x.block<3, 1>(0, 0)).matrix();
#else
    R = expSO3(delta_x.block<3, 1>(0, 0));
#endif
    Eigen::Vector3f t = delta_x.block<3, 1>(3, 0);

    T.setIdentity();
    T.block<3, 3>(0, 0) = R;
    T.block<3, 1>(0, 3) = t;
}

int main() {
    Eigen::Matrix4f gt_transform = Eigen::Matrix4f::Identity();
    float angle = M_PI / 4;  // 定义旋转角度45度
    Eigen::Matrix3f R;       // 定义旋转矩阵（绕Z轴）
    R = Eigen::AngleAxisf(angle, Eigen::Vector3f::UnitZ()).toRotationMatrix();
    Eigen::Vector3f t(0.5, 0.5, 1.0);
    // 将旋转和平移填充到变换矩阵
    gt_transform.block<3, 3>(0, 0) = R;
    gt_transform.block<3, 1>(0, 3) = t;

    int numPoints = 100;
    Eigen::Matrix3Xf oriPoints = generatePointCloud(numPoints, 100);
    Eigen::Matrix3Xf tranPoints = applyTransformation(oriPoints, R, t);
    float noiseLevel = 10;
    Eigen::Matrix3Xf noisyTransformedPoints = addNoise(tranPoints, noiseLevel);

    Eigen::Matrix4f esti_transform;

    esti_transform = Eigen::Matrix4f::Identity();
    TIMER_CREATE(tim_svd);
    icp_solve_svd(oriPoints, tranPoints, esti_transform);
    tim_svd.toc_cout();
    evaluatePoseError(esti_transform, gt_transform);

    esti_transform = Eigen::Matrix4f::Identity();
    TIMER_CREATE(tim_gn);
    icp_solve_GN(oriPoints, tranPoints, esti_transform);
    tim_gn.toc_cout();
    evaluatePoseError(esti_transform, gt_transform);

    return 0;
}

// #include <pcl/kdtree/kdtree_flann.h>
// #include <ceres/ceres.h>
// #include <ceres/solver.h>
// #include <ceres/rotation.h>

// class IcpCeres {
// public:
//     IcpCeres() { *tree = new pcl::KdTreeFLANN<PointType>(); }

//     void setInputCloud(const CloudPtr& input_) {
//         // 改成复制
//         *input = *input_;
//     }
//     void setTargetCloud(const CloudPtr& target_) {
//         target = target_;
//         tree->setInputCloud(target_);
//     }
//     bool align() {
//         // 创建优化问题
//         ceres::Problem problem;
//         ceres::LossFunction* loss_function = new ceres::HuberLoss(0.1);

//         int pt_size = input->points.size();
//         Eigen::Vector3d rotation_vector(0, 0, 0);     // 旋转向量
//         Eigen::Vector3d translation_vector(0, 0, 0);  // 平移向量

//         // 如果有初始位姿
//         for (int iter = 0; iter < max_iter; iter++) {
//             CloudType corr_cloud;
//             corr_cloud.reserve(pt_size);

//             // 查找最近邻,根据输入点云来,omp并行加速
//             std::vector<int> kd_idx(1);
//             std::vector<float> kd_dis(1);
//             for (int i = 0; i < pt_size; i++) {
//                 tree->nearestKSearch(input->points[i], 1, kd_idx, kd_dis);
//                 corr_cloud.emplace_back(target->points[kd_dis[0]]);
//             }
//             // 计算残差
//             for (int i = 0; i < pt_size; i++) {
//                 ceres::CostFunction* cost_function = new ceres::AutoDiffCostFunction<IcpError, 3, 3, 3>(new IcpError(input->points[i], corr_cloud[i]));
//                 problem.AddResidualBlock(cost_function, loss_function, rotation_vector, translation_vector);
//             }

//             // 构建ceres计算
//             ceres::Solver::Options option;
//             option.linear_solver_type = ceres::DENSE_QR;
//             option.max_num_iterations = 10;

//             ceres::Solver::Summary summary;
//             ceres::Solve(option, &problem, &summary);

//             // 更新点云
//             pcl::transformPointCloud(*input, *input, T);

//             // 检查收敛条件
//         }

//         return true;
//     }

//     bool align(CloudPtr* align_cloud) { return true; }

// private:
//     int max_iter = 100;
//     CloudPtr input;
//     CloudPtr target;
//     pcl::KdTreeFLANN<PointType>::Ptr tree;

//     // Ceres 计算残差
//     struct IcpError {
//         IcpError(PointType input_, PointType target_) : input_pcl(input_), target_pcl(target_) {}
//         template <typename T>
//         bool operator()(const T* const rot, const T* const tran, T* residu) {
//             using vec3 = Eigen::Matrix<T, 3, 1>;
//             // 获取点
//             vec3 input_eig(input_pcl.x, input_pcl.y, input_pcl.z);
//             vec3 target_eig(target_pcl.x, target_pcl.y, target_pcl.z);

//             ceres::AngleAxisRotatePoint(rot, input_eig.data(), input_eig.data());  // 应用旋转
//             input_eig += Eigen::Map<const vec3>(tran);                             // 应用平移
//             input_eig -= target_eig;                                               // 计算残差

//             // 将计算的残差赋值到输出参数
//             residu[0] = input_eig[0], residu[1] = input_eig[1], residu[2] = input_eig[2];
//             return true;
//         }

//         PointType input_pcl, target_pcl;
//     };
// };

// int main(int argc, char* argv[]) { return 0; }
#include <iostream>
#include <eigen3/Eigen/Dense>
#include <random>

Eigen::Matrix3Xf generatePointCloud(int numPoints, int range) {
    Eigen::Matrix3Xf points(3, numPoints);
    std::default_random_engine generator;
    std::uniform_real_distribution<float> distribution(0, range);

    for (int i = 0; i < numPoints; ++i) {
        points(0, i) = distribution(generator);  // X
        points(1, i) = distribution(generator);  // Y
        points(2, i) = distribution(generator);  // Z
    }
    return points;
}

// Applying rotation and translation
Eigen::Matrix3Xf applyTransformation(const Eigen::Matrix3Xf& points, const Eigen::Matrix3f& R, const Eigen::Vector3f& t) { return (R * points).colwise() + t; }

Eigen::Matrix3Xf addNoise(const Eigen::Matrix3Xf& points, float noiseLevel) {
    Eigen::Matrix3Xf noisyPoints = points;
    std::default_random_engine generator;
    std::normal_distribution<float> distribution(0.0, noiseLevel);

    for (int i = 0; i < points.rows(); ++i) {
        for (int j = 0; j < points.cols(); ++j) noisyPoints(i, j) += distribution(generator);
    }
    return noisyPoints;
}

// Function to evaluate the esti pose error against the ground truth
void evaluatePoseError(const Eigen::Matrix4f& estiTransform, const Eigen::Matrix4f& gtTransform) {
    // Compute the difference in translation
    Eigen::Vector3f estiTranslation = estiTransform.block<3, 1>(0, 3);
    Eigen::Vector3f gtTranslation = gtTransform.block<3, 1>(0, 3);
    float translationError = (estiTranslation - gtTranslation).norm();

    // Compute the difference in rotation
    Eigen::Matrix3f estiRotation = estiTransform.block<3, 3>(0, 0);
    Eigen::Matrix3f gtRotation = gtTransform.block<3, 3>(0, 0);
    Eigen::Matrix3f rotationDiff = estiRotation.transpose() * gtRotation;
    float angleError = acos((rotationDiff.trace() - 1) / 2);  // In radians

    std::cout << "GT : " << std::endl << gtTransform << std::endl;
    std::cout << "Esti : " << std::endl << estiTransform << std::endl;
    std::cout << "Translation error: " << translationError << std::endl;
    std::cout << "Rotation error (radians): " << angleError << std::endl;
}

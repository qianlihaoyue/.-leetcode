
#include <Eigen/Dense>

#include <Eigen/src/Core/Matrix.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/common.h>
#include <pcl/io/pcd_io.h>

#include <unordered_map>
#include <vector>
#include <iostream>

#include "../esti_plane/timer.h"
#include <omp.h>

using PointType = pcl::PointXYZ;
using CloudType = pcl::PointCloud<PointType>;
using CloudPtr = CloudType::Ptr;

struct Point {
    // 如果单纯降采样，可以使用Eigen
    PointType pt = PointType(0, 0, 0);
    int cnt = 0;
};

void voxel_filter(const CloudPtr &cloud_in, CloudPtr &cloud_out, float vox_size = 0.1) {
    // 计算包围盒
    PointType min_pt, max_pt;
    pcl::getMinMax3D(*cloud_in, min_pt, max_pt);

    // float vox_size = 0.1;
    int x_len = (max_pt.x - min_pt.x) / vox_size + 1;
    int y_len = (max_pt.y - min_pt.y) / vox_size + 1;
    int z_len = (max_pt.z - min_pt.z) / vox_size + 1;

    // std::vector<std::vector<std::vector<Point>>> Voxel;
    std::unordered_map<int, Point> Voxel;

    // openmp 加速
    // 线程不安全
    for (auto &pt : cloud_in->points) {
        int x_idx = (pt.x - min_pt.x) / vox_size;
        int y_idx = (pt.y - min_pt.y) / vox_size;
        int z_idx = (pt.z - min_pt.z) / vox_size;
        int idx = z_idx * x_len * y_len + y_idx * x_len + x_idx;  // 计算唯一索引
        auto iter = Voxel.find(idx);
        if (iter != Voxel.end()) {
            iter->second.pt.x += pt.x, iter->second.pt.y += pt.y, iter->second.pt.z += pt.z;
            iter->second.cnt++;
        } else {
            Voxel[idx].pt = pt;
            Voxel[idx].cnt++;
        }
    }

    // 遍历容器
    // CloudPtr filter_cloud(new CloudType());
    cloud_out->clear();
    for (auto &vox : Voxel) {
        auto &v = vox.second;
        v.pt.x /= v.cnt, v.pt.y /= v.cnt, v.pt.z /= v.cnt;
        cloud_out->push_back(v.pt);
    }
}

int main(int argc, char *argv[]) {
    CloudPtr cloud(new CloudType());
    CloudPtr cloud_ds(new CloudType());
    // 填充数据

    pcl::io::loadPCDFile("./0.pcd", *cloud);
    std::cout << "cloud: " << cloud->points.size() << std::endl;
    TIMER_CREATE(time_vox);
    voxel_filter(cloud, cloud_ds, 1);
    time_vox.toc_cout();
    std::cout << "cloud_ds: " << cloud_ds->points.size() << std::endl;

    pcl::io::savePCDFileBinary("./0_1.pcd", *cloud_ds);

    return 0;
}


struct M_POINT {
    float xyz[3];
    int count = 0;
};

// 每个体素格中所有点替换为一个点(均值)，并添加了一个计数count
void downsample_voxel(pcl::PointCloud<PointType> &pc, double voxel_size) {
    if (voxel_size < 0.01) return;

    std::unordered_map<VOXEL_LOC, M_POINT> feature_map;
    size_t pt_size = pc.size();

    for (size_t i = 0; i < pt_size; i++) {
        PointType &pt_trans = pc[i];
        float loc_xyz[3];
        for (int j = 0; j < 3; j++) {
            loc_xyz[j] = pt_trans.data[j] / voxel_size;
            if (loc_xyz[j] < 0) loc_xyz[j] -= 1.0;
        }

        VOXEL_LOC position((int64_t)loc_xyz[0], (int64_t)loc_xyz[1], (int64_t)loc_xyz[2]);
        auto iter = feature_map.find(position);
        if (iter != feature_map.end()) {
            iter->second.xyz[0] += pt_trans.x;
            iter->second.xyz[1] += pt_trans.y;
            iter->second.xyz[2] += pt_trans.z;
            iter->second.count++;
        } else {
            M_POINT anp;
            anp.xyz[0] = pt_trans.x;
            anp.xyz[1] = pt_trans.y;
            anp.xyz[2] = pt_trans.z;
            anp.count = 1;
            feature_map[position] = anp;
        }
    }

    pt_size = feature_map.size();
    pc.clear();
    pc.resize(pt_size);

    size_t i = 0;
    for (auto iter = feature_map.begin(); iter != feature_map.end(); ++iter) {
        pc[i].x = iter->second.xyz[0] / iter->second.count;
        pc[i].y = iter->second.xyz[1] / iter->second.count;
        pc[i].z = iter->second.xyz[2] / iter->second.count;
        i++;
    }
}

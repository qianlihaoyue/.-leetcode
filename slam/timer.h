/**
 * @author qianlihaoyue (qianlihaoyue@gmail.com)
 * @brief 计时工具
 * @version 0.3
 * @date 2024-01-30
 */
#pragma once

#include <chrono>
#include <iostream>

class Timer {
public:
    // 初始化的时候会记载一次开始时间，const变量只能在构造函数的初始化列表中进行初始化
    Timer() { tic(); }
    Timer(const std::string& nameIn) : name(nameIn) { tic(); }

    // 重置开始时间
    void tic() { start = std::chrono::system_clock::now(); }

    // 计算结束时间
    double toc() {
        end = std::chrono::system_clock::now();
        std::chrono::duration<double> dt = end - start;
        return dt.count() * 1000;
    }

    // 含有输出的结束
    void toc_cout() { std::cout << "[" << name << "]:" << toc() << "ms" << std::endl; }

private:
    const std::string name;
    std::chrono::time_point<std::chrono::system_clock> start, end;
};

#define TIMER_CREATE(name) Timer name(#name)

#include <iomanip>
#include <sstream>
#include <fstream>
#include <cmath>
#include <numeric>

#include <string>
#include <vector>
#include <map>

// 用于时间分析
class TimerRecord {
public:
    // STL会自动处理
    void add_record(const std::string& name, double time) { records_[name].push_back(time); }
    void dump_to_file(const std::string& file_name, bool print_flag = false) {
        // 创建一个字符串流
        std::stringstream ss;
        std::ofstream ofs(file_name, std::ios::out);

        if (!ofs.is_open()) {
            std::cerr << "Failed to open file: " << file_name << "!!!" << std::endl;
            return;
        } else
            std::cout << "Dump Time Records into file: " << file_name << std::endl;

        ss << ">>> ===== Printing run time =====" << std::endl;
        if (print_flag) std::cout << ss.str();
        ofs << ss.str(), ss.str("");

        // 设置列宽
        const int nameWidth = 40;
        const int scoreWidth = 15;

        // 输出表头
        ss << std::left << std::setw(nameWidth) << "Function"  //
           << std::setw(scoreWidth) << "Ave time"              // 平均运行时间
           << std::setw(scoreWidth) << "S.D time"              // 运行时间标准差
           << std::setw(scoreWidth) << "Called times"          // 调用次数
           << std::setw(scoreWidth) << "All time"              // 平均时间*调用次数
           << std::endl;
        if (print_flag) std::cout << ss.str();
        ofs << ss.str(), ss.str("");

        // 输出分隔线
        ss << std::setfill('-') << std::setw(nameWidth + 4 * scoreWidth) << "" << std::setfill(' ') << std::endl;
        if (print_flag) std::cout << ss.str();
        ofs << ss.str(), ss.str("");

        // 遍历计算每一项
        for (auto& r : records_) {
            std::vector<double>& tim = r.second;
            double ave_time = std::accumulate(tim.begin(), tim.end(), 0.0) / double(tim.size());  // 时间均值
            // 计算方差
            double variance = 0.0;
            for (const auto& num : tim) variance += (num - ave_time) * (num - ave_time);
            variance /= tim.size();

            ss << std::fixed << std::setprecision(3)                      //
               << std::left << std::setw(nameWidth) << r.first            //
               << std::setw(scoreWidth) << ave_time                       // 平均运行时间
               << std::setw(scoreWidth) << sqrt(variance)                 // 运行时间标准差
               << std::setw(scoreWidth) << tim.size()                     // 调用次数
               << std::setw(scoreWidth) << ave_time * double(tim.size())  // 平均时间*调用次数
               << std::endl;

            if (print_flag) std::cout << ss.str();
            ofs << ss.str(), ss.str("");
        }
        ss << ">>> ===== Printing run time end =====" << std::endl;
        if (print_flag) std::cout << ss.str();
        ofs << ss.str(), ss.str("");

        // 输出所有数据
        size_t max_length = 0;
        for (const auto& kv : records_) {
            ofs << std::setw(scoreWidth) << kv.first;
            max_length = std::max(max_length, kv.second.size());
        }
        ofs << std::endl;

        for (size_t i = 0; i < max_length; ++i) {
            for (const auto& kv : records_) {
                const auto& iter = kv.second;
                if (i < iter.size())
                    ofs << std::setw(scoreWidth) << iter[i];
                else
                    ofs << std::setw(scoreWidth) << "-";
                ofs << std::setw(scoreWidth);
            }
            ofs << std::endl;
        }
        ofs.close();
    }

private:
    std::map<std::string, std::vector<double>> records_;
};

/*



*/
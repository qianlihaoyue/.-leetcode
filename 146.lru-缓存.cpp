/*
 * @lc app=leetcode.cn id=146 lang=cpp
 *
 * [146] LRU 缓存
 */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>
#include <unordered_map>
#include <vector>

using namespace std;

// @lc code=start

// 将被访问的节点移到链表头部 !!! 缓存拼接
// https://blog.csdn.net/boiled_water123/article/details/103753598中的方法二
// 维护体素地图中的体素由新到旧进行排列
// dst.splice(dst.iter_pos,src,src.iter_pos)
// 将src的元素移动到dst的指定位置，高效的插入到dst并从src中删除
// dst.splice(dst.iter_pos,src) // 全部插入
// dst.splice(dst.iter_pos,src,src.iter_begin_pos,src.iter_end_pos)

class LRUCache {
public:
    LRUCache(int capacity) : max_capacity(capacity) {}

    int get(int key) {
        auto iter = umap.find(key);
        if (iter != umap.end()) {
            cache.splice(cache.begin(), cache, iter->second);  // 缓存拼接
            return iter->second->second;
        }
        return -1;
    }

    void put(int key, int value) {
        auto iter = umap.find(key);
        if (iter != umap.end()) {
            iter->second->second = value;                      // modify data
            cache.splice(cache.begin(), cache, iter->second);  // 缓存拼接
            return;
        }

        cache.push_front({key, value});
        umap[key] = cache.begin();
        if (cache.size() > max_capacity) {
            umap.erase(cache.back().first);  // 是back，而不是end
            cache.pop_back();
        }
    }

private:
    int max_capacity;
    std::list<std::pair<int, int>> cache;
    std::unordered_map<int, std::list<std::pair<int, int>>::iterator> umap;
};

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */
// @lc code=end

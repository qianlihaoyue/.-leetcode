/*
 * @lc app=leetcode.cn id=416 lang=cpp
 *
 * [416] 分割等和子集
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    bool canPartition(vector<int>& nums) {
        int sum = 0;
        for (auto& n : nums) sum += n;
        if (sum % 2) return false;  // 快速排除：如果和为奇数，则一定不行

        // 计算总和为sum/2的子集
        int half_sum = sum /= 2;
        vector<int> dp(half_sum + 1, 0);
        for (int i = 0; i < nums.size(); i++) {
            for (int j = half_sum; j >= nums[i]; j--) {
                dp[j] = max(dp[j], dp[j - nums[i]] + nums[i]);
            }
        }

        return (dp[half_sum] == half_sum);
    }
};
// @lc code=end

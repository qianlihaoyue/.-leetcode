/*
 * @lc app=leetcode.cn id=143 lang=cpp
 *
 * [143] 重排链表
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    void reorderList(ListNode *head) {
        if (!head) return;

        // vector大法
        // vector<ListNode *> vec;
        // while (head) {
        //     vec.push_back(head);
        //     head = head->next;
        // }
        // // 一种是直接操作vector,然后按顺序输出

        // // 一种是遍历vec,然后操作节点
        // int i = 0, j = vec.size() - 1;
        // while (i < j) {
        //     vec[i++]->next = vec[j];
        //     vec[j--]->next = vec[i];
        // }
        // // 处理末尾元素
        // vec[vec.size() / 2]->next = nullptr;
        // head = vec[0];

        // 双端队列法
        deque<ListNode *> que;
        ListNode *cur = head;

        while (cur->next) {
            que.push_back(cur->next);
            cur = cur->next;
        }

        cur = head;
        int count = 0;  // 计数，偶数去后面，奇数取前面
        ListNode *node;
        while (que.size()) {
            if (count % 2 == 0) {
                node = que.back();
                que.pop_back();
            } else {
                node = que.front();
                que.pop_front();
            }
            count++;
            cur->next = node;
            cur = cur->next;
        }
        cur->next = nullptr;  // 注意结尾
    }
};
// @lc code=end

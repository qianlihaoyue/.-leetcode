/*
 * @lc app=leetcode.cn id=210 lang=cpp
 *
 * [210] 课程表 II
 */

#include <algorithm>
#include <queue>
#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> findOrder(int numCourses, vector<vector<int>> &prerequisites) {
        std::vector<int> deg(numCourses, 0);
        std::vector<std::vector<int>> tab(numCourses);
        std::vector<int> res;

        for (auto &pair : prerequisites) {
            deg[pair[0]]++;  // 出度，最终需要==0才算没有阻碍
            tab[pair[1]].push_back(pair[0]);
        }

        std::queue<int> que;
        for (int i = 0; i < numCourses; i++)
            if (deg[i] == 0) que.push(i);

        // 一种是不需要计数的BFS，还有一种分层的，就再新建一个que，然后复制
        while (que.size()) {
            int idx = que.front();
            res.push_back(idx);
            que.pop();
            for (auto &t : tab[idx]) {
                deg[t]--;
                if (deg[t] == 0) que.push(t);
            }
        }
        for (auto &d : deg)
            if (d > 0) return vector<int>();

        return res;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=168 lang=cpp
 *
 * [168] Excel表列名称
 */

#include <algorithm>
#include <iostream>

#include <iterator>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    // 也就是一个26进制
    string convertToTitle(int columnNumber) {
        columnNumber--;  // 0-25映射  1-26 --> 0-25
        string result;
        while (columnNumber >= 0) {
            result.push_back(columnNumber % 26 + 'A');
            columnNumber = columnNumber / 26 - 1;
        }
        std::reverse(result.begin(), result.end());
        return result;
    }
};
// @lc code=end

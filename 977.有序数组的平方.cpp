/*
 * @lc app=leetcode.cn id=977 lang=cpp
 *
 * [977] 有序数组的平方
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> sortedSquares(vector<int>& nums) {
        // 从两边开始
        vector<int> result(nums.size());
        int l = 0, r = nums.size() - 1;

        // 确实也需要倒着填
        for (int i = nums.size() - 1; i >= 0; i--) {
            if (nums[l] < 0) nums[l] = -nums[l];
            if (nums[l] > nums[r]) {
                result[i] = nums[l] * nums[l];
                l++;
            } else {
                result[i] = nums[r] * nums[r];
                r--;
            }
        }
        return result;
    }
};
// @lc code=end

int main() {
    Solution solute;

    vector<int> nums = {-5, -3, -2, -1};
    nums = solute.sortedSquares(nums);
    for (auto& n : nums) std::cout << n << " ";

    return 0;
}

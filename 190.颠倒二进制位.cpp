/*
 * @lc app=leetcode.cn id=190 lang=cpp
 *
 * [190] 颠倒二进制位
 */

#include <algorithm>
#include <bitset>
#include <cstdint>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    uint32_t reverseBits(uint32_t n) {
        // 一进一出
        uint32_t res = 0x00;
        for (int i = 0; i < 32; i++) {
            res <<= 1;
            res |= (n & 0x01);
            n >>= 1;
        }
        return res;
    }
};
// @lc code=end

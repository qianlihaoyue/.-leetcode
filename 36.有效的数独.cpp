/*
 * @lc app=leetcode.cn id=36 lang=cpp
 *
 * [36] 有效的数独
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <map>
using namespace std;

// @lc code=start
class Solution {
public:
    bool isValidSudoku(vector<vector<char>>& board) {
        std::vector<bool> flg(9, false);
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j] != '.') {
                    if (flg[board[i][j] - '1']) {
                        // std::cout << "r "<< board[i][j] - '0'<<std::endl;
                        return false;
                    }
                    flg[board[i][j] - '1'] = true;
                }
            }
            std::fill(flg.begin(), flg.end(), false);
        }

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[j][i] != '.') {
                    if (flg[board[j][i] - '1']) {
                        //  std::cout << "c "<< board[j][i] - '0'<<std::endl;
                        return false;
                    }
                    flg[board[j][i] - '1'] = true;
                }
            }
            std::fill(flg.begin(), flg.end(), false);
        }

        // 第一个代表第几个大格子
        for (int i = 0; i < 9; i++) {
            // 第二个代表小格子索引
            for (int j = 0; j < 9; j++) {
                int idx_i = (i / 3) * 3 + j / 3, idx_j = (i % 3) * 3 + (j % 3);
                if (board[idx_i][idx_j] != '.') {
                    if (flg[board[idx_i][idx_j] - '1']) {
                        //  std::cout << "b "<< board[idx_i][idx_j] - '0'<<std::endl;
                        return false;
                    }
                    flg[board[idx_i][idx_j] - '1'] = true;
                }
            }
            std::fill(flg.begin(), flg.end(), false);
        }

        return true;
    }
};
// @lc code=end

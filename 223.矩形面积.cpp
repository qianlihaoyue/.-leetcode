/*
 * @lc app=leetcode.cn id=223 lang=cpp
 *
 * [223] 矩形面积
 */

// @lc code=start
#include <algorithm>
#include <cmath>
class Solution {
public:
    int computeArea(int ax1, int ay1, int ax2, int ay2, int bx1, int by1, int bx2, int by2) {
        int areaA = (ax2 - ax1) * (ay2 - ay1);
        int areaB = (bx2 - bx1) * (by2 - by1);

        int cx1 = std::max(ax1, bx1), cy1 = std::max(ay1, by1);
        int cx2 = std::min(ax2, bx2), cy2 = std::min(ay2, by2);

        if (cx2 < cx1 || cy2 < cy1) return areaA + areaB;
        int areaC = (cx2 - cx1) * (cy2 - cy1);
        return areaA + areaB - areaC;
    }
};
// @lc code=end

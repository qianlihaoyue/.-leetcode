/*
 * @lc app=leetcode.cn id=232 lang=cpp
 *
 * [232] 用栈实现队列
 */

#include <stack>
#include <iostream>
using namespace std;

// @lc code=start

class MyQueue {
private:
    std::stack<int> ist, ost;

public:
    MyQueue() {}

    void push(int x) { ist.push(x); }

    // 优先把ost输出完，再从ist批量导入
    int pop() {
        int x = peek();  // 此时ost肯定有元素
        ost.pop();
        return x;
    }

    int peek() {
        if (ost.size()) return ost.top();  // 如果输出不为空
        if (ist.empty()) return -1;        // 两个都为空
        // ist 转 ost
        while (ist.size()) {
            ost.push(ist.top()), ist.pop();
        }
        return ost.top();
    }

    bool empty() { return ist.empty() && ost.empty(); }
};

// class MyQueue {
// public:
//     // 使用两个stack 输入输出栈 倒腾
//     MyQueue() {}

//     void push(int x) {
//         o2i();
//         ist.push(x);
//     }

//     int pop() {
//         i2o();
//         int x = ost.top();
//         ost.pop();
//         return x;
//     }

//     int peek() {
//         i2o();
//         return ost.top();
//     }

//     bool empty() { return ist.empty() && ost.empty(); }

// private:
//     std::stack<int> ist, ost;
//     // ist 导入ost
//     void i2o() {
//         while (ist.size()) {
//             ost.push(ist.top());
//             ist.pop();
//         }
//     }

//     void o2i() {
//         while (ost.size()) {
//             ist.push(ost.top());
//             ost.pop();
//         }
//     }
// };

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue* obj = new MyQueue();
 * obj->push(x);
 * int param_2 = obj->pop();
 * int param_3 = obj->peek();
 * bool param_4 = obj->empty();
 */
// @lc code=end

int main() {
    // Solution solute;
    // cout << solute.reverseStr(s, 2);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }
    MyQueue* obj = new MyQueue();
    obj->push(1);
    cout << obj->pop() << endl;
    cout << obj->peek() << endl;
    cout << obj->empty() << endl;
    // cout << result << endl;

    return 0;
}

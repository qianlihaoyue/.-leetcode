/*
 * @lc app=leetcode.cn id=18 lang=cpp
 *
 * [18] 四数之和
 */

#include <algorithm>
#include <vector>
#include <iostream>
#include <map>
#include <unordered_map>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        vector<vector<int>> result;
        int n = nums.size();
        for (int i = 0; i < n - 3; i++) {
            if (i && nums[i] == nums[i - 1]) continue;  // 重复

            if (nums[i] + nums[i + 1] > target - nums[i + 2] - nums[i + 3]) break;  // 加速

            for (int j = i + 1; j < n - 2; j++) {
                if (j > i + 1 && nums[j] == nums[j - 1]) continue;  // 去重

                if (nums[i] + nums[j] > target - nums[j + 1] - nums[j + 2]) break;  // 加速

                int l = j + 1, r = n - 1;
                while (l < r) {
                    long long tmp = nums[i] + nums[j] + nums[l] + nums[r];
                    if (tmp == target) {
                        result.push_back({nums[i], nums[j], nums[l], nums[r]});
                        // 去重
                        while (l < r && nums[l] == nums[l + 1]) ++l;
                        while (l < r && nums[r] == nums[r - 1]) --r;
                        l++, r--;  // 还需要另外分别向内移动
                    } else if (tmp < target)
                        l++;
                    else
                        r--;
                }
            }
        }

        return result;
    }
};
// @lc code=end

int main() {
    Solution solute;
    vector<int> nums = {1, 0, -1, 0, -2, 2};
    auto result = solute.fourSum(nums, 0);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

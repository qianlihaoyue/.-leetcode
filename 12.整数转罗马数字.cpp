/*
 * @lc app=leetcode.cn id=12 lang=cpp
 *
 * [12] 整数转罗马数字
 */

#include <algorithm>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    string intToRoman(int num) {
        std::vector<int> val = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        std::vector<string> str = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        string res;

        for (int i = 0; i < val.size(); i++) {
            while (num >= val[i]) {
                num -= val[i];
                res += str[i];
            }
        }

        // while (num >= 1000) {
        //     res += 'M';
        //     num -= 1000;
        // }

        // if (num >= 900) {
        //     res += "CM";
        //     num -= 900;
        // }
        // if (num >= 500) {
        //     res += 'D';
        //     num -= 500;
        // }
        // if (num >= 400) {
        //     res += "CD";
        //     num -= 400;
        // }
        // while (num >= 100) {
        //     res += "C";
        //     num -= 100;
        // }

        // if (num >= 90) {
        //     res += "XC";
        //     num -= 90;
        // }
        // if (num >= 50) {
        //     res += "L";
        //     num -= 50;
        // }
        // if (num >= 40) {
        //     res += "XL";
        //     num -= 40;
        // }
        // while (num >= 10) {
        //     res += "X";
        //     num -= 10;
        // }

        // if (num >= 9) {
        //     res += "IX";
        //     num -= 9;
        // }
        // if (num >= 5) {
        //     res += "V";
        //     num -= 5;
        // }
        // if (num >= 4) {
        //     res += "IV";
        //     num -= 4;
        // }
        // while (num >= 1) {
        //     res += "I";
        //     num -= 1;
        // }
        return res;
    }
};
// @lc code=end

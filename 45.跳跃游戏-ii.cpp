/*
 * @lc app=leetcode.cn id=45 lang=cpp
 *
 * [45] 跳跃游戏 II
 */

#include <algorithm>
#include <cstdint>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int jump(vector<int>& nums) {
        // if (nums.size() == 1) return 0;

        // std::vector<int> dp(nums.size(), INT16_MAX);
        // dp[0] = 0;
        // for (int i = 0; i < nums.size(); i++) {
        //     // 前向
        //     for (int j = 1; j <= nums[i]; j++) {
        //         if (i + j < nums.size()) dp[i + j] = min(dp[i + j], dp[i] + 1);
        //     }
        // }

        // return dp[nums.size() - 1];

        // 标准dp问题，每一个值为该位置最小跳跃次数
        // std::vector<int> dp(nums.size(), INT32_MAX);
        // dp[0] = 0;
        // for (int i = 0; i < nums.size(); i++) {
        //     // 提前加速
        //     if (i + nums[i] >= nums.size() - 1) return dp[i] + 1;
        //     // 往前更新
        //     for (int j = 1; j <= nums[i]; j++) {
        //         if (i + j < nums.size()) dp[i + j] = std::min(dp[i + j], dp[i] + 1);
        //     }
        // }
        // return dp[nums.size() - 1];

        std::vector<int> dp(nums.size());
        int bestStart = 0;
        for (int i = 1; i < nums.size(); i++) {
            while (i - bestStart > nums[bestStart]) bestStart++;
            dp[i] = dp[bestStart] + 1;
        }
        return dp[nums.size() - 1];
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=191 lang=cpp
 *
 * [191] 位1的个数
 */

#include <algorithm>
#include <bitset>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    int hammingWeight(uint32_t n) {
        // 可以逐位计数
        // int cnt = 0;
        // while (n) {
        //     cnt += n & 0x01;
        //     n >>= 1;
        // }
        // return cnt;

        // 新特性
        return bitset<32>(n).count();
    }
};
// @lc code=end

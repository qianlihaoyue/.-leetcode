/*
 * @lc app=leetcode.cn id=138 lang=cpp
 *
 * [138] 随机链表的复制
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;
class Node {
public:
    int val;
    Node* next;
    Node* random;

    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};
// @lc code=start
/*
// Definition for a Node.
class Node {
public:
    int val;
    Node* next;
    Node* random;

    Node(int _val) {
        val = _val;
        next = NULL;
        random = NULL;
    }
};
*/

class Solution {
public:
    Node* copyRandomList(Node* head) {
        map<Node*, Node*> mymap;

        Node* cur = head;
        Node* dummy = new Node(0);
        Node* pre = dummy;

        while (cur) {
            Node* ncur;

            auto findc = mymap.find(cur);
            if (findc != mymap.end())
                ncur = findc->second;
            else {
                ncur = new Node(cur->val);
                mymap[cur] = ncur;
            }

            if (cur->random) {
                auto findr = mymap.find(cur->random);
                if (findr != mymap.end())
                    ncur->random = findr->second;
                else {
                    ncur->random = new Node(cur->random->val);
                    mymap[cur->random] = ncur->random;
                }
            }

            pre->next = ncur;
            pre = ncur;
            cur = cur->next;
        }
        return dummy->next;
    }
};
// @lc code=end

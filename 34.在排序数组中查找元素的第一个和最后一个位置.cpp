/*
 * @lc app=leetcode.cn id=34 lang=cpp
 *
 * [34] 在排序数组中查找元素的第一个和最后一个位置
 */

#include <algorithm>
#include <iterator>
#include <vector>
using namespace std;

// @lc code=start

// 都采用闭区间  r=size-1   l<=r
class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        // lower_bound 查找第一个 >=num 的数字
        // upper_bound 查找第一个 > num 的数字

        auto start = std::lower_bound(nums.begin(), nums.end(), target);

        // auto end = std::lower_bound(start, nums.end(), target + 1);
        // if (start != nums.end() && *start == target) {
        //     return {(int)(start - nums.begin()), (int)(end - nums.begin()) - 1};
        // }

        if (start != nums.end() && *start == target) {
            auto end = std::upper_bound(start, nums.end(), target);
            return {(int)(start - nums.begin()), (int)distance(nums.begin(), end) - 1};
        }

        return {-1, -1};
    }
};
// @lc code=end

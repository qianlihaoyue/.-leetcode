/*
 * @lc app=leetcode.cn id=169 lang=cpp
 *
 * [169] 多数元素
 */

// @lc code=start

class Solution {
public:
    int majorityElement(vector<int>& nums) {
        int vote, cnt = 0;
        for (auto& i : nums) {
            if (cnt == 0) vote = i;
            if (i == vote)
                cnt++;
            else
                cnt--;
        }
        return vote;
    }
};
// @lc code=end

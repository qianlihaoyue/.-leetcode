/*
 * @lc app=leetcode.cn id=14 lang=cpp
 *
 * [14] 最长公共前缀
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    string longestCommonPrefix(vector<string> &strs) {
        auto min_it = *std::min_element(strs.begin(), strs.end(), [](const string &a, const string &b) { return a.size() < b.size(); });
        string result;
        bool end_flg = false;
        for (int i = 0; i < min_it.size(); i++) {
            auto chr = strs[0][i];
            for (int j = 1; j < strs.size(); j++) {
                if (strs[j][i] != chr) {
                    end_flg = true;
                    break;
                }
            }
            if (end_flg) break;
            result += chr;
        }
        return result;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=494 lang=cpp
 *
 * [494] 目标和
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    // 倒序
    // int dfs(vector<int>& nums, int target, int idx) {
    //     if (idx == 0) {
    //         if (target == nums[0])
    //             return 1;
    //         else
    //             return 0;
    //     }

    //     return dfs(nums, target + nums[idx], idx - 1) + dfs(nums, target - nums[idx], idx - 1);
    // }

    int findTargetSumWays(vector<int>& nums, int target) {
        // 还是分成两组
        // 正 A = 0.5(sum+target) 负 B = 0.5(sum-target)

        int sum = 0;
        for (auto& n : nums) sum += n;
        if ((sum - target) % 2) return 0;
        if (sum < target) return 0;
        int PB = 0.5 * (sum - target);

        // cout << "sum: " << sum << " PB: " << PB << endl;
        vector<int> dp(PB + 1, 0);
        dp[0] = 1;
        for (int i = 0; i < nums.size(); i++) {
            for (int j = PB; j >= nums[i]; j--) {
                dp[j] += dp[j - nums[i]];
            }

            // for (int k = 0; k < PB + 1; k++) cout << dp[k] << " ";
            // cout << endl;
        }

        return dp[PB];
    }
};
// @lc code=end
int main() {
    vector<int> nums = {2, 107, 109, 113, 127, 131, 137, 3, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 47, 53};
    Solution solu;
    int res = solu.findTargetSumWays(nums, 1000);
    cout << "res: " << res << endl;
}

/*
 * @lc app=leetcode.cn id=15 lang=cpp
 *
 * [15] 三数之和
 */

#include <algorithm>
#include <vector>
#include <iostream>
#include <map>
#include <unordered_map>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> threeSum(vector<int>& nums) {
        vector<vector<int>> result;
        // 先排序
        std::sort(nums.begin(), nums.end());
        // 三指针和暴力计算也没什么区别
        for (int i = 0; i < nums.size() - 2; i++) {
            // 优化加速
            int tmp = nums[i] + nums[i + 1] + nums[i + 2];
            if (nums[i] > 0 || tmp > 0) return result;
            // 去重
            if (i && nums[i] == nums[i - 1]) continue;
            // 双指针
            int l = i + 1, r = nums.size() - 1;
            while (l < r) {
                int tmp = nums[i] + nums[l] + nums[r];
                if (tmp == 0) {
                    result.push_back({nums[i], nums[l], nums[r]});
                    l++;
                    r--;
                    // 去重
                    while (l < r && nums[l] == nums[l - 1]) l++;
                    while (l < r && nums[r] == nums[r + 1]) r--;
                } else if (tmp < 0)
                    l++;
                else if (tmp > 0)
                    r--;
            }
        }

        return result;
    }
};
// @lc code=end

int main() {
    Solution solute;
    vector<int> nums = {-1, 0, 1, 2, -1, -4};
    auto result = solute.threeSum(nums);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

/*
 * @lc app=leetcode.cn id=1047 lang=cpp
 *
 * [1047] 删除字符串中的所有相邻重复项
 */

#include <bits/stdc++.h>
#include <stack>
#include <string>
using namespace std;

// @lc code=start
class Solution {
public:
    string removeDuplicates(string s) {
        // 直接使用字符串
        string res;
        for (auto &c : s) {
            if (res.size() && c == res.back())
                res.pop_back();
            else
                res.push_back(c);
        }
        return res;

        // 使用数组模拟,这个删除元素速度更快
        // string res;
        // res.resize(s.length());
        // int size = 0;
        // for (auto &c : s) {
        //     if (size && c == res[size - 1])
        //         size--;
        //     else
        //         res[size++] = c;
        // }
        // res.resize(size);  // 截断
        // return res;

        // 使用标准栈
        // stack<char> st;
        // for (auto &c : s) {
        //     if (st.size() && c == st.top())
        //         st.pop();
        //     else
        //         st.emplace(c);
        // }
        // // 栈的输出
        // string ans;
        // while (st.size()) ans = st.top() + ans, st.pop();
        // return ans;
    }
};
// @lc code=end

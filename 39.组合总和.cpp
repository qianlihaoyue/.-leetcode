/*
 * @lc app=leetcode.cn id=39 lang=cpp
 *
 * [39] 组合总和
 */

#include <algorithm>

using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    int sum = 0;
    // 31% 74%
    // 通过sort剪枝
    void dfs(const vector<int>& nums, const int target, int i) {
        if (sum == target) {
            ans.push_back(path);
            return;
        }
        // 需要先排序
        if (sum + nums[i] > target) return;
        // 可以重复
        for (int j = i; j < nums.size(); j++) {
            // 剪枝
            if (sum + nums[j] <= target) {
                path.push_back(nums[j]), sum += nums[j];
                dfs(nums, target, j);
                path.pop_back(), sum -= nums[j];
            }
        }
    }

    vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
        // 排序前后好像没什么区别
        sort(candidates.begin(), candidates.end());
        dfs(candidates, target, 0);
        return ans;
    }
};
// @lc code=end

int main(int argc, char* argv[]) {
    Solution solu;

    vector<int> candidates = {2, 3, 6, 7, 4, 2, 8, 9, 10, 11, 3, 4};

    // TIMER_CREATE(tim_cnt);
    auto res = solu.combinationSum(candidates, 20);
    // tim_cnt.toc_cout();
    // for (auto &num : res) {
    //     for (auto &n : num) cout << n << " ";
    //     cout << endl;
    // }

    return 0;
}

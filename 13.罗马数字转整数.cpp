/*
 * @lc app=leetcode.cn id=13 lang=cpp
 *
 * [13] 罗马数字转整数
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    inline int char2num(char c) {
        switch (c) {
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
            default:
                return 0;
        }
    }
    int romanToInt(string s) {
        int last = char2num(s[0]);
        if (s.size() == 1) return last;

        int nega_flg = 0;
        int sum = 0;
        for (int i = 1; i < s.size(); i++) {
            int cur = char2num(s[i]);
            if (last < cur)
                sum -= last;
            else
                sum += last;
            last = cur;
        }
        sum += char2num(s[s.size() - 1]);
        return sum;
    }
};
// @lc code=end

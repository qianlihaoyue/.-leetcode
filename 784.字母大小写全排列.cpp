/*
 * @lc app=leetcode.cn id=784 lang=cpp
 *
 * [784] 字母大小写全排列
 */

#include <algorithm>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<string> ans;

    inline void change(char& c) {
        // if (c >= 'a')
        //     c -= 32;
        // else
        //     c += 32;
        // c ^= 32;
        c ^= 0x20;
        // c ^= 0b100000;
    }
    void dfs(string& s, int i) {
        // 末尾有数字
        while (i < s.size() && s[i] >= '0' && s[i] <= '9') i++;

        if (i == s.size()) {
            ans.push_back(s);
            return;
        }

        for (int j = i; j < s.size(); j++) {
            if (s[j] >= '0' && s[j] <= '9') continue;
            dfs(s, j + 1);  // 因为这个的缘故？
            change(s[j]);
            dfs(s, j + 1);
            change(s[j]);
            break;  // 找到一个就跳出，
        }
    }

    vector<string> letterCasePermutation(string s) {
        dfs(s, 0);
        return ans;
    }
};
// @lc code=end

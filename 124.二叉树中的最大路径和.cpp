/*
 * @lc app=leetcode.cn id=124 lang=cpp
 *
 * [124] 二叉树中的最大路径和
 */

#include <algorithm>
#include <cstdint>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
#include <unordered_map>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int max_val = INT32_MIN;

    int maxValue(TreeNode *root) {
        if (root) {
            int max_l = maxValue(root->left) + root->val;
            int max_r = maxValue(root->right) + root->val;
            max_val = max({max_val, max_l, max_r, max_l + max_r - root->val, root->val});
            return max({max_l, max_r, 0});
        }
        return 0;
    }

    int maxPathSum(TreeNode *root) {
        maxValue(root);
        return max_val;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=350 lang=cpp
 *
 * [350] 两个数组的交集 II
 */

#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        vector<int> result;
        int map[1001] = {};              // 简化版hash，初始化为0
        for (auto& n : nums1) map[n]++;  // 因为需要取较小次数
        for (auto& n : nums2) {
            if (map[n]) {
                map[n]--;
                result.push_back(n);
            }
        }
        return result;
    }
};
// @lc code=end

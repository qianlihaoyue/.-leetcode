/*
 * @lc app=leetcode.cn id=130 lang=cpp
 *
 * [130] 被围绕的区域
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    void dfs(vector<vector<char>>& grid, char mark, int i, int j) {  // y,x
        if (grid[i][j] == 'X' || grid[i][j] == mark) return;
        grid[i][j] = mark;
        if (i - 1 >= 0) dfs(grid, mark, i - 1, j);
        if (j - 1 >= 0) dfs(grid, mark, i, j - 1);
        if (i + 1 < grid.size()) dfs(grid, mark, i + 1, j);
        if (j + 1 < grid[0].size()) dfs(grid, mark, i, j + 1);
        return;
    }

    // 使用visit数组，第一遍，在边上进行dfs
    // 第二遍遍历中间，进行修改
    // 实际上也可以使用 Y 代替 visit
    void solve(vector<vector<char>>& board) {
        // 先标记外圈
        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board[0].size(); j++) {
                if ((i == 0 || i == board.size() - 1 || j == 0 || j == board[0].size() - 1)) {
                    if (board[i][j] == 'O') dfs(board, 'Y', i, j);
                }
            }
        }

        for (int i = 0; i < board.size(); i++) {
            for (int j = 0; j < board[0].size(); j++) {
                if (board[i][j] == 'O')
                    board[i][j] = 'X';
                else if (board[i][j] == 'Y')
                    board[i][j] = 'O';
            }
        }
    }
};
// @lc code=end

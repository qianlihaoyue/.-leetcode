/*
 * @lc app=leetcode.cn id=239 lang=cpp
 *
 * [239] 滑动窗口最大值
 */

#include <bits/stdc++.h>
#include <deque>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> maxSlidingWindow(vector<int>& nums, int k) {
        // // 使用 multiset+排序  O(nlog(n))
        // vector<int> ans;
        // std::multiset<int, std::greater<int>> qu;
        // for (int i = 0; i < k; i++) qu.insert(nums[i]);
        // ans.push_back(*qu.begin());
        // for (int i = 0; i < nums.size() - k; i++) {
        //     qu.erase(qu.find(nums[i]));
        //     qu.insert(nums[i + k]);
        //     ans.push_back(*qu.begin());
        // }
        // return ans;

        // 单调队列
        vector<int> ans;
        deque<int> qu;
        for (int i = 0; i < nums.size(); i++) {
            // 当输入一个值前，删掉 队列中 更小的元素
            while (qu.size() && nums[qu.back()] <= nums[i]) qu.pop_back();
            qu.push_back(i);
            // 出
            if (i - qu.front() > k - 1) qu.pop_front();
            // 记录答案,队首是最大值
            if (i >= k - 1) ans.push_back(nums[qu.front()]);
        }
        return ans;
    }
};
// @lc code=end

int main() {
    Solution solute;

    vector<int> nums = {9, 10, 9, -7, -4, -8, 2, -6};
    auto result = solute.maxSlidingWindow(nums, 5);

    // vector<int> nums = {1, 3, -1, -3, 5, 3, 6, 7};
    // auto result = solute.maxSlidingWindow(nums, 3);

    // vector<int> nums = {1};
    // auto result = solute.maxSlidingWindow(nums, 1);

    // vector<int> nums = {-6, -10, -7, -1, -9, 9, -8, -4, 10, -5, 2, 9, 0, -7, 7, 4, -2, -10, 8, 7};
    // auto result = solute.maxSlidingWindow(nums, 7);

    for (auto& n : result) cout << n << " ";

    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

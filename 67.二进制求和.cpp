/*
 * @lc app=leetcode.cn id=67 lang=cpp
 *
 * [67] 二进制求和
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    // Tip:考虑到测试用例中有超大数，所以采用字符串计算
    // Tip:没有考虑负数
    // int bin2dec(string &str) {
    //     int sum = 0;
    //     int base = 1;
    //     for (int i = str.size() - 1; i >= 0; i--) {
    //         sum += (str[i] - '0') * base;
    //         base *= 2;
    //     }
    //     return sum;
    // }

    // string dec2bin(int num) {
    //     string result;
    //     while (num > 1) {
    //         result.push_back(num % 2 + '0');
    //         num /= 2;
    //     }
    //     result.push_back(num + '0');
    //     std::reverse(result.begin(), result.end());
    //     return result;
    // }

    string addBinary(string a, string b) {
        string result;

        // 字符串级别操作
        int val = 0;
        for (int i = a.size() - 1, j = b.size() - 1; i >= 0 || j >= 0; i--, j--) {
            int sum = val;
            if (i >= 0) sum += (a[i] - '0');
            if (j >= 0) sum += (b[j] - '0');
            if (sum > 1) {
                val = 1;
                sum -= 2;
            } else
                val = 0;
            result.push_back(sum + '0');
        }
        if (val) result.push_back(val + '0');
        std::reverse(result.begin(), result.end());
        return result;

        // 转十进制再操作
        // return dec2bin(bin2dec(a) + bin2dec(b));
    }
};
// @lc code=end

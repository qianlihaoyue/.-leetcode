/*
 * @lc app=leetcode.cn id=264 lang=cpp
 *
 * [264] 丑数 II
 */

#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int nthUglyNumber(int n) {
        // int n2 = 0, n3 = 0, n5 = 0;
        // int dp[n];
        // dp[0] = 1;
        // for (int i = 1; i < n; i++) {
        //     dp[i] = std::min(2 * dp[n2], std::min(3 * dp[n3], 5 * dp[n5]));
        //     if (dp[i] == 2 * dp[n2]) n2++;
        //     if (dp[i] == 3 * dp[n3]) n3++;
        //     if (dp[i] == 5 * dp[n5]) n5++;
        // }
        // return dp[n - 1];

        if (n <= dp.size()) {
            return dp[n - 1];
        }
        dp.resize(n);
        int n2 = 0, n3 = 0, n5 = 0;
        for (int i = 1; i < n; i++) {
            int num_2 = 2 * dp[n2], num_3 = 3 * dp[n3], num_5 = 5 * dp[n5];
            dp[i] = std::min(num_2, std::min(num_3, num_5));
            // 同时兼顾去重
            if (dp[i] == num_2) n2++;
            if (dp[i] == num_3) n3++;
            if (dp[i] == num_5) n5++;
        }
        return dp[n - 1];
    }

protected:
    std::vector<int> dp = {1};
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=206 lang=cpp
 *
 * [206] 反转链表
 */

#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* reverseList(ListNode* head) {
        // 原地翻转算法，双指针法  时间复杂度: O(n) 空间复杂度: O(1)

        // 双指针法
        ListNode* cur = head;
        ListNode* pre = nullptr;
        while (cur) {
            ListNode* tmp = cur->next;  // 备份cur->next
            cur->next = pre;            // 然后立即使用
            pre = cur, cur = tmp;       // 继续接龙，下一个是上一个的结尾，cur 最终 = cur->next
        }
        return pre;

        // 通过数组进行操作，需要特别处理头尾，坏处是占用O(n)空间
    }
};
// @lc code=end

int main() {
    Solution solute;
    // cout << solute.reverseStr(s, 2);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

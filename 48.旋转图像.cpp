/*
 * @lc app=leetcode.cn id=48 lang=cpp
 *
 * [48] 旋转图像
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

// @lc code=start
class Solution {
public:
    void rotate(vector<vector<int>>& matrix) {
        // 规律法
        int n = matrix.size();
        for (int row = 0; row < n / 2; row++) {
            for (int col = row; col < n - row - 1; col++) {
                int tmp = matrix[row][col];
                matrix[row][col] = matrix[n - 1 - col][row];
                matrix[n - 1 - col][row] = matrix[n - 1 - row][n - 1 - col];
                matrix[n - 1 - row][n - 1 - col] = matrix[col][n - 1 - row];
                matrix[col][n - 1 - row] = tmp;
                // r=0, c=1  2,0  3,2  1,3     n-1=3
                // r=1, c=1  2,1  2,2  1,2
            }
        }
        // 对角线翻转，再逆序
        
    }
};
// @lc code=end

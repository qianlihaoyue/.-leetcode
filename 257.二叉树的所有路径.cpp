/*
 * @lc app=leetcode.cn id=257 lang=cpp
 *
 * [257] 二叉树的所有路径
 */

#include <algorithm>
#include <queue>
#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // vector<vector<int>> result;
    // // 此处并不是指针进入，利用复制的特性
    // void recurpre(TreeNode *root, vector<int> group) {
    //     group.push_back(root->val);
    //     if (!root->left && !root->right) result.push_back(group);
    //     if (root->left) recurpre(root->left, group);
    //     if (root->right) recurpre(root->right, group);
    // }

    // vector<string> binaryTreePaths(TreeNode *root) {
    //     vector<int> group;
    //     recurpre(root, group);

    //     // 转字符串
    //     vector<string> result_str;
    //     for (auto &vec : result) {
    //         string s;
    //         for (int i = 0; i < vec.size() - 1; i++) s = s + to_string(vec[i]) + "->";
    //         s += to_string(vec[vec.size() - 1]);
    //         result_str.push_back(s);
    //     }

    //     return result_str;
    // }

    vector<string> result;
    // 此处并不是指针进入，利用复制的特性
    void recurpre(TreeNode *root, string group) {
        group += std::to_string(root->val);
        if (!root->left && !root->right) result.push_back(group);
        group += "->";
        if (root->left) recurpre(root->left, group);
        if (root->right) recurpre(root->right, group);
    }

    vector<string> binaryTreePaths(TreeNode *root) {
        string group;
        recurpre(root, group);
        return result;
    }
};
// @lc code=end

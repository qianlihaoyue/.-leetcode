/*
 * @lc app=leetcode.cn id=684 lang=cpp
 *
 * [684] 冗余连接
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    int Find(const vector<int>& parent, int idx) {
        if (parent[idx] == idx) return idx;
        return Find(parent, parent[idx]);
    }

    // 17% 86%
    // int Find(vector<int>& parent, int index) {
    //     if (parent[index] != index) {
    //         parent[index] = Find(parent, parent[index]);
    //     }
    //     return parent[index];
    // }

    void Union(vector<int>& parent, int idx1, int idx2) {
        // 直接祖宗相连
        parent[Find(parent, idx1)] = Find(parent, idx2);
    }

    vector<int> findRedundantConnection(vector<vector<int>>& edges) {
        // init，各自为营
        vector<int> parent(edges.size() + 1);
        for (int i = 1; i <= edges.size(); ++i) parent[i] = i;

        for (auto& edge : edges) {
            int node1 = edge[0], node2 = edge[1];
            // 未联通
            if (Find(parent, node1) != Find(parent, node2))
                Union(parent, node1, node2);
            else
                return edge;
        }
        return vector<int>{};
    }
};
// @lc code=end

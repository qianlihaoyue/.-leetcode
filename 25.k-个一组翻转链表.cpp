/*
 * @lc app=leetcode.cn id=25 lang=cpp
 *
 * [25] K 个一组翻转链表
 */

#include <algorithm>
#include <functional>
#include <queue>
#include <string>
#include <unordered_set>
#include <vector>
#include <iostream>
#include <map>
#include <set>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    int getLen(ListNode* head) {
        int cnt = 0;
        while (head) {
            cnt++;
            head = head->next;
        }
        return cnt;
    }
    ListNode* reverseKGroup(ListNode* head, int k) {
        ///////////////////// 数组法
        // std::vector<ListNode*> vec;
        // ListNode* cur = head;
        // while (cur) {
        //     vec.push_back(cur);
        //     cur = cur->next;
        // }
        // if (vec.size() < k) return head;
        // // 翻转
        // // std::reverse 左闭右开
        // for (int i = 0; i <= vec.size() - k; i += k) std::reverse(vec.begin() + i, vec.begin() + i + k);
        // // 连接
        // for (int i = 0; i < vec.size() - 1; i++) vec[i]->next = vec[i + 1];
        // vec[vec.size() - 1]->next = nullptr;  // 连接之后，最后一个要为nullptr
        // return vec[0];

        ////////////// 批量翻转法

        int i = k;
        ListNode* last = head;
        // 判断该组是否有k个数，如果没有直接返回，如果有则记住下一组的头结点，并准备对这组进行翻转
        while (i--) {
            if (!last) return head;
            last = last->next;
        }
        ListNode* cur = head->next;
        // 用该组头结点连接翻转后的下一组结点
        head->next = reverseKGroup(last, k);
        // 翻转该组的k个结点
        while (--k) {
            ListNode* tmp = cur->next;
            cur->next = head, head = cur, cur = tmp;
        }
        // 把翻转后的该组结点返回，返回后刚好被上一组结点连上
        return head;
    }
};
// @lc code=end

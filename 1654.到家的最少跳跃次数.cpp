/*
 * @lc app=leetcode.cn id=1654 lang=cpp
 *
 * [1654] 到家的最少跳跃次数
 */

#include <iostream>
#include <vector>
#include <queue>
#include <unordered_set>

using namespace std;

// @lc code=start
class Solution {
public:
    int minimumJumps(vector<int>& forbidden, int a, int b, int x) {
        unordered_set<int> forb(forbidden.begin(), forbidden.end());
        queue<pair<int, bool>> q;  // Pair: position and a flag indicating if the last jump was backwards
        unordered_set<int> visited_forward, visited_back;

        q.push({0, false});         // Start from position 0, and the last jump wasn't backwards
        visited_forward.insert(0);  // Mark the start position as visited
        visited_back.insert(0);

        int jumps = 0;
        int pos;
        bool jumpedBack;
        while (!q.empty()) {
            // 由于需要进行计数，所以不是只使用了一个队列就完了，而是一层一层处理
            int size = q.size();
            for (int i = 0; i < size; ++i) {
                // 从队列取一个元素
                auto tmp = q.front();
                pos = tmp.first, jumpedBack = tmp.second;
                q.pop();

                // 终止条件
                if (pos == x) return jumps;

                // 往前跳
                int nextPos = pos + a;
                // 下一个位置，不在禁止区，且未到访过，上限需要整挺大
                if (nextPos < 10000 && forb.find(nextPos) == forb.end() && visited_forward.find(nextPos) == visited_forward.end()) {
                    q.push({nextPos, false});
                    visited_forward.insert(nextPos);
                }

                // 往后跳   , 不在禁止区，且未到访过，不会低于下限0，上次没往后跳
                nextPos = pos - b;
                if (!jumpedBack && nextPos > 0 && forb.find(nextPos) == forb.end() && visited_back.find(nextPos) == visited_back.end()) {
                    q.push({nextPos, true});
                    visited_back.insert(nextPos);  // Encode the state with position and direction
                }
            }
            jumps++;
        }

        return -1;
    }
};
// @lc code=end

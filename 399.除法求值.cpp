/*
 * @lc app=leetcode.cn id=399 lang=cpp
 *
 * [399] 除法求值
 */

#include <bits/stdc++.h>
#include <unordered_map>
using namespace std;

// @lc code=start
class Solution {
public:
    // 并查集

    unordered_map<string, string> parent;

    string Find(string idx) {
        if (parent[idx] != idx) parent[idx] = Find(parent[idx]);
        return parent[idx];
    }

    void Union(string idx1, string idx2) { parent[Find(idx1)] = Find(idx2); }

    vector<double> calcEquation(vector<vector<string>>& equations, vector<double>& values, vector<vector<string>>& queries) {
        //
    }

    // // 简单BFS
    // unordered_map<string, set<pair<string, double>>> graph;

    // double dfs(const string src, const string dst) {
    //     if (graph.find(src) == graph.end()) return -1;

    //     unordered_set<string> visit;
    //     queue<pair<string, double>> qu;
    //     qu.push({src, 1});
    //     // visit.insert(src);

    //     while (qu.size()) {
    //         auto node = qu.front().first;
    //         auto weight = qu.front().second;
    //         qu.pop();

    //         if (node == dst) return weight;

    //         if (visit.find(node) != visit.end()) continue;
    //         visit.insert(node);

    //         for (auto& next : graph[node]) {
    //             double new_weight = weight * next.second;
    //             qu.push({next.first, new_weight});
    //         }
    //     }
    //     return -1;
    // }

    // vector<double> calcEquation(vector<vector<string>>& equations, vector<double>& values, vector<vector<string>>& queries) {
    //     for (int i = 0; i < equations.size(); i++) {
    //         graph[equations[i][0]].insert({equations[i][1], values[i]});
    //         graph[equations[i][1]].insert({equations[i][0], 1 / values[i]});
    //     }

    //     vector<double> ans;
    //     for (auto& qu : queries) ans.push_back(dfs(qu[0], qu[1]));
    //     return ans;
    // }
};
// @lc code=end

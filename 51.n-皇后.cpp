/*
 * @lc app=leetcode.cn id=51 lang=cpp
 *
 * [51] N 皇后
 */

#include <bits/stdc++.h>
#include <cstdio>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<string>> ans;

    vector<vector<string>> solveNQueens(int n) {
        vector<string> path(n, string(n, '.'));  // init
        dfs(path, 0, n);
        return ans;
    }
    void dfs(vector<string>& path, int level, const int n) {
        if (level == n) {
            ans.push_back(path);
            return;
        }
        for (int j = 0; j < n; j++) {
            if (isValid(path, level, j)) {
                path[level][j] = 'Q';
                dfs(path, level + 1, n);
                path[level][j] = '.';
            }
        }
    }
    //
    bool isValid(const vector<string>& path, int level, int j) {
        // int n = path.size();
        for (int r = level; r >= 0; r--)  // 上
            if (path[r][j] == 'Q') return false;
        for (int r = level, c = j; r >= 0 && c >= 0; r--, c--)  // 左上
            if (path[r][c] == 'Q') return false;
        for (int r = level, c = j; r >= 0 && c < path.size(); r--, c++)  // 右上
            if (path[r][c] == 'Q') return false;
        return true;  // 其他方向都是未放过皇后的，不可能为false
    }
};
// @lc code=end

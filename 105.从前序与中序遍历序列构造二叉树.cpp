/*
 * @lc app=leetcode.cn id=105 lang=cpp
 *
 * [105] 从前序与中序遍历序列构造二叉树
 */

#include <algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // TreeNode* build(vector<int>& preorder, vector<int>& inorder, int pl, int pr, int il, int ir) {
    //     if (pl > pr || il > ir) return nullptr;

    //     // 前序遍历第一个值为根节点
    //     TreeNode* root = new TreeNode(preorder[pl]);
    //     // 在中序遍历中找到根节点的位置
    //     int mid = il;
    //     while (inorder[mid] != preorder[pl]) mid++;

    //     // 构建左子树
    //     root->left = build(preorder, inorder, pl + 1, pl + mid - il, il, mid - 1);
    //     // 构建右子树
    //     root->right = build(preorder, inorder, pl + mid - il + 1, pr, mid + 1, ir);

    //     return root;
    // }

    // TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
    //     // 前序：根 左 右
    //     // 中序：左 根 右
    //     return build(preorder, inorder, 0, preorder.size() - 1, 0, inorder.size() - 1);
    // }

    TreeNode* build(vector<int>& preorder, vector<int>& inorder, int pl, int pr, int il, int ir, unordered_map<int, int>& idxMap) {
        if (pl > pr || il > ir) return nullptr;

        // 创建根节点
        TreeNode* root = new TreeNode(preorder[pl]);

        // 找到根节点在中序遍历中的位置
        int rIdx = idxMap[root->val];

        // 计算左子树的大小
        int leftSize = rIdx - il;

        // 递归构建左子树和右子树
        root->left = build(preorder, inorder, pl + 1, pl + leftSize, il, rIdx - 1, idxMap);
        root->right = build(preorder, inorder, pl + leftSize + 1, pr, rIdx + 1, ir, idxMap);

        return root;
    }

    TreeNode* buildTree(vector<int>& preorder, vector<int>& inorder) {
        unordered_map<int, int> idxMap;
        int n = inorder.size();
        // 哈希表存储中序遍历序列中每个节点值对应的索引
        for (int i = 0; i < n; ++i) idxMap[inorder[i]] = i;
        return build(preorder, inorder, 0, n - 1, 0, n - 1, idxMap);
    }
};
// @lc code=end

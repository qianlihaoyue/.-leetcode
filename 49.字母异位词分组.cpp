/*
 * @lc app=leetcode.cn id=49 lang=cpp
 *
 * [49] 字母异位词分组
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string> &strs) {
        // my: 全部重排序，然后找相同
        map<string, vector<string>> strmap;

        for (auto &str : strs) {
            string sortstr = str;
            std::sort(sortstr.begin(), sortstr.end());
            strmap[sortstr].emplace_back(str);
        }

        vector<vector<string>> res;
        for (auto &p : strmap) res.emplace_back(p.second);

        return res;

        // 分配质数法,但是需要背,这不可能

        // 位带计算,不行，存在相同的
        // map<uint32_t, vector<string>> strmap;
        // for (auto &str : strs) {
        //     uint32_t uni_key = 0x00;
        //     for (char c : str) uni_key |= (1 << (c - 'a'));
        //     strmap[uni_key].emplace_back(str);
        // }
        // vector<vector<string>> res;
        // for (auto &p : strmap) res.emplace_back(p.second);

        // return res;
    }
};
// @lc code=end

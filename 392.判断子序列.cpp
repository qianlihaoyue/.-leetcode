/*
 * @lc app=leetcode.cn id=392 lang=cpp
 *
 * [392] 判断子序列
 */

#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

// @lc code=start
class Solution {
public:
    bool isSubsequence(string s, string t) {
        // 从头开始
        int idx = 0;
        for (int i = 0; i < t.length(); i++) {
            if (t[i] == s[idx]) idx++;
        }
        if (idx == s.length()) return true;
        return false;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=35 lang=cpp
 *
 * [35] 搜索插入位置
 */

#include <algorithm>
#include <iterator>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int searchInsert(vector<int>& nums, int target) {
        // 经典二分法搜素
        int l = 0, r = nums.size() - 1;
        while (l <= r) {
            int mid = (l + r) >> 1;
            if (nums[mid] > target)
                r = mid - 1;
            else if (nums[mid] < target)
                l = mid + 1;
            else
                return mid;
        }
        
        return r + 1;  // 唯一不同

        // // 库函数
        // auto iter = std::lower_bound(nums.begin(), nums.end(), target);
        // return distance(nums.begin(), iter);
    }
};
// @lc code=end

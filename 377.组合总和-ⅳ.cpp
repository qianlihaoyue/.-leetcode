/*
 * @lc app=leetcode.cn id=377 lang=cpp
 *
 * [377] 组合总和 Ⅳ
 */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    // // 使用回溯会超时
    // int ans = 0;
    // void dfs(const vector<int>& nums, int target) {
    //     if (target == 0) {
    //         ans++;
    //         return;
    //     }
    //     if (target < 0) return;
    //     for (auto& n : nums) dfs(nums, target - n);
    // }
    // int combinationSum4(vector<int>& nums, int target) {
    //     dfs(nums, target);
    //     return ans;
    // }

    // DP
    int combinationSum4(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());  // 用于加速

        // vector<long> dp(target + 1, 0);
        // long 32/64位 long long 64位
        // unsigned 溢出了归0，而不会报错
        vector<unsigned> dp(target + 1, 0);
        dp[0] = 1;

        for (int i = 1; i <= target; ++i) {
            for (auto& n : nums) {
                if (i - n >= 0)
                    dp[i] += dp[i - n];
                else
                    break;
            }
        }

        return dp[target];
    }
};
// @lc code=end

int main() {
    vector<int> nums{10,  20,  30,  40,  50,  60,  70,  80,  90,  100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250,
                     260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500,
                     510, 520, 530, 540, 550, 560, 570, 580, 590, 600, 610, 620, 630, 640, 650, 660, 670, 680, 690, 700, 710, 720, 730, 740, 750,
                     760, 770, 780, 790, 800, 810, 820, 830, 840, 850, 860, 870, 880, 890, 900, 910, 920, 930, 940, 950, 960, 970, 980, 990, 111};
    Solution solu;
    std::cout << solu.combinationSum4(nums, 999) << std::endl;
}
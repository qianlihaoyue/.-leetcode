/*
 * @lc app=leetcode.cn id=637 lang=cpp
 *
 * [637] 二叉树的层平均值
 */

#include <algorithm>
#include <any>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<double> averageOfLevels(TreeNode *root) {
        vector<double> result;
        vector<TreeNode *> node_group;
        node_group.push_back(root);
        while (!node_group.empty()) {
            vector<TreeNode *> node_next;
            long int sum = 0;
            int cnt = 0;
            for (auto &node : node_group) {
                sum += node->val;
                cnt++;
                if (node->left != nullptr) node_next.push_back(node->left);
                if (node->right != nullptr) node_next.push_back(node->right);
            }
            node_group = node_next;
            result.push_back((double)sum / cnt);
        }
        return result;
    }
};
// @lc code=end

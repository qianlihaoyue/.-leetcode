/*
 * @lc app=leetcode.cn id=28 lang=cpp
 *
 * [28] 找出字符串中第一个匹配项的下标
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    int strStr(string haystack, string needle) {
        int n = haystack.size(), m = needle.size();
        if (n < m) return -1;

        // 暴力
        for (int i = 0; i < n - m + 1; i++) {
            int j = i, cnt = 0;
            while (cnt < m && haystack[j] == needle[cnt]) {
                j++, cnt++;
            }
            if (cnt == m) return i;
        }

        // KMP

        
        // if (m == 0) return 0;
        // // 设置哨兵
        // haystack.insert(haystack.begin(), ' ');
        // needle.insert(needle.begin(), ' ');
        // vector<int> next(m + 1);
        // // 预处理next数组
        // for (int i = 2, j = 0; i <= m; i++) {
        //     while (j && needle[i] != needle[j + 1]) j = next[j];
        //     if (needle[i] == needle[j + 1]) j++;
        //     next[i] = j;
        // }
        // // 匹配过程
        // for (int i = 1, j = 0; i <= n; i++) {
        //     while (j && haystack[i] != needle[j + 1]) j = next[j];
        //     if (haystack[i] == needle[j + 1]) j++;
        //     if (j == m) return i - m;
        // }

        return -1;
    }
};
// @lc code=end

int main() {
    Solution solute;
    string haystack = "abb", needle = "abaaa";
    cout << solute.strStr(haystack, needle);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

/*
 * @lc app=leetcode.cn id=209 lang=cpp
 *
 * [209] 长度最小的子数组
 */

#include <algorithm>
#include <vector>
#include <iostream>
#include <limits>
using namespace std;

// @lc code=start
class Solution {
public:
    // 一眼滑窗
    int minSubArrayLen(int target, vector<int>& nums) {
        int min_len = nums.size() + 1;
        int sum = 0, start = 0;

        // 外圈遍历
        for (int i = 0; i < nums.size(); i++) {
            sum += nums[i];
            while (sum >= target) {
                min_len = std::min(min_len, i - start + 1);
                sum -= nums[start++];
            }
        }

        if (min_len == nums.size() + 1) return 0;
        return min_len;
    }
};
// @lc code=end

int main() {
    Solution solute;

    vector<int> nums = {2, 3, 1, 2, 4, 3};
    std::cout << solute.minSubArrayLen(7, nums) << endl;

    return 0;
}

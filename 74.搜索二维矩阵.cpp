/*
 * @lc app=leetcode.cn id=74 lang=cpp
 *
 * [74] 搜索二维矩阵
 */

#include <algorithm>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        // 也就是个嵌套，先找哪一行，再找哪一列
        // int start_row=0,end_row=matrix.size();

        // 2.展开成一维数组，然后二分查找
        int m = matrix.size(), n = matrix[0].size();
        // 逐行这种思路好，不逐行就得求余，求除法
        for (int i = 0; i < m; i++) {
            // 快速判断，局限在一行
            if (target > matrix[i][n - 1]) continue;
            if (target == matrix[i][0] || target == matrix[i][n - 1]) return true;

            int left = 0, right = n - 1;
            // 二分法统一使用 左闭右闭
            while (left <= right) {
                int mid = (left + right) / 2;
                if (matrix[i][mid] > target)
                    right = mid - 1;
                else if (matrix[i][mid] < target)
                    left = mid + 1;
                else if (matrix[i][mid] == target)
                    return true;
            }
            return false;
        }
        return false;
    }
};
// @lc code=end

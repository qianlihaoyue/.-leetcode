/*
 * @lc app=leetcode.cn id=151 lang=cpp
 *
 * [151] 反转字符串中的单词
 */

#include <bits/stdc++.h>
#include "slam/timer.h"
using namespace std;

// @lc code=start
// easy
class Solution {
public:
    string reverseWords(string s) {
        string word, result;
        std::istringstream iss(s);
        while (iss >> word) result = word + " " + result;
        result.pop_back();
        return result;
    }
};

// vector<string> words;
// for (char &c : s) {
//     if (c == ' ') {
//         if (!word.empty()) {
//             words.push_back(word);
//             word.clear();
//         }
//     } else
//         word.push_back(c);
// }
// if (!word.empty()) words.push_back(word);
// @lc code=end

int main() {
    Solution solute;

    int cnt = 1000;
    TIMER_CREATE(tim);
    while (cnt--) {
        string s = "the sky is blue asdasd asdasd adasd asdasd asdasd fgdf gdfgdfg dfgdfg dfgdfg";
        solute.reverseWords(s);
    }
    tim.toc_cout();

    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

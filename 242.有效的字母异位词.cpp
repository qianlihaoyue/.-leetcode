/*
 * @lc app=leetcode.cn id=242 lang=cpp
 *
 * [242] 有效的字母异位词
 */

#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

// @lc code=start
class Solution {
public:
    bool isAnagram(string s, string t) {
        int s_table[26] = {0};
        for (auto &c : s) s_table[c - 'a']++;
        for (auto &c : t) s_table[c - 'a']--;
        for (auto &n : s_table)
            if (n) return false;
        return true;
    }
};
// @lc code=end

int main() {
    Solution solute;

    auto result = solute.isAnagram("anagrm", "nagaram");
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    cout << result << endl;

    return 0;
}

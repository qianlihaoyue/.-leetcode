/*
 * @lc app=leetcode.cn id=572 lang=cpp
 *
 * [572] 另一棵树的子树
 */

#include <algorithm>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    bool isSameTree(TreeNode* p, TreeNode* q) {
        if (!p && !q) return true;
        if (!p || !q) return false;
        if (p->val != q->val) return false;
        return isSameTree(p->left, q->left) && isSameTree(p->right, q->right);
    }

    // 我的
    // void recurpre(TreeNode* root, std::vector<TreeNode*>& root_group, int target) {
    //     if (!root) return;
    //     if (root->val == target) root_group.push_back(root);
    //     recurpre(root->left, root_group, target);
    //     recurpre(root->right, root_group, target);
    // }

    // bool isSubtree(TreeNode* root, TreeNode* subRoot) {
    //     // 先查找顶点
    //     std::vector<TreeNode*> root_group;
    //     recurpre(root, root_group, subRoot->val);

    //     // 遍历顶点
    //     for (auto& node : root_group)
    //         if (isSameTree(node, subRoot)) return true;

    //     return false;
    // }

    // 抄的，一个树是另一个树的子树 则
    // 要么这两个树相等，要么这个树是左树的子树，要么这个树hi右树的子树
    bool isSubtree(TreeNode* s, TreeNode* t) {
        if (s == NULL && t == NULL) return true;
        if (s == NULL && t != NULL) return false;
        return isSameTree(s, t) || isSubtree(s->left, t) || isSubtree(s->right, t);
    }
};
// @lc code=end

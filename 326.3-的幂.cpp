/*
 * @lc app=leetcode.cn id=326 lang=cpp
 *
 * [326] 3 的幂
 */

#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    bool isPowerOfThree(int n) {
        double num = (double)n;
        while (num >= 27) num /= 27;
        while (num >= 9) num /= 9;
        while (num >= 3) num /= 3;
        return num == 1.0;

        // 数学法
        // return n > 0 && 1162261467%n == 0;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=26 lang=cpp
 *
 * [26] 删除有序数组中的重复项
 */

#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if (nums.size() < 2) return nums.size();
        // for (auto it = nums.begin() + 1; it != nums.end();) {
        //     if (*it == *(it - 1))
        //         nums.erase(it);
        //     else
        //         it++;
        // }
        // return nums.size();

        int j = 0;
        for (int i = 1; i < nums.size(); i++)
            if (nums[j] != nums[i]) nums[++j] = nums[i];
        return ++j;
    }
};
// @lc code=end

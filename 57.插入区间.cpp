/*
 * @lc app=leetcode.cn id=57 lang=cpp
 *
 * [57] 插入区间
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> insert(vector<vector<int>>& intervals, vector<int>& newInterval) {
        // if (intervals.empty()) return {newInterval};
        bool ins_flg = false;
        for (int i = 0; i < intervals.size(); i++) {
            if (intervals[i][0] > newInterval[0]) {
                intervals.insert(intervals.begin() + i, newInterval);
                ins_flg = true;
                break;
            }
        }
        // 首尾特殊情况，都添加在末尾
        if (!ins_flg) intervals.push_back(newInterval);

        vector<vector<int>> res;
        for (int i = 0; i < intervals.size();) {
            int t = intervals[i][1];
            int j = i + 1;
            while (j < intervals.size() && t >= intervals[j][0]) t = max(t, intervals[j++][1]);

            res.push_back({intervals[i][0], t});
            i = j;
        }
        return res;
    }
};
// @lc code=end

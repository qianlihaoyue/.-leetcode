/*
 * @lc app=leetcode.cn id=17 lang=cpp
 *
 * [17] 电话号码的字母组合
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

#include "slam/timer.h"

// @lc code=start
class Solution {
public:
    // const 能极大缩短时间
    const vector<string> digs = {"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    vector<string> ans;
    string path;  // 节省空间
    void dfs(const string& nums, int i) {
        if (i == nums.size()) {
            ans.push_back(path);
            return;
        }
        for (auto& c : digs[nums[i] - '2']) {
            path.push_back(c);
            dfs(nums, i + 1);
            path.pop_back();
        }
    }

    vector<string> letterCombinations(string digits) {
        if (digits == "") return ans;
        dfs(digits, 0);
        return ans;
    }
};
// @lc code=end

int main(int argc, char* argv[]) {
    Solution solu;

    TIMER_CREATE(tim_cnt);
    auto res = solu.letterCombinations("24534334676784");
    tim_cnt.toc_cout();
    // for (auto &num : res) {
    //     for (auto &n : num) cout << n << " ";
    //     cout << endl;
    // }

    return 0;
}

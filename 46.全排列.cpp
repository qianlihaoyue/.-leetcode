/*
 * @lc app=leetcode.cn id=46 lang=cpp
 *
 * [46] 全排列
 */

#include <bits/stdc++.h>
#include <set>
#include <unordered_set>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    // 递归算法：当前项依次和后面项交换，然后递归处理剩余数组
    vector<vector<int>> ans;
    void perm_dfs(vector<int>& nums, int i) {
        if (i == nums.size() - 1) {
            ans.push_back(nums);
            return;
        }
        for (int j = i; j < nums.size(); ++j) {
            swap(nums[i], nums[j]);
            perm_dfs(nums, i + 1);
            swap(nums[i], nums[j]);  // 恢复
        }
    }

    // unordered_set<int> visit;
    // // vector<int> path;
    // void dfs_set(const vector<int>& nums, int i) {
    //     if (i == nums.size()) {
    //         vector<int> path(visit.begin(), visit.end());
    //         ans.push_back(path);
    //         return;
    //     }
    //     for (auto& n : nums) {
    //         if (visit.find(n) == visit.end()) {
    //             visit.insert(n);
    //             dfs_set(nums, i + 1);
    //             visit.erase(visit.find(n));
    //         }
    //     }
    // }

    vector<vector<int>> permute(vector<int>& nums) {
        // vector<vector<int>> ans;
        // sort(nums.begin(), nums.end());
        // // next_permutation： 用于生成给定序列的下一个排列，原理基于字典序
        // // do {
        // //     ans.push_back(nums);
        // // } while (next_permutation(nums.begin(), nums.end()));

        // // my_next_permutation 100% 95%
        // do {
        //     ans.push_back(nums);
        // } while (my_next_permutation(nums));

        perm_dfs(nums, 0);
        // dfs_set(nums, 0);
        return ans;

        // return perm_by_insert(nums);
    }

    // 寻找下一个数：找到比当前数更大的数字中最小的那个，需要先排序
    bool my_next_permutation(vector<int>& nums) {
        if (nums.size() <= 1) return false;

        // 从后往前找第一个非递增的元素
        int i = nums.size() - 2;
        while (i >= 0 && nums[i] >= nums[i + 1]) i--;

        // 如果找不到非递增的元素，说明当前排列已经是最后一个排列，返回 false
        if (i < 0) return false;

        // 从后往前找第一个比 nums[i] 大的元素
        int j = nums.size() - 1;
        while (j > i && nums[j] <= nums[i]) j--;

        swap(nums[i], nums[j]);
        // 将下标 i+1 之后的元素逆序排列
        reverse(nums.begin() + i + 1, nums.end());

        return true;
    }

    // 插入法构造新排列，直观，但效率低
    vector<vector<int>> perm_by_insert(vector<int>& nums) {
        int n = nums.size();
        // 定义一个数组，记录第i个元素剩余选择次数
        vector<int> p(n);
        for (int i = 0; i < n; ++i) p[i] = i;

        vector<vector<int>> ans;

        while (true) {
            vector<int> temp;
            // 输出当前序列
            for (int j = 0; j < n; ++j) temp.insert(temp.begin() + p[j], nums[j]);
            ans.push_back(temp);

            // 从后往前找第一个还有剩余次数的位置
            int i = n - 1;
            while (i >= 0 && p[i] == 0) i--;

            if (i < 0) break;  // 没有次数了，跳出循环

            p[i]--;  // 减少次数
            // 后面的恢复次数
            for (int j = i + 1; j < n; ++j) p[j] = j;
        }

        return ans;
    }
};
// @lc code=end

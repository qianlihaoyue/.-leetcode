#include "my_smartptr.hpp"
#include "my_vector.hpp"
#include <cstring>
#include <iostream>
#include <memory>
#include <vector>

void smartptr_test() {
    SharedPtr<int> ptr1(new int(42));
    std::cout << "ptr1: " << *ptr1 << " (use_count: " << ptr1.use_count() << ")" << std::endl;

    SharedPtr<int> ptr2 = ptr1;
    std::cout << "ptr2: " << *ptr2 << " (use_count: " << ptr2.use_count() << ")" << std::endl;

    {
        SharedPtr<int> ptr3 = ptr1;
        std::cout << "ptr3: " << *ptr3 << " (use_count: " << ptr3.use_count() << ")" << std::endl;
    }

    std::cout << "ptr2: " << *ptr2 << " (use_count: " << ptr2.use_count() << ")" << std::endl;
}

void vector_test() {
    // 测试 () 初始化
    my::vector<int> vec;
    // std::vector<int> vec;
    std::cout << "Size after initialization: " << vec.size() << std::endl;

    std::cout << "Testing push_back and size: \t";
    for (int i = 0; i < 5; ++i) {
        vec.push_back(i);
        std::cout << vec.size() << " ";
    }
    std::cout << std::endl;

    std::cout << "Testing pop_back and size: \t\t";
    while (!vec.empty()) {
        vec.pop_back();
        std::cout << vec.size() << " ";
    }
    std::cout << std::endl;

    std::cout << "Testing erase and size: \t\t";
    for (int i = 0; i < 10; ++i) vec.push_back(i);

    // vec.erase(5);  // 删除索引为 5 的元素
    // std::cout << vec.size() << std::endl;

    // std::cout << "Testing insert and size: \t\t";
    // vec.insert(5, 100);  // 在索引为 5 的位置插入元素 100
    // std::cout << vec.size() << std::endl;
}

#include "my_memory_pool.hpp"

class DataT {
public:
    int value;
    // 需要提供重置方法
    void reset() { value = 0; }
};

void memory_pool_test() {
    const int pollCap = 10;

    // 创建内存池，增长大小设置为 5
    MyMemoryPool<DataT> pool([]() { return new DataT(); }, 5);
    pool.expand_pool(pollCap);  // 不安全分配，需要提前确保容量

    DataT* objects[pollCap];

    // 分配对象
    for (int i = 0; i < pollCap; ++i) {
        objects[i] = pool.allocate();
        objects[i]->value = i;  // 设置每个对象的 value 为其索引
        std::cout << "Allocated value: " << objects[i]->value << std::endl;
    }
    // 释放所有对象
    for (int i = 0; i < pollCap; ++i) pool.deallocate(objects[i]);

    // 重新分配对象并检查是否重置
    for (int i = 0; i < pollCap; ++i) {
        objects[i] = pool.allocate();
        std::cout << "reset value: " << objects[i]->value << std::endl;  // 应输出 0
    }
}

int main() {
    // smartptr_test();
    // vector_test();
    memory_pool_test();

    return 0;
}

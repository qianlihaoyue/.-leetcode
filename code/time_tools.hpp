/**
 * @author qianlihaoyue (qianlihaoyue@gmail.com)
 * @brief 计时工具
 * @version 0.4
 * @date 2024-04-24
 */
#pragma once

#include <chrono>
#include <iostream>
#include <string>

#define USE_OMP_GET_TIME 0

#if USE_OMP_GET_TIME
#include <omp.h>
#define GET_TIME() omp_get_wtime()
#define GET_USED(t2, t1) (t2 - t1)
#else
// system_clock high_resolution_clock
#define GET_TIME() std::chrono::high_resolution_clock::now()
#define GET_USED(t2, t1) std::chrono::duration<double>(t2 - t1).count()
#endif

class Timer {
public:
    Timer() { tic(); }
    Timer(const std::string& nameIn) : name(nameIn) { tic(); }
    void tic() { start = GET_TIME(); }
    double toc() { return GET_USED(GET_TIME(), start) * 1000; }
    void toc_cout() { std::cout << "[" << name << "]:" << toc() << "ms" << std::endl; }

private:
    const std::string name;
    std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
};

#define TIMER_CREATE(name) Timer name(#name)

#include <iomanip>
#include <sstream>
#include <fstream>
#include <cmath>
#include <vector>
#include <numeric>
#include <map>

// 全局静态类，用于时间分析
class TimerRecord {
public:
    static void add_record(const std::string& name, double time) { records_[name].push_back(time); }

    template <class F>
    static void Evaluate(const std::string& name, F&& func) {
        auto t1 = GET_TIME();
        std::forward<F>(func)();
        add_record(name, GET_USED(GET_TIME(), t1) * 1000);
    }

    static void DumpIntoFile(const std::string& file_name, bool print_flag = false) { dump_to_file(file_name, print_flag); }
    static void dump_to_file(const std::string& file_name, bool print_flag = false);

private:
    static std::map<std::string, std::vector<double>> records_;
};

std::map<std::string, std::vector<double>> TimerRecord::records_;

void TimerRecord::dump_to_file(const std::string& file_name, bool print_flag) {
    // 创建一个字符串流
    std::stringstream ss;
    std::ofstream ofs(file_name, std::ios::out);

    if (!ofs.is_open()) {
        std::cerr << "Failed to open file: " << file_name << "!!!" << std::endl;
        return;
    } else
        std::cout << "Dump Time Records into file: " << file_name << std::endl;

    ss << ">>> ===== Printing run time =====" << std::endl;
    if (print_flag) std::cout << ss.str();
    ofs << ss.str(), ss.str("");

    // 设置列宽
    const int nameWidth = 40;
    const int scoreWidth = 15;

    // 输出表头
    ss << std::left << std::setw(nameWidth) << "Function"  //
       << std::setw(scoreWidth) << "Ave time"              // 平均运行时间
       << std::setw(scoreWidth) << "S.D time"              // 运行时间标准差
       << std::setw(scoreWidth) << "Called times"          // 调用次数
       << std::setw(scoreWidth) << "All time"              // 平均时间*调用次数
       << std::endl;
    if (print_flag) std::cout << ss.str();
    ofs << ss.str(), ss.str("");

    // 输出分隔线
    ss << std::setfill('-') << std::setw(nameWidth + 4 * scoreWidth) << "" << std::setfill(' ') << std::endl;
    if (print_flag) std::cout << ss.str();
    ofs << ss.str(), ss.str("");

    // 遍历计算每一项
    for (auto& r : records_) {
        std::vector<double>& tim = r.second;
        double ave_time = std::accumulate(tim.begin(), tim.end(), 0.0) / double(tim.size());  // 时间均值
        // 计算方差
        double variance = 0.0;
        for (const auto& num : tim) variance += (num - ave_time) * (num - ave_time);
        variance /= tim.size();

        ss << std::fixed << std::setprecision(3)                      //
           << std::left << std::setw(nameWidth) << r.first            // 函数名
           << std::setw(scoreWidth) << ave_time                       // 平均运行时间
           << std::setw(scoreWidth) << sqrt(variance)                 // 运行时间标准差
           << std::setw(scoreWidth) << tim.size()                     // 调用次数
           << std::setw(scoreWidth) << ave_time * double(tim.size())  // 平均时间*调用次数
           << std::endl;

        if (print_flag) std::cout << ss.str();
        ofs << ss.str(), ss.str("");
    }

#if 0
        ofs << ">>> ===== Printing run time end =====" << std::endl;
        // 输出所有数据
        size_t max_length = 0;
        for (const auto& kv : records_) {
            ofs << std::setw(scoreWidth) << kv.first;
            max_length = std::max(max_length, kv.second.size());
        }
        ofs << std::endl;

        for (size_t i = 0; i < max_length; ++i) {
            for (const auto& kv : records_) {
                const auto& iter = kv.second;
                if (i < iter.size())
                    ofs << std::setw(scoreWidth) << iter[i];
                else
                    ofs << std::setw(scoreWidth) << "-";
                ofs << std::setw(scoreWidth);
            }
            ofs << std::endl;
        }
        ofs.close();
#endif
}
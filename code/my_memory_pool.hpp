#pragma once

#include <vector>
#include <functional>

// #define SAFE_ALLOCATE

template <typename DataType>
class MyMemoryPool {
public:
    // 定义一个函数类型，用于构造 DataType 类型的对象
    using ConstructFunc = std::function<DataType*()>;

    // 默认构造函数
    MyMemoryPool() : construct_func_(nullptr), increm_size_(1000), remain_num_(0) {}

    // 构造函数，接受一个构造函数和一个可选的增长大小
    MyMemoryPool(ConstructFunc cfunc, size_t incr_size = 1000) : construct_func_(cfunc), increm_size_(incr_size), remain_num_(0) {}

    // 设置构造函数
    void set_construct_func(ConstructFunc construct_func) { construct_func_ = construct_func; }

    // 设置内存池的增长大小
    void set_increm_size(int increm_size) { increm_size_ = increm_size; }

    // 扩展内存池以容纳至少 n 个对象
    void expand_pool(int n) {
        if (pool_.size() < n) {
            int stop_size = remain_num_ + n - pool_.size();
            pool_.resize(n);
            for (; remain_num_ < stop_size; remain_num_++) {
                pool_[remain_num_] = construct_func_();
            }
        }
    }

    // 安全分配一个对象
    DataType* allocate_safe() {
        if (remain_num_ == 0) expand_pool(pool_.size() + increm_size_);
        remain_num_--;
        return pool_[remain_num_];
    }

    // 快速分配一个对象，不检查是否需要扩展池
    DataType* allocate_fast() {
        remain_num_--;
        return pool_[remain_num_];
    }

    // 根据 SAFE_ALLOCATE 宏选择分配方式
    DataType* allocate() {
#ifdef SAFE_ALLOCATE
        return allocate_safe();
#else
        return allocate_fast();
#endif
    }

    // 释放一个对象，将其重新放入池中
    void deallocate(DataType* data) {
        data->reset();
        pool_[remain_num_] = data;
        remain_num_++;
    }

private:
    size_t remain_num_;             // 池中剩余可用对象的数量
    std::vector<DataType*> pool_;   // 存储对象指针的向量
    size_t increm_size_;            // 池的增长大小
    ConstructFunc construct_func_;  // 用于构造对象的函数
};

/*
// 相当于提前占用一块内存，专门服务。

static MyMemoryPool<HVoxNode> voxel_pool_;
std::vector<HVoxNode*> map_content;

voxel_pool_.set_increm_size(voxel_pool_increm_size);
voxel_pool_.set_construct_func([](){ return new HVoxNode(); });
voxel_pool_.expand_pool(reserve_voxel_size); // 初始化分配

if (!map_content[0])
    map_content[0] = voxel_pool_.allocate();

for(auto& ptr: map_content) {
    if(ptr) {
        voxel_pool_.deallocate(ptr);
        ptr = nullptr;
    }
}
*/

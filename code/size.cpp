#include <cstring>
#include <iostream>
#include <vector>

struct MyStruct {
    double a;  // 8 个字节
    char b;    // 本来占一个字节，但是接下来的 int 需要起始地址为4的倍数，所以这里也会加3字节的padding
    int c;     // 4 个字节    总共: 8 + 4 + 4 = 16
};

// 24
struct MyStruct2 {
    char b;    // 本来1个字节 + 7个字节padding
    double a;  // 8 个字节
    int c;     // 本来 4 个字节，但是整体要按 8 字节对齐，所以 4个字节padding， 总共: 8 + 8 + 8 = 24
};

// 默认即对齐
struct AlignedStruct {
    char a;   //  1 + padding 3
    int b;    //  4
    short c;  //  2 + padding 2 , 按照 4 字节对齐(成员对齐边界最大的是int 4) , 4+4+4=12
};

#pragma pack(push, 1)  // 设置字节对齐为 1 字节，取消自动对齐
struct UnalignedStruct {
    char a;   // 1
    int b;    // 4
    short c;  // 2
};
#pragma pack(pop)  // 恢复默认的字节对齐设置

struct EmptyStruct {};

void func(char array[]) {
    std::cout << "sizeof: " << sizeof(array) << std::endl;  // 指针
    std::cout << "strlen: " << strlen(array) << std::endl;  // 长度
}

int main() {
    std::cout << "--------------" << std::endl;
    std::cout << "bool: " << sizeof(bool) << std::endl;
    std::cout << "char: " << sizeof(char) << std::endl;
    std::cout << "short: " << sizeof(short) << std::endl;
    std::cout << "int: " << sizeof(int) << std::endl;
    std::cout << "float: " << sizeof(float) << std::endl;
    std::cout << "double: " << sizeof(double) << std::endl;

    std::cout << "--------------" << std::endl;
    char str[] = "Hello";
    char *p = str;
    double *dp;
    std::cout << "str: " << sizeof(str) << std::endl;
    std::cout << "p: " << sizeof(p) << std::endl;
    std::cout << "dp: " << sizeof(dp) << std::endl;

    std::cout << "--------------" << std::endl;
    std::cout << "MyStruct: " << sizeof(MyStruct) << std::endl;
    std::cout << "MyStruct2: " << sizeof(MyStruct2) << std::endl;
    std::cout << "EmptyStruct: " << sizeof(EmptyStruct) << std::endl;
    std::cout << "UnalignedStruct: " << sizeof(UnalignedStruct) << std::endl;
    std::cout << "AlignedStruct: " << sizeof(AlignedStruct) << std::endl;

    std::cout << "--------------" << std::endl;
    char array[] = "Hello";
    std::cout << "sizeof: " << sizeof(array) << std::endl;  // 内存+1
    std::cout << "strlen: " << strlen(array) << std::endl;  // 长度
    func(array);

    std::cout << "--------------" << std::endl;
    // std::vector<int> vec(2);
    // std::cout << "vec: " << sizeof(vec) << std::endl;
    // std::cout << "1==1: " << sizeof(1 == 1) << std::endl; // bool

    return 0;
}

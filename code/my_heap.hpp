#include <functional>
#include <vector>

template <typename T = int, typename Cmp = std::less<T>>
class Heap {
public:
    // Heap(const Cmp& cmp = Cmp()) : cmp(cmp) {}

    int size() { return data.size(); }
    bool empty() { return data.empty(); }
    int top() { return data[0]; }

    // 入堆：
    void push(T val) {
        data.push_back(val);  // 先将元素插入末尾
        sift_up(size() - 1);  // 然后从下往上交换
    }
    void emplace(T val) {
        data.emplace_back(val);
        sift_up(size() - 1);
    }
    // 出堆
    void pop() {
        // if (empty()) throw std::out_of_range("empty");
        std::swap(data[0], data[size() - 1]);  // 先将堆顶与堆底交换
        data.pop_back();                       // 然后删除末尾节点
        sift_down(0);                          // 然后从上往下交换
    }

private:
    std::vector<T> data;
    Cmp cmp;

    // 从下往上
    void sift_up(int i) {
        while (1) {
            int p = (i - 1) / 2;                         // 与父节点进行对比
            if (p < 0 || !cmp(data[i], data[p])) break;  // 大顶堆 <=
            std::swap(data[i], data[p]);
            i = p;
        }
    }

    void sift_down(int i) {
        while (1) {
            int l = 2 * i + 1, r = 2 * i + 2, m = i;
            // 不，是先左后右，如果左边小，就先左边，然后左右互比，如果右边更小，就右边
            if (l < size() && cmp(data[l], data[m])) m = l;
            if (r < size() && cmp(data[r], data[m])) m = r;
            if (m == i) break;  // 两边都无法推进，或到达边界
            std::swap(data[i], data[m]);
            i = m;
        }
    }
};

// 大顶堆，输出的是 从小到大
/* 堆的长度为 n ，从节点 i 开始，从顶至底堆化 */
void sift_down(std::vector<int>& nums, int n, int i) {
    while (1) {
        // 判断节点 i, l, r 中值最大的节点，记为 ma
        int l = 2 * i + 1, r = 2 * i + 2, ma = i;
        if (l < n && nums[l] > nums[ma]) ma = l;
        if (r < n && nums[r] > nums[ma]) ma = r;
        // 若节点 i 最大或索引 l, r 越界，则无须继续堆化，跳出
        if (ma == i) break;
        std::swap(nums[i], nums[ma]);
        i = ma;  // 循环向下堆化
    }
}

void heap_sort(std::vector<int>& nums) {
    // 建堆操作：堆化除叶节点以外的其他所有节点，已经考虑叶子了
    for (int i = nums.size() / 2 - 1; i >= 0; --i) sift_down(nums, nums.size(), i);

    // 从堆中提取最大元素，循环 n-1 轮
    for (int i = nums.size() - 1; i > 0; --i) {
        std::swap(nums[0], nums[i]);  // 交换根节点与最右叶节点（交换首元素与尾元素）
        sift_down(nums, i, 0);        // 以根节点为起点，从顶至底进行堆化
    }
}
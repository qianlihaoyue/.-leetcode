#include <iostream>

template <typename T>
class SharedPtr {
public:
    // 构造函数，如果是指针，初始化计数为1
    explicit SharedPtr(T* ptr = nullptr) : ptr(ptr), cnt(new size_t(ptr ? 1 : 0)) {}

    // 拷贝构造函数
    SharedPtr(const SharedPtr& other) : ptr(other.ptr), cnt(other.cnt) {  incrRefCount(); }

    // 重载赋值运算符
    SharedPtr& operator=(const SharedPtr& other) {
        if (this != &other) {
            decrRefCount();  // 需要先释放当前资源（替换）
            ptr = other.ptr;
            cnt = other.cnt;
            incrRefCount();
        }
        return *this;
    }

    // 析构函数，降低引用
    ~SharedPtr() { decrRefCount(); }

    // 解引用操作符
    T& operator*() const { return *ptr; }

    // 成员访问操作符
    T* operator->() const { return ptr; }

    // 获取引用计数
    size_t use_count() const { return *cnt; }

private:
    // 都需要改成指针的形式，多个类对象使用同一块地址
    T* ptr;       // 指向实际对象的指针
    size_t* cnt;  // 引用计数

    // 增加引用计数
    void incrRefCount() { ++(*cnt); }

    // 减少引用计数，如果引用计数为 0，则删除指针
    void decrRefCount() {
        if (--(*cnt) == 0) {
            delete ptr;
            delete cnt;
        }
    }
};


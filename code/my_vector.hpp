#include <iostream>
#include <stdexcept>

namespace my {
template <typename T>
class vector {
public:
    vector() {}
    vector(int cap) : cap_(cap), size_(cap), data(new T[cap]) {}

    ~vector() { delete[] data; }

    void push_back(const T& value) {
        if (size_ == cap_) extend();
        data[size_++] = value;
    }

    void pop_back() {
        if (size_ > 0) --size_;
    }

    T& operator[](size_t index) {
        if (index >= size_) throw std::out_of_range("Index out of range");
        return data[index];
    }

    const size_t size() const { return size_; }

    bool empty() const { return size_ == 0; }

    void erase(size_t index) {
        if (index >= size_) throw std::out_of_range("Index out of range");
        for (size_t i = index; i < size_ - 1; ++i) data[i] = std::move(data[i + 1]);
        --size_;
    }

    void insert(size_t index, const T& value) {
        if (index > size_) throw std::out_of_range("Index out of range");
        if (size_ == cap_) extend();
        for (size_t i = size_; i > index; --i) data[i] = std::move(data[i - 1]);
        data[index] = value;
        ++size_;
    }

private:
    T* data = nullptr;
    size_t cap_ = 0;
    size_t size_ = 0;

    void extend() {
        cap_ = (cap_ == 0) ? 1 : cap_ * 2;
        T* new_data = new T[cap_];
        // 当单个数据T较大时，使用移动语义更快
        // for (size_t i = 0; i < size_; ++i) newData[i] = data[i];
        for (size_t i = 0; i < size_; ++i) new_data[i] = std::move(data[i]);
        delete[] data;
        data = new_data;
    }
};

}  // namespace my



#include <algorithm>
#include <vector>

////////////////////////////// 比较排序 O(n^2)

// 选择排序：第一遍0~n-1,每次选择后面序列的最小元素，放到第i位
void select_sort(std::vector<int>& nums) {
    for (int i = 0; i < nums.size() - 1; i++) {
        for (int j = i + 1; j < nums.size(); j++) {
            if (nums[j] < nums[i]) std::swap(nums[i], nums[j]);
        }
    }
}

void select_sort2(std::vector<int>& nums) {
    for (int i = 0; i < nums.size() - 1; i++) {
        int min_idx = i;
        for (int j = i + 1; j < nums.size(); j++) {
            if (nums[j] < nums[min_idx]) min_idx = j;
        }
        std::swap(nums[min_idx], nums[i]);
    }
}

// 冒泡排序：第一遍从大到小 ,每次遍历 0~i
void bubble_sort(std::vector<int>& nums) {
    for (int i = nums.size() - 1; i > 0; i--) {
        for (int j = 0; j < i; j++) {
            if (nums[j] > nums[j + 1]) std::swap(nums[j], nums[j + 1]);
        }
    }
}

// 插入排序：每次选择第i个元素，插入到之前排好的序列
void insert_sort(std::vector<int>& nums) {
    for (int i = 1; i < nums.size(); i++) {
        int base = nums[i], j = i - 1;  // 此处已经空下
        while (j >= 0 && nums[j] > base) {
            nums[j + 1] = nums[j];
            j--;
        }
        nums[j + 1] = base;
    }
}

////////////////////////////// 比较排序 O(nlog(n)) - 快排

// 快速排序：
void quick_sort(std::vector<int>& nums, int l, int r) {
    if (r <= l) return;
    int mid = l + (r - l) / 2;
    std::nth_element(nums.begin() + l, nums.begin() + mid, nums.begin() + r);
    quick_sort(nums, l, mid);  // nth_element: 左闭右开
    quick_sort(nums, mid + 1, r);
}
// nth_element: 左闭右开
void quick_sort_nth(std::vector<int>& nums) { quick_sort(nums, 0, nums.size()); }

// 一次划分，返回划分的索引，不能指定划分到第几个
// nth_element 相当于手动指定划分，内部经过多次划分
int mypart(std::vector<int>& nums, int l, int r) {
    int i = l, j = r, pivot = nums[l];
    while (i < j) {
        // 是有顺序的，必须按照这个顺序
        while (i < j && nums[j] >= pivot) j--;  // 从右向左找首个小于基准数的元素
        while (i < j && nums[i] <= pivot) i++;  // 左边
        std::swap(nums[i], nums[j]);
    }
    std::swap(nums[i], nums[l]);  // 将基准数交换至两子数组的分界线
    return i;
}
void quick_sort2(std::vector<int>& nums, int l, int r) {
    if (l >= r) return;
    int pidx = mypart(nums, l, r);
    quick_sort2(nums, l, pidx - 1);
    quick_sort2(nums, pidx + 1, r);
}
// 左闭右闭
void quick_sort_part(std::vector<int>& nums) { quick_sort2(nums, 0, nums.size() - 1); }

////////////////////////////// 比较排序 O(nlog(n)) - 堆排

#include <queue>

void heap_sort_pq(std::vector<int>& nums) {
    // sort 默认 less,升序
    // priority_queue 默认less, 大顶堆，内部降序
    // 但使用堆排，大顶堆，由于交换，所以降序
    std::priority_queue<int, std::vector<int>, std::greater<int>> heap;
    for (auto& n : nums) heap.emplace(n);
    for (int i = 0; i < nums.size(); i++) {
        nums[i] = heap.top();
        heap.pop();
    }
}

#include "my_heap.hpp"
void heap_sort_heap(std::vector<int>& nums) {
    Heap<int, std::less<int>> heap;
    for (auto& n : nums) heap.emplace(n);
    for (int i = 0; i < nums.size(); i++) {
        nums[i] = heap.top();
        heap.pop();
    }
}

void heap_sort_mh(std::vector<int>& nums) {
    make_heap(nums.begin(), nums.end(), std::less<int>());
    // 不断地将堆顶元素移动到未排序部分的末尾
    for (int i = nums.size() - 1; i > 0; --i) {
        pop_heap(nums.begin(), nums.begin() + i + 1, std::less<int>());
    }
}

// 归并
void merge_sort(std::vector<int>& nums, int l, int r) {}

////////////////////////////// 空间换时间

void bucket_sort(std::vector<int>& nums) {
    if (nums.empty()) return;

    // 计算桶的数量
    int min_val = *std::min_element(nums.begin(), nums.end());
    int max_val = *std::max_element(nums.begin(), nums.end());

    // 计算桶的数量
    int num_buckets = nums.size() / 2;
    std::vector<std::vector<int>> buckets(num_buckets);

    // 1. 将元素分配到各个桶中,这一步才是关键，如何分配
    for (auto num : nums) {
        int i = (num - min_val) * num_buckets / (max_val - min_val + 1);
        buckets[i].push_back(num);
    }

    // 2. 对各个桶执行排序
    for (auto& bucket : buckets) sort(bucket.begin(), bucket.end());

    // 3. 按照桶顺序合并结果
    int i = 0;
    for (auto& bucket : buckets)
        for (int num : bucket) nums[i++] = num;
}

///////////////////////////////////////////////////////
void lib_sort(std::vector<int>& nums) { std::sort(nums.begin(), nums.end()); }

#include <functional>
#include <iostream>
#include <string>
#include <random>

const std::vector<int> nums_ori = {5, 2, 9, 1, 5, 6, 0};
std::vector<int> gnums;
int random_int(int min, int max) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    std::uniform_int_distribution<int> dist(min, max);
    return dist(gen);
}
void generate_random_numbers(int size, int min_val, int max_val) {
    gnums.clear();
    for (int i = 0; i < size; ++i) gnums.push_back(random_int(min_val, max_val));
}

void show_nums(const std::string name, const std::vector<int>& nums) {
    std::cout << name << ":\t";
    for (int num : nums) std::cout << num << " ";
    std::cout << std::endl;
}

#include "./time_tools.hpp"
TIMER_CREATE(tim_sort);
void test_sort(const std::function<void(std::vector<int>&)>& sort_function, const std::string name) {
    std::vector<int> nums = nums_ori;
    sort_function(nums);
    // show_nums(name, nums);
    std::cout << name << ":\t";
    for (int num : nums) std::cout << num << " ";

    std::vector<int> gnums_ = gnums;
    tim_sort.tic();
    sort_function(gnums_);
    std::cout << "\t[time]: " << tim_sort.toc() << "\t";
    std::cout << std::endl;
}

void test_sort2(void (*sort_function)(std::vector<int>&), const std::string name) {
    std::vector<int> nums = nums_ori;
    sort_function(nums);
    show_nums(name, nums);
}

int main() {
    // 设置随机数生成的范围和数量
    int size = 10000, min_val = 1, max_val = 100000;
    generate_random_numbers(size, min_val, max_val);

    test_sort(bubble_sort, "bubble_sort");
    test_sort(select_sort, "select_sort");
    test_sort(select_sort2, "select_sort2");
    test_sort(insert_sort, "insert_sort");

    test_sort(quick_sort_nth, "quick_sort_nth");
    test_sort(quick_sort_part, "quick_sort_pt");

    test_sort(heap_sort, "heap_sort  ");
    test_sort(heap_sort_pq, "heap_sort_pq");
    test_sort(heap_sort_mh, "heap_sort_mh");
    test_sort(heap_sort_heap, "heap_sort_heap");

    test_sort(bucket_sort, "bucket_sort");

    test_sort(lib_sort, "lib_sort\t");

    return 0;
}
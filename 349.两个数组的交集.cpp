/*
 * @lc app=leetcode.cn id=349 lang=cpp
 *
 * [349] 两个数组的交集
 */

// #include <cstddef>
#include <unordered_set>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
        std::vector<int> result;

        // 100% 52%
        // std::unordered_set<int> map1, map2;
        // map1.insert(nums1.begin(), nums1.end());
        // for (auto& n : nums2)  // 去重
        //     if (map1.find(n) != map1.end()) map2.insert(n);
        // result.insert(result.end(), map2.begin(), map2.end());

        // 100% 92%
        bool map1[1001] = {false};
        for (auto& n : nums1) map1[n] = true;
        for (auto& n : nums2) {
            if (map1[n]) {
                map1[n] = false;  // 去重
                result.push_back(n);
            }
        }

        return result;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=145 lang=cpp
 *
 * [145] 二叉树的后序遍历
 */

#include <algorithm>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void recur_traversal(TreeNode *root, vector<int> &result) {
        if (root == nullptr) return;
        recur_traversal(root->left, result);
        recur_traversal(root->right, result);
        result.push_back(root->val);  // 中
    }

    void iter_traversal(TreeNode *root, vector<int> &result) {
        stack<TreeNode *> st;
        if (root) st.push(root);
        while (!st.empty()) {
            TreeNode *node = st.top();
            if (node) {
                st.pop();
                st.push(node);  // 中
                st.push(nullptr);
                if (node->right) st.push(node->right);  // 右
                if (node->left) st.push(node->left);    // 左
            } else {
                st.pop();
                node = st.top();
                st.pop();
                result.push_back(node->val);
            }
        }
    }

    vector<int> postorderTraversal(TreeNode *root) {
        vector<int> result;
        // recur_traversal(root, result);
        iter_traversal(root, result);
        return result;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=187 lang=cpp
 *
 * [187] 重复的DNA序列
 */

#include <bits/stdc++.h>
#include <string>
#include <unordered_set>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<string> findRepeatedDnaSequences(string s) {
        set<string> ans;
        unordered_set<string> mp;
        if (s.size() < 10) return {};

        for (int i = 0; i <= s.size() - 10; i++) {
            string tmp = s.substr(i, 10);
            auto it = mp.find(tmp);
            if (it != mp.end())
                ans.insert(tmp);
            else
                mp.insert(tmp);
        }

        vector<string> res(ans.begin(), ans.end());
        return res;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=452 lang=cpp
 *
 * [452] 用最少数量的箭引爆气球
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    int findMinArrowShots(vector<vector<int>>& points) {
        sort(points.begin(), points.end());
        int res = 0;

        // 不太一样，是需要所有的区间均有公共项
        for (int i = 0; i < points.size();) {
            int l = points[i][0];
            int r = points[i][1];
            int j = i + 1;
            while (j < points.size() && r >= points[j][0]) {
                l = max(l, points[j][0]);
                r = min(r, points[j][1]);
                j++;
            }
            res++;
            i = j;
        }

        return res;
    }
};
// @lc code=end

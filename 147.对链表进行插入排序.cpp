/*
 * @lc app=leetcode.cn id=147 lang=cpp
 *
 * [147] 对链表进行插入排序
 */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <list>
#include <unordered_map>
#include <vector>

using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* insertionSortList(ListNode* head) {
        if (!head) return nullptr;

        ///////////// 新建链表法
        ListNode* dummy = new ListNode();

        while (head) {
            ListNode* ori_next = head->next;  // 备份
            ListNode* cmp = dummy;

            // 有先后顺序的
            while (cmp->next && head->val > cmp->next->val) cmp = cmp->next;

            head->next = cmp->next;
            cmp->next = head;

            head = ori_next;
        }
        return dummy->next;
    }
};
// @lc code=end

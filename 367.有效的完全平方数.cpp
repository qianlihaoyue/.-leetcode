/*
 * @lc app=leetcode.cn id=367 lang=cpp
 *
 * [367] 有效的完全平方数
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    // f(x) = f(x_0) + f'(x_0)(x-x_0) = 0
    // x = x_0 - f(x_0) / f'(x_0)
    bool isPerfectSquare(int num) {
        float x = (float)num / 2;
        while (1) {
            float x_new = 0.5 * (x + (float)num / x);
            if (std::abs(x - x_new) < 0.5) break;
            x = x_new;
        }
        return (int)x * (int)x == num;
    }
};
// @lc code=end

int main() {
    Solution solu;
    solu.isPerfectSquare(195);
}

/*
 * @lc app=leetcode.cn id=77 lang=cpp
 *
 * [77] 组合
 */

#include <algorithm>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    void dfs(const int n, const int k, int i) {
        if (path.size() == k) {
            ans.push_back(path);
            return;
        }

        if (n - i + path.size() < k - 1) return; // 剪枝

        for (int j = i; j <= n; j++) {
            path.push_back(j);
            dfs(n, k, j + 1);
            path.pop_back();
        }
    }

    vector<vector<int>> combine(int n, int k) {
        dfs(n, k, 1);
        return ans;
    }
};
// @lc code=end

int main(int argc, char* argv[]) {
    Solution solu;

    // TIMER_CREATE(tim_cnt);
    auto res = solu.combine(20, 10);
    // tim_cnt.toc_cout();
    // for (auto &num : res) {
    //     for (auto &n : num) cout << n << " ";
    //     cout << endl;
    // }

    return 0;
}

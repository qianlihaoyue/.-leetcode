/*
 * @lc app=leetcode.cn id=454 lang=cpp
 *
 * [454] 四数相加 II
 */
#include <vector>
#include <unordered_map>
using namespace std;

// @lc code=start
class Solution {
public:
    // O(n)   + O(n^3) = O(n^3)
    // O(n^2) + O(n^2) = O(n^2)
    int fourSumCount(vector<int>& nums1, vector<int>& nums2, vector<int>& nums3, vector<int>& nums4) {
        int result = 0;
        unordered_map<int, int> ab_map;
        for (auto& a : nums1)
            for (auto& b : nums2) ab_map[a + b]++;
        for (auto& c : nums3)
            for (auto& d : nums4) {
                int tmp = 0 - (c + d);
                if (ab_map.find(tmp) != ab_map.end()) result += ab_map[tmp];
            }
        return result;
    }
};
// @lc code=end

int main() {
    Solution solute;

    // auto result = solute.fourSumCount(22);
    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << result << endl;

    return 0;
}

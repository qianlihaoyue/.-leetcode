/*
 * @lc app=leetcode.cn id=543 lang=cpp
 *
 * [543] 二叉树的直径
 */

#include <algorithm>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int max_dist = 0;

    int maxValue(TreeNode *root) {
        if (root) {
            int depth_l = maxValue(root->left);
            int depth_r = maxValue(root->right);
            int dist = depth_l + depth_r;
            max_dist = max(max_dist, dist);
            return max(depth_l, depth_r) + 1;
        }
        return 0;
    }

    int diameterOfBinaryTree(TreeNode *root) {
        maxValue(root);
        return max_dist;
    }
};
// @lc code=end

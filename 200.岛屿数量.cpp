/*
 * @lc app=leetcode.cn id=200 lang=cpp
 *
 * [200] 岛屿数量
 */

#include <algorithm>
using namespace std;

// @lc code=start
class Solution {
public:
    // DFS
    void dfs(vector<vector<char>>& grid, int i, int j) {  // y,x
        if (grid[i][j] == '0') return;
        grid[i][j] = '0';
        if (i - 1 >= 0) dfs(grid, i - 1, j);
        if (j - 1 >= 0) dfs(grid, i, j - 1);
        if (i + 1 < grid.size()) dfs(grid, i + 1, j);
        if (j + 1 < grid[0].size()) dfs(grid, i, j + 1);
    }

    int numIslands(vector<vector<char>>& grid) {
        int cnt = 0;
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if (grid[i][j] == '1') {
                    cnt++;
                    dfs(grid, i, j);
                }
            }
        }

        return cnt;
    }
};
// @lc code=end

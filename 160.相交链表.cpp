/*
 * @lc app=leetcode.cn id=160 lang=cpp
 *
 * [160] 相交链表
 */

#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        // 两个同时走完自己与对方的路
        if (!headA || !headB) return nullptr;
        ListNode *nodeA = headA, *nodeB = headB;
        while (nodeA != nodeB) {                  // 第一个相遇的就是 相交点
            nodeA = (nodeA) ? nodeA->next : headB;  // 走对方的路
            nodeB = (nodeB) ? nodeB->next : headA;  //
        }
        // 当不存在相交节点时，会走到末路，两个都是nullptr时相遇，此时已跳出循环
        return nodeA;
    }
};
// @lc code=end

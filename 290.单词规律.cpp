/*
 * @lc app=leetcode.cn id=290 lang=cpp
 *
 * [290] 单词规律
 */

#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <functional>
#include <map>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <string>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    bool wordPattern(string pattern, string s) {
        // 切分词语
        std::vector<string> words;
        std::istringstream iss(s);
        string word;
        while (iss >> word) words.push_back(word);

        if (pattern.size() != words.size()) return false;
        ////////////////// 双向哈希

        

        ////////////////// 单向hash+检验
        // std::vector<string> mymap(26, "");

        // for (int i = 0; i < pattern.size(); i++) {
        //     if (mymap[pattern[i] - 'a'] == "") {
        //         mymap[pattern[i] - 'a'] = words[i];

        //     } else {
        //         if (mymap[pattern[i] - 'a'] != words[i]) return false;
        //     }
        // }

        // set<string> mset;
        // int patt_cnt = 0;
        // for (int i = 0; i < 26; i++) {
        //     if (mymap[i] != "") {
        //         patt_cnt++;
        //         mset.insert(mymap[i]);
        //     }
        // }

        // return mset.size() == patt_cnt;
    }
};
// @lc code=end

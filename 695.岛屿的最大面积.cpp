/*
 * @lc app=leetcode.cn id=695 lang=cpp
 *
 * [695] 岛屿的最大面积
 */

#include <algorithm>
#include <iostream>

#include <array>
#include <vector>
#include <map>
#include <set>
#include <queue>
#include <string>
#include <utility>

using namespace std;

// @lc code=start
class Solution {
public:
    void dfs(vector<vector<int>>& grid, int& cnt, int i, int j) {  // y,x
        if (!grid[i][j]) return;
        grid[i][j] = 0;
        cnt++;
        if (i - 1 >= 0) dfs(grid, cnt, i - 1, j);
        if (j - 1 >= 0) dfs(grid, cnt, i, j - 1);
        if (i + 1 < grid.size()) dfs(grid, cnt, i + 1, j);
        if (j + 1 < grid[0].size()) dfs(grid, cnt, i, j + 1);
        return;
    }

    int maxAreaOfIsland(vector<vector<int>>& grid) {
        int max_num = 0;
        for (int i = 0; i < grid.size(); i++) {
            for (int j = 0; j < grid[0].size(); j++) {
                if (grid[i][j]) {
                    int cnt = 0;
                    dfs(grid, cnt, i, j);
                    max_num = std::max(max_num, cnt);
                }
            }
        }
        return max_num;
    }
};
// @lc code=end

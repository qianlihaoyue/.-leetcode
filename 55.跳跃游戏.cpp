/*
 * @lc app=leetcode.cn id=55 lang=cpp
 *
 * [55] 跳跃游戏
 */

#include <algorithm>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    bool canJump(vector<int>& nums) {
        if (nums.size() == 1) return true;
        if (nums[0] == 0) return false;

        // 主要问题在于0，能不能过去所有的0
        // 倒着来
        for (int i = nums.size() - 2; i >= 0;) {
            if (nums[i] == 0) {
                bool can = false;
                for (int j = 1; j <= i; j++) {
                    if (nums[i - j] > j) {
                        i -= j;
                        can = true;
                        break;
                    }
                }
                if (can == false) return false;
            } else {
                i--;
            }
        }
        return true;

        // // 查找所有的0，看是否能跳过,抄的
        // int count = 0;
        // for (int i = nums.size() - 2; i >= 0; i--) {  // 最后一个不用考虑
        //     if (nums[i] <= count)
        //         count++;  // 需要跨越的距离
        //     else
        //         count = 0;
        // }
        // return count == 0;
    }
};
// @lc code=end

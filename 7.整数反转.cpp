/*
 * @lc app=leetcode.cn id=7 lang=cpp
 *
 * [7] 整数反转
 */

#include <algorithm>
#include <cstdint>
#include <iostream>

#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    int reverse(int x) {
        // 字符串法

        // 位操作法，好像不行

        // 特例
        // if (x == -2147483648) return 0;
        // // // 求余运算法
        // int nega_flg = 1;
        // if (x < 0) {
        //     x = -x;
        //     nega_flg = -1;
        // }
        // int result = 0;
        // while (x >= 1) {
        //     if (result > INT32_MAX / 10) return 0;
        //     result = result * 10 + (x % 10);
        //     x /= 10;
        // }
        // return result * nega_flg;

        // 使用long
        long int tmp = x;
        long int result = 0;
        while (tmp) {
            result = result * 10 + tmp % 10;
            tmp /= 10;
        }
        return (int)result == result ? (int)result : 0;
    }
};
// @lc code=end

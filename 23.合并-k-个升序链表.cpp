/*
 * @lc app=leetcode.cn id=23 lang=cpp
 *
 * [23] 合并 K 个升序链表
 */

#include <algorithm>
#include <bits/stdc++.h>
#include <cstdint>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode* next) : val(x), next(next) {}
};

// @lc code=start
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* mergeKLists(vector<ListNode*>& lists) {
        // auto it = std::min_element(lists.begin(), lists.end(), [](ListNode* a, ListNode* b) { return a->val < b->val; });

        // 数组排序，不行，因为没法得到其余的元素
        // 小跟堆排序，行

        // ListNode* dummy = new ListNode();
        // ListNode* cur = dummy;
        // while (1) {
        //     int min_val = INT32_MAX, min_idx = -1;
        //     bool visit = false;
        //     for (int i = 0; i < lists.size(); i++) {
        //         if (lists[i]) {
        //             visit = true;
        //             if (lists[i]->val < min_val) {
        //                 min_val = lists[i]->val;
        //                 min_idx = i;
        //             }
        //         }
        //     }
        //     if (!visit) break;

        //     cur->next = lists[min_idx];
        //     cur = cur->next;
        //     lists[min_idx] = lists[min_idx]->next;
        // }
        // return dummy->next;

        // 递归
        // int min_val = INT32_MAX, min_idx = -1;
        // bool visit = false;
        // for (int i = 0; i < lists.size(); i++) {
        //     if (lists[i]) {
        //         visit = true;
        //         if (lists[i]->val < min_val) {
        //             min_val = lists[i]->val;
        //             min_idx = i;
        //         }
        //     }
        // }
        // if (!visit) return nullptr;

        // ListNode* res = lists[min_idx];
        // lists[min_idx] = lists[min_idx]->next;
        // res->next = mergeKLists(lists);
        // return res;
    }
};
// @lc code=end

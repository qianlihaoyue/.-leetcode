/*
 * @lc app=leetcode.cn id=433 lang=cpp
 *
 * [433] 最小基因变化
 */

#include <bits/stdc++.h>
#include <queue>
#include <set>
#include <string>
#include <unordered_map>
#include <variant>
using namespace std;

// @lc code=start
class Solution {
public:
    int minMutation(string startGene, string endGene, vector<string>& bank) {
        unordered_set<string> bakmp;
        for (auto& b : bank) bakmp.insert(b);
        const char key[4] = {'A', 'G', 'C', 'T'};
        unordered_set<string> visit;

        vector<string> qu;
        qu.push_back(startGene);

        int cnt = 0;
        while (qu.size()) {
            vector<string> newqu;
            cnt++;
            for (auto& gene : qu) {
                for (int i = 0; i < gene.size(); i++) {
                    char cur = gene[i];
                    auto tmp = gene;
                    for (auto& k : key) {
                        if (k == cur) continue;
                        tmp[i] = k;
                        // 在库中
                        if (bakmp.find(tmp) != bakmp.end()) {
                            if (tmp == endGene) return cnt;
                            if (visit.find(tmp) != visit.end()) continue;
                            visit.insert(tmp);
                            newqu.push_back(tmp);
                        }
                    }
                }
            }
            qu = newqu;
        }
        return -1;
    }
};
// @lc code=end

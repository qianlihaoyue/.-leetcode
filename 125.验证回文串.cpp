/*
 * @lc app=leetcode.cn id=125 lang=cpp
 *
 * [125] 验证回文串
 */

#include <algorithm>
#include <iostream>

#include <iterator>
#include <map>
#include <set>
#include <queue>
#include <string>
#include <vector>

using namespace std;

// @lc code=start
class Solution {
public:
    bool isPalindrome(string s) {
        // 格式化
        // 统一转成大写：ch & 0b11011111 简写：ch & 0xDF
        // 统一转成小写：ch | 0b00100000 简写：ch | 0x20

        // for (auto it = s.begin(); it != s.end();) {
        //     if (*it >= 'A' && *it <= 'Z') *it = *it | 0x20;  // - 'A' + 'a';
        //     if ((*it >= 'a' && *it <= 'z') || (*it >= '0' && *it <= '9'))
        //         it++;
        //     else
        //         s.erase(it);
        // }

        // 新建缓存
        string news;
        for (auto& c : s) {
            if (c >= 'A' && c <= 'Z') c |= 0x20;  // - 'A' + 'a';
            if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9')) news.push_back(c);
        }
        s = news;
        // 判断回文
        for (int i = 0; i < s.size() / 2; i++)
            if (s[i] != s[s.size() - 1 - i]) return false;
        return true;

        // 还有双指针法，不过我觉得可读性稍差一些
        // int fptr = 0, bptr = s.size() - 1;
        // while (fptr < bptr) {
        //     if (s[fptr]) }
    }
};
// @lc code=end

int main(int argc, char* argv[]) {
    Solution solu;

    string s = "A man, a plan, a canal: Panama";

    cout << solu.isPalindrome(s);

    return 0;
}

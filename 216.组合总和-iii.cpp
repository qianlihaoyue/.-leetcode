/*
 * @lc app=leetcode.cn id=216 lang=cpp
 *
 * [216] 组合总和 III
 */

#include <algorithm>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    int sum = 0;
    void dfs(const int n, const int k, int i) {
        if (path.size() == k) {
            if (sum == n) ans.push_back(path);
            return;
        }
        if (sum >= n) return;                  // sum太大，剪枝
        if (10 - i + path.size() < k) return;  // 剩余数目不够，剪枝

        for (int j = i; j <= 9; j++) {
            path.push_back(j), sum += j;
            dfs(n, k, j + 1);
            path.pop_back(), sum -= j;
        }
    }

    vector<vector<int>> combinationSum3(int k, int n) {
        dfs(n, k, 1);
        return ans;
    }
};
// @lc code=end

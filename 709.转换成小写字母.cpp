/*
 * @lc app=leetcode.cn id=709 lang=cpp
 *
 * [709] 转换成小写字母
 */

#include <algorithm>
#include <iterator>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    string toLowerCase(string s) {
        int offset = 'a' - 'A';
        for (auto &c : s) {
            if (c <= 'Z' && c >= 'A') c += offset;
        }
        return s;
    }
};
// @lc code=end

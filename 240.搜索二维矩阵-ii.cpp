/*
 * @lc app=leetcode.cn id=240 lang=cpp
 *
 * [240] 搜索二维矩阵 II
 */

#include <mutex>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        for (int i = 0, j = matrix[0].size() - 1; i < matrix.size() && j >= 0;) {  // 遍历行和列
            int& nums = matrix[i][j];                                              // 右上角值
            if (nums > target)       // 比目标值大，该数据不能在这一列，消除一列
                j--;                 // 往前找一列
            else if (nums < target)  // 比目标值小,本行全部不符合规则，消除一行
                i++;
            else if (nums == target)
                return true;
        }
        return false;
    }

    // int row_size = matrix.size();
    // int col_size = matrix[0].size();

    // int max_i = -1;

    // for (int i = 0; i < row_size && i < col_size; i++) {
    //     if (matrix[i][i] > target) {
    //         max_i = i;
    //         break;
    //     } else if (matrix[i][i] == target)
    //         return true;
    // }
    // // 是方阵
    // if (max_i != -1) {
    //     // 二分法找
    //     for (int i = 0; i < max_i; i++)
    //         if (matrix[i][max_i] == target || matrix[max_i][i] == target) return true;
    // }
    // // 非方阵
    // else {

    // }

    // return false;
};
// @lc code=end

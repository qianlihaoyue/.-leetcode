/*
 * @lc app=leetcode.cn id=20 lang=cpp
 *
 * [20] 有效的括号
 */

#include <stack>
#include <string>
using namespace std;

// @lc code=start
class Solution {
public:
    bool isValid(string s) {
        if (s.size() % 2) return false;

        // 抽象解法
        // std::stack<char> ch_stack;
        // for (int i = 0; i < s.size(); i++) {
        //     if (s[i] == '(')
        //         ch_stack.push(')');
        //     else if (s[i] == '[')
        //         ch_stack.push(']');
        //     else if (s[i] == '{')
        //         ch_stack.push('}');
        //     else {
        //         if (ch_stack.empty()) return false;
        //         char last_ch = ch_stack.top();
        //         ch_stack.pop();
        //         if (last_ch != s[i]) return false;
        //     }
        // }
        // return ch_stack.empty();

        stack<char> stk;
        for (auto &ch : s) {
            if (ch == '(' || ch == '[' || ch == '{')
                stk.push(ch);
            else {
                if (stk.empty()) return false;
                if ((stk.top() == '(' && ch == ')') || (stk.top() == '[' && ch == ']') || (stk.top() == '{' && ch == '}'))
                    stk.pop();
                else
                    return false;
            }
        }
        return stk.empty();
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=429 lang=cpp
 *
 * [429] N 叉树的层序遍历
 */

#include <algorithm>
#include <any>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

class Node {
public:
    int val;
    vector<Node *> children;

    Node() {}

    Node(int _val) { val = _val; }

    Node(int _val, vector<Node *> _children) {
        val = _val;
        children = _children;
    }
};

// @lc code=start
/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
public:
    vector<vector<int>> levelOrder(Node *root) {
        vector<vector<int>> result;
        if (root == nullptr) return result;
        vector<Node *> node_group;
        node_group.push_back(root);
        while (!node_group.empty()) {
            vector<Node *> node_next;
            vector<int> num_group;
            for (auto &node : node_group) {
                num_group.push_back(node->val);
                for (auto &child : node->children) node_next.push_back(child);
            }
            result.push_back(num_group);
            node_group = node_next;
        }
        return result;
    }
};
// @lc code=end

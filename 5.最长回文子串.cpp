/*
 * @lc app=leetcode.cn id=5 lang=cpp
 *
 * [5] 最长回文子串
 */

#include <algorithm>
using namespace std;

// @lc code=start
class Solution {
public:
    string longestPalindrome(string s) {
        // 抄的，字母扩散法，这不是暴力吗
        for (int i = 0; i < s.size(); i++) {
            // 两个都得有
            func(s, i, i);      // 奇数长度的子串
            func(s, i, i + 1);  // 偶数长度的子串
        }
        return s.substr(start, len);
    }

private:
    // 用于构造最长子串
    int start;
    int len = 0;

    void func(string& s, int i, int j) {
        while (i >= 0 && j < s.size() && s[i] == s[j]) {
            if (j - i + 1 > len) {
                // 定位暂时最长的子串位置
                start = i;
                len = j - i + 1;
            }
            i--, j++;
        }
    }
};
// @lc code=end

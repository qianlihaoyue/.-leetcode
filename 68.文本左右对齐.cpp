/*
 * @lc app=leetcode.cn id=68 lang=cpp
 *
 * [68] 文本左右对齐
 */

#include <bits/stdc++.h>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<string> fullJustify(vector<string>& words, int maxWidth) {
        vector<string> res;

        vector<string> tmp;
        int len_sum = 0;
        for (int i = 0; i < words.size(); i++) {
            // 每个单词都补充一个空格，末尾空格可以取消
            if (len_sum + (words[i].length() + 1) <= maxWidth + 1) {
                len_sum += (words[i].length() + 1);
                tmp.push_back(words[i]);
            } else {
                int word_size = len_sum - tmp.size();
                int mag = maxWidth - word_size;
                string token;
                for (int j = 0; j < tmp.size(); j++) {
                    token += tmp[j];

                    token += " ";
                }
            }
        }
    }
};
// @lc code=end

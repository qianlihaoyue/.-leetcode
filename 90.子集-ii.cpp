/*
 * @lc app=leetcode.cn id=90 lang=cpp
 *
 * [90] 子集 II
 */

#include <algorithm>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    void dfs(const vector<int>& nums, int i) {
        // if (i == nums.size()) {
        ans.push_back(path);
        // return;
        // }

        for (int j = i; j < nums.size(); j++) {
            // 去重
            if (j > i && nums[j] == nums[j - 1]) continue;
            //
            path.push_back(nums[j]);
            dfs(nums, j + 1);
            path.pop_back();
        }
    }

    vector<vector<int>> subsetsWithDup(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        dfs(nums, 0);
        return ans;
    }
};
// @lc code=end

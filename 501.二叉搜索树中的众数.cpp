/*
 * @lc app=leetcode.cn id=501 lang=cpp
 *
 * [501] 二叉搜索树中的众数
 */

#include <algorithm>
#include <iterator>
#include <memory>
#include <queue>
#include <vector>
#include <iostream>
#include <map>
#include <set>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    // 经典使用map统计众数
    map<int, int> mp;
    void traversal(TreeNode* root) {
        if (!root) return;
        mp[root->val]++;
        traversal(root->left);
        traversal(root->right);
    }

    vector<int> findMode(TreeNode* root) {
        traversal(root);

        // 如果是取前几位众数，则需要自定义排序
        // bool static cmp(const pair<int, int>& a, const pair<int, int>& b) {
        //     return a.second > b.second;  // 按照频率从大到小排序
        // }
        // vector<pair<int, int>> vec(mp.begin(), mp.end());
        // sort(vec.begin(), vec.end(), cmp);  // 给频率排个序

        // vector<int> cnt_vec(mp.size());
        // for (auto& n : mp) cnt_vec.push_back(n.second);
        // int max_cnt = *max_element(cnt_vec.begin(), cnt_vec.end());
        int max_cnt = 0;
        for (auto& n : mp) max_cnt = max(max_cnt, n.second);

        // 挑选result
        vector<int> result;
        for (auto& n : mp)
            if (n.second == max_cnt) result.push_back(n.first);
        return result;
    }
};
// @lc code=end

/*
 * @lc app=leetcode.cn id=238 lang=cpp
 *
 * [238] 除自身以外数组的乘积
 */

#include <algorithm>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<int> productExceptSelf(vector<int>& nums) {
        // 滑窗，分成两波 -- 比较浪费空间 81% 22%
        // vector<int> lefts(nums.begin(), nums.end());
        // vector<int> rights(nums.begin(), nums.end());
        // for (int i = 1; i < nums.size() - 1; i++) lefts[i] *= lefts[i - 1];
        // for (int i = nums.size() - 2; i > 0; i--) rights[i] *= rights[i + 1];

        // vector<int> answer(nums.size());
        // answer[0] = rights[1];
        // answer[nums.size() - 1] = lefts[nums.size() - 2];
        // for (int i = 1; i < nums.size() - 1; i++) {
        //     answer[i] = lefts[i - 1] * rights[i + 1];
        // }
        // return answer;

        // 积分 94% 33%
        vector<int> answer(nums.size(), 1);
        int sum = nums[0];
        for (int i = 1; i < nums.size(); i++) {
            answer[i] *= sum;
            sum *= nums[i];
        }
        sum = nums[nums.size() - 1];
        for (int i = nums.size() - 2; i >= 0; i--) {
            answer[i] *= sum;
            sum *= nums[i];
        }
        return answer;
    }
};
// @lc code=end

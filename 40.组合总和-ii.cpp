/*
 * @lc app=leetcode.cn id=40 lang=cpp
 *
 * [40] 组合总和 II
 */

#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    vector<vector<int>> ans;
    vector<int> path;
    int sum = 0;
    void dfs(const vector<int>& nums, const int target, int i) {
        if (sum == target) {
            ans.push_back(path);
            return;
        }

        for (int j = i; j < nums.size(); j++) {
            if (sum + nums[i] > target) return;  // 剪枝
            // 去除连续，j>i而不是0, 也可以使用vector<bool>& used来判断重复
            if (j > i && nums[j] == nums[j - 1]) continue;  // 去重

            path.push_back(nums[j]), sum += nums[j];
            dfs(nums, target, j + 1);
            path.pop_back(), sum -= nums[j];
        }
    }

    vector<vector<int>> combinationSum2(vector<int>& candidates, int target) {
        sort(candidates.begin(), candidates.end());
        dfs(candidates, target, 0);
        return ans;
    }
};

// @lc code=end

int main(int argc, char* argv[]) {
    Solution solu;

    // vector<int> candidates = {10, 1, 2, 7, 6, 1, 5};
    // auto res = solu.combinationSum2(candidates, 8);

    vector<int> candidates = {2, 5, 2, 1, 2};
    auto res = solu.combinationSum2(candidates, 5);

    for (auto& num : res) {
        for (auto& n : num) cout << n << " ";
        cout << endl;
    }

    return 0;
}

/*
 * @lc app=leetcode.cn id=144 lang=cpp
 *
 * [144] 二叉树的前序遍历
 */

#include <algorithm>
#include <vector>
#include <stack>
using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// @lc code=start
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    void recur_traversal(TreeNode *root, vector<int> &result) {
        if (root == nullptr) return;
        result.push_back(root->val);  // 中
        recur_traversal(root->left, result);
        recur_traversal(root->right, result);
    }

    void iter_traversal(TreeNode *root, vector<int> &result) {
        vector<TreeNode *> st;
        if (root) st.push_back(root);
        while (st.size()) {
            TreeNode *node = st.back();
            st.pop_back();
            result.push_back(node->val);
            if (node->right) st.push_back(node->right);  // 右
            if (node->left) st.push_back(node->left);    // 左
        }
    }
    vector<int> preorderTraversal(TreeNode *root) {
        vector<int> result;
        recur_traversal(root, result);
        // iter_traversal(root, result);
        return result;
    }
};
// @lc code=end

int main() {
    Solution solute;

    // vector<int> g = {2, 3, 1, 1, 4};
    // auto result = preorderTraversal.canJump(g);

    // for (auto& n : result) cout << n << " ";

    // for (auto& row : result) {
    //     for (auto& n : row) cout << n << " ";
    //     cout << endl;
    // }

    // cout << endl << result << endl;

    return 0;
}

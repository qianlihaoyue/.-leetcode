/*
 * @lc app=leetcode.cn id=33 lang=cpp
 *
 * [33] 搜索旋转排序数组
 */

#include <algorithm>
#include <iterator>
#include <vector>
using namespace std;

// @lc code=start
class Solution {
public:
    int search(vector<int>& nums, int target) {
        // 定理一：只有在顺序区间内才可 二分查找
        // 定理二：判断顺序区间还是乱序区间，left <= right ：顺序
        // 定理三：每次二分都会至少存在一个顺序区间
        int l = 0, r = nums.size() - 1;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            if (nums[mid] == target) return mid;

            // left 到 mid 是顺序区间
            if (nums[l] <= nums[mid]) {
                (target >= nums[l] && target < nums[mid]) ? r = mid - 1 : l = mid + 1;
            }
            // mid 到 right 是顺序区间
            else {
                (target > nums[mid] && target <= nums[r]) ? l = mid + 1 : r = mid - 1;
            }
        }
        return -1;
    }
};
// @lc code=end
